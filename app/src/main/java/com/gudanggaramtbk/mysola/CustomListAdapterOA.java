package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by LuckyM on 11/15/2018.
 */

public class CustomListAdapterOA extends ArrayAdapter<OpnameData> {
    Context                 mcontext;
    List<OpnameData>        TrxOA;
    ListView                oalisttask;
    private LayoutInflater  mInflater;
    public boolean          checkBoxState[];

    public CustomListAdapterOA(Context context, List<OpnameData> list, ListView listTask) {
        super(context, 0, list);
        mcontext      = context;
        TrxOA         = list;
        checkBoxState = new boolean[list.size()];
        oalisttask    = listTask;
        mInflater     = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // initialized list data
        for (int i = 0; i < list.size(); i++) {
            TrxOA.get(i).setQtyCheck(0.00);
            TrxOA.get(i).setChkFlag(false);

            // -- field note di set enabled tanpa terkecuali
            TrxOA.get(i).setNote_Enabled(true);
            //TrxOA.get(i).setNote_Enabled(false);

            oalisttask.setItemChecked(i, false);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderOA holder;
        Log.d("[GudangGaram]:", "CustomListAdapterOA :: getView");
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.rowlist_opname_accounting, parent, false);
            holder = new ViewHolderOA();

            holder.Xlbl_oa_trx_id       = (TextView) convertView.findViewById(R.id.lbl_oac_trx_id);
            holder.Xlbl_oa_Seq          = (TextView) convertView.findViewById(R.id.lbl_oac_seq);
            holder.Xtxt_oa_itemCode     = (TextView) convertView.findViewById(R.id.txt_oac_itemCode);
            holder.Xtxt_oa_OldItem      = (TextView) convertView.findViewById(R.id.txt_oac_OldItem);
            holder.Xtxt_oa_rak          = (TextView) convertView.findViewById(R.id.txt_oac_rak);
            holder.Xtxt_oa_bin          = (TextView) convertView.findViewById(R.id.txt_oac_bin);
            holder.Xtxt_oa_comp         = (TextView) convertView.findViewById(R.id.txt_oac_comp);
            holder.Xtxt_oa_itemID       = (TextView) convertView.findViewById(R.id.txt_oac_itemID);
            holder.Xtxt_oa_itemDesc     = (TextView) convertView.findViewById(R.id.txt_oac_itemDesc);
            holder.Xtxt_oa_blockID      = (TextView) convertView.findViewById(R.id.txt_oac_blockID);
            holder.Xtxt_oa_subinvstatus = (TextView) convertView.findViewById(R.id.txt_oac_subinvstatus);
            holder.Xtxt_oa_koli         = (TextView) convertView.findViewById(R.id.txt_oac_koli);
            holder.Xtxt_oa_lot          = (TextView) convertView.findViewById(R.id.txt_oac_lot);
            holder.Xtxt_oa_Qty_OHS      = (TextView) convertView.findViewById(R.id.txt_oac_Qty_OHS);
            holder.Xtxt_oa_Qty_PK       = (TextView) convertView.findViewById(R.id.txt_oac_Qty_PK);
            holder.Xchk_oa              = (CheckBox) convertView.findViewById(R.id.chk_oac);
            holder.Xtxt_oa_Qty_CK       = (EditText) convertView.findViewById(R.id.txt_oac_Qty_CK);
            holder.Xtxt_oa_UOM          = (TextView) convertView.findViewById(R.id.txt_oac_UOM);
            holder.Xtxt_oa_Note         = (TextView) convertView.findViewById(R.id.txt_oac_Note);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolderOA) convertView.getTag();
            holder.Xchk_oa.setOnCheckedChangeListener(null);
        }

        /*
        // disabled qty checked awal2
        holder.Xtxt_oa_Qty_CK.setEnabled(false);
        // disabled notes
        holder.Xtxt_oa_Note.setEnabled(false);
        */

        holder.Xlbl_oa_trx_id.setText(TrxOA.get(position).getDtl_ID().toString());
        holder.Xlbl_oa_Seq.setText(TrxOA.get(position).getSeq().toString());
        holder.Xtxt_oa_itemCode.setText(TrxOA.get(position).getItemCode());
        holder.Xtxt_oa_OldItem.setText(TrxOA.get(position).getItemOld());
        holder.Xtxt_oa_rak.setText(TrxOA.get(position).getLocRak());
        holder.Xtxt_oa_bin.setText(TrxOA.get(position).getLocBin());
        holder.Xtxt_oa_comp.setText(TrxOA.get(position).getLocComp());
        //holder.Xtxt_oa_itemID.setText(TrxOA.get(position).getItemID());
        holder.Xtxt_oa_itemDesc.setText(TrxOA.get(position).getItemDesc());
        //holder.Xtxt_oa_blockID;
        holder.Xtxt_oa_subinvstatus.setText(TrxOA.get(position).getSubInvStatus());
        holder.Xtxt_oa_koli.setText(TrxOA.get(position).getKoliNum());
        holder.Xtxt_oa_lot.setText(TrxOA.get(position).getLotNum());
        holder.Xtxt_oa_Qty_OHS.setText(Double.toString(TrxOA.get(position).getQtyOnHand()));
        holder.Xtxt_oa_Qty_PK.setText(Double.toString(TrxOA.get(position).getQtyPicked()));
        holder.Xtxt_oa_UOM.setText(TrxOA.get(position).getUom());

        holder.Xtxt_oa_Qty_CK.setText(Double.toString(TrxOA.get(position).getQtyCheck()));
        //holder.Xtxt_oa_Qty_CK.setEnabled(TrxOA.get(position).getChkFlag());

        //holder.Xtxt_oa_Qty_CK.setEnabled(oalisttask.isItemChecked(position));
        holder.Xtxt_oa_Note.setText(TrxOA.get(position).getNotes());
        holder.Xtxt_oa_Note.setEnabled(TrxOA.get(position).getNote_Enabled());
        holder.Xchk_oa.setChecked(TrxOA.get(position).getChkFlag());
        //holder.Xchk_oa.setChecked(oalisttask.isItemChecked(position));

        holder.Xchk_oa.setFocusable(false);
        holder.Xchk_oa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EH_CMD_CHECK(v,holder, position);
            }
        });

        holder.Xtxt_oa_Qty_CK.setFocusable(false);
        holder.Xtxt_oa_Qty_CK.setFocusableInTouchMode(true);
        holder.Xtxt_oa_Qty_CK.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // When focus is lost check that the text field has valid values.
                if (!hasFocus) {
                    Log.d("[GudangGaram]", "CustomListAdapterCK :: Xtxt_oa_Qty_CK :: Lost Focus");
                    EH_XQty_CH_CHANGE(v, holder, position);
                }
                else{
                    Log.d("[GudangGaram]", "CustomListAdapterCK :: Xtxt_oa_Qty_CK :: Got Focus");
                }
            }
        });

        holder.Xtxt_oa_Note.setEnabled(TrxOA.get(position).getNote_Enabled());

        holder.Xtxt_oa_Note.setFocusable(false);
        holder.Xtxt_oa_Note.setFocusableInTouchMode(true);
        holder.Xtxt_oa_Note.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // When focus is lost check that the text field has valid values.
                if (!hasFocus) {
                    Log.d("[GudangGaram]", "CustomListAdapterOA :: Xtxt_oa_Note :: Lost Focus");
                    EH_XTxt_Note_CHANGE(v, holder, position);
                }
                else{
                    Log.d("[GudangGaram]", "CustomListAdapterOA :: Xtxt_oa_Note :: Got Focus");
                }
            }
        });

        return convertView;
    }


    public void EH_CMD_CHECK(View v, ViewHolderOA hd, Integer position){
        Boolean selection;
        CheckBox cb = (CheckBox) v ;
        selection = ((CheckBox) v).isChecked();

        try{
            // init clear input
            //TrxOA.get(position).setNotes("");
            TrxOA.get(position).setNote_Enabled(true);
            //TrxOA.get(position).setNote_Enabled(false);

            if(selection == true){
                Log.d("[GudangGaram]:", "EH_CMD_CHECK :: Checked");
                if(Double.compare(Double.parseDouble(TrxOA.get(position).getQtyOnHand().toString()), 0.0) == 0){
                    Log.d("[GudangGaram]:", "EH_CMD_CHECK :: A");
                    TrxOA.get(position).setQtyCheck(0.0);
                    // qty di rubah jadi nol ada kemungkinan memang fakta qty di lapangan 0 (hilang dsb)
                    //TrxOA.get(position).setChkFlag(false);
                    //TrxOA.get(position).setNotes("");
                    //oalisttask.setItemChecked(position, false);
                }
                else{
                    Log.d("[GudangGaram]:", "EH_CMD_CHECK :: B");
                    TrxOA.get(position).setQtyCheck(Double.parseDouble(TrxOA.get(position).getQtyOnHand().toString()));
                    TrxOA.get(position).setChkFlag(true);
                    oalisttask.setItemChecked(position, true);
                }
            }
            else{
                Log.d("[GudangGaram]:", "EH_CMD_CHECK :: UnChecked");
                TrxOA.get(position).setQtyCheck(0.0);
                TrxOA.get(position).setChkFlag(false);
                //TrxOA.get(position).setNotes("");
                oalisttask.setItemChecked(position, false);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "EH_CMD_CHECK Exception " + e.getMessage().toString());
        }
    }

    public void EH_XQty_CH_CHANGE(View v, ViewHolderOA hd, Integer position){
        EditText et = (EditText) v ;
        try{
            Log.d("[GudangGaram]", "EH_XQty_CH_CHANGE et.getText() = " + et.getText().toString());
            if(Double.compare(Double.parseDouble(et.getText().toString()),0.0) > 0){
                Log.d("[GudangGaram]", "EH_XQty_CH_CHANGE > 0");
                TrxOA.get(position).setQtyCheck(Double.parseDouble(et.getText().toString()));
                TrxOA.get(position).setChkFlag(true);
                oalisttask.setItemChecked(position, true);
                //TrxOA.get(position).setNotes("");

                if (TrxOA.get(position).getChkFlag().equals(true) &&
                        (Double.parseDouble(TrxOA.get(position).getQtyOnHand().toString()) != Double.parseDouble(et.getText().toString()))) {
                    Log.d("[GudangGaram]", "EH_XQty_CH_CHANGE :: A");
                    TrxOA.get(position).setNote_Enabled(true);
                }
                else {
                    Log.d("[GudangGaram]", "EH_XQty_CH_CHANGE :: B");
                    // -- field note di set enabled tanpa terkecuali
                    TrxOA.get(position).setNote_Enabled(true);
                    //TrxOA.get(position).setNote_Enabled(false);
                }
            }
            else if(Double.compare(Double.parseDouble(et.getText().toString()),0.0) == 0){
                Log.d("[GudangGaram]", "EH_XQty_CH_CHANGE = 0");
                TrxOA.get(position).setQtyCheck(0.0);
                if(TrxOA.get(position).getChkFlag() == true){
                    // qty opname 0 maka harus di isi reason nya (hilang / rusak / dsb)
                    TrxOA.get(position).setNote_Enabled(true);
                }
                else{
                    // -- field note di set enabled tanpa terkecuali
                    TrxOA.get(position).setNote_Enabled(true);
                    //TrxOA.get(position).setNote_Enabled(false);
                }
                //TrxOA.get(position).setChkFlag(false);
                //oalisttask.setItemChecked(position, false);
            }
            hd.Xtxt_oa_Note.setEnabled(TrxOA.get(position).getNote_Enabled());
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_XQty_CH_CHANGE Exception " + e.getMessage());
            TrxOA.get(position).setQtyCheck(0.0);
            TrxOA.get(position).setChkFlag(false);
            // -- field note di set enabled tanpa terkecuali
            TrxOA.get(position).setNote_Enabled(true);
            //TrxOA.get(position).setNote_Enabled(false);
            oalisttask.setItemChecked(position, false);
        }
    }

    public void EH_XTxt_Note_CHANGE(View v, ViewHolderOA hd, Integer position) {
        EditText et = (EditText) v;
        Log.d("[GudangGaram]", "EH_XTxt_Note_CHANGE :" + et.getText());
        try {
            if(et.getText().length() > 0) {
                TrxOA.get(position).setNotes(et.getText().toString());
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "EH_XTxt_Note_CHANGE Exception : " + e.getMessage());
        }
    }
}
