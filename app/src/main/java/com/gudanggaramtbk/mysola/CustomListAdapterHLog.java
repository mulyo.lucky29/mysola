package com.gudanggaramtbk.mysola;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by luckym on 11/23/2018.
 */

public class CustomListAdapterHLog extends ArrayAdapter<OpnameHLog> {
    Context                   mcontext;
    List<OpnameHLog>          ListOpnameHLog;
    ListView                  olistbatch;
    private SharedPreferences config;
    private MySQLiteHelper    dbHelper;
    private String            xDeviceID;
    private LayoutInflater    mInflater;
    public boolean            checkBoxState[];
    private Message           msg;
    private MainActivity      xParentActivity;


    public CustomListAdapterHLog(Context context,  List<OpnameHLog> list) {
        super(context, 0, list);
        this.mcontext = context;
        this.ListOpnameHLog = list;

        Log.d("[GudangGaram]:", "Main Activity :: CustomListAdapterHLog");

        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        msg = new Message(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderHLog holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.rowlist_hist_log, parent, false);

            holder = new ViewHolderHLog();

            holder.Xtxt_HLogID          = (TextView) convertView.findViewById(R.id.txt_HLogID);
            holder.Xtxt_HLog_NoTrx      = (TextView) convertView.findViewById(R.id.txt_HLog_NoTrx);
            holder.Xtxt_HLog_TglOpnam   = (TextView) convertView.findViewById(R.id.txt_HLog_TglOpnam);
            holder.Xtxt_HLog_Gudang     = (TextView) convertView.findViewById(R.id.txt_HLog_Gudang);
            holder.Xtxt_HLog_Rak        = (TextView) convertView.findViewById(R.id.txt_HLog_Rak);
            holder.Xtxt_HLog_Bin        = (TextView) convertView.findViewById(R.id.txt_HLog_Bin);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolderHLog) convertView.getTag();
        }

        TextView Lbl_HLogID = holder.Xtxt_HLogID;
        TextView Lbl_HLog_NoTrx = holder.Xtxt_HLog_NoTrx;
        TextView Lbl_HLog_TglOpnam = holder.Xtxt_HLog_TglOpnam;
        TextView Lbl_HLog_Gudang = holder.Xtxt_HLog_Gudang;
        TextView Lbl_HLog_Rak = holder.Xtxt_HLog_Rak;
        TextView Lbl_HLog_Bin = holder.Xtxt_HLog_Bin;


        Lbl_HLogID.setText(ListOpnameHLog.get(position).getHLogID());
        Lbl_HLog_NoTrx.setText(ListOpnameHLog.get(position).getHLog_NoTrx());
        Lbl_HLog_TglOpnam.setText(ListOpnameHLog.get(position).getHLog_TglOpnam());
        Lbl_HLog_Gudang.setText(ListOpnameHLog.get(position).getHLog_Gudang());
        Lbl_HLog_Rak.setText(ListOpnameHLog.get(position).getHLog_Rak());
        Lbl_HLog_Bin.setText(ListOpnameHLog.get(position).getHLog_Bin());


        return convertView;
    }
}
