package com.gudanggaramtbk.mysola;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LoadOpnameAccActivity extends AppCompatActivity implements View.OnClickListener{
    private SharedPreferences        config;
    private MySQLiteHelper           dbHelper;
    private String                   StrSessionNIK;
    private String                   StrSessionName;
    private String                   StrSessionGroup;

    private String                   StrDeviceID;
    private Boolean                  PopulateFlag;

    private Spinner                  Cbo_oa_KodeOpnam;
    private TextView                 Txt_oa_fileID;
    private TextView                 Txt_oa_org_name;
    private TextView                 Txt_oa_org_id;
    private Button                   Cmd_oa_populate;
    private Button                   Cmd_oa_close;
    private Button                   Cmd_oa_submit;
    private ListView                 Lst_OpnameAcc;

    private CustomListAdapterOA      oaadapter;
    private List<OpnameData>         Result;

    private SoapSendDataOA           SendOA;

    private void EH_cmd_back_main(){
        Intent upIntent = NavUtils.getParentActivityIntent(this);
        if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
            TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(upIntent)
                    .startActivities();
        } else {
            // passing intent login information before back
            upIntent.putExtra("PIC_NIK",   StrSessionNIK);
            upIntent.putExtra("PIC_NAME",  StrSessionName);
            upIntent.putExtra("PIC_GROUP", StrSessionGroup);
            NavUtils.navigateUpTo(this, upIntent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed LoadOpnameAccActivity Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                EH_cmd_back_main();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_opname_acc);

        Log.d("[GudangGaram]: ","LoadOpnameAccActivity :: onCreate");

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // get information intent
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
        StrSessionGroup = getIntent().getStringExtra("PIC_GROUP");
        StrDeviceID    = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        Cbo_oa_KodeOpnam   = (Spinner) findViewById(R.id.cbo_oa_KodeOpnam);
        Txt_oa_fileID      = (TextView) findViewById(R.id.txt_oa_fileID);
        Txt_oa_org_name    = (TextView) findViewById(R.id.txt_oa_org_name);
        Txt_oa_org_id      = (TextView) findViewById(R.id.txt_oa_org_id);

        Cmd_oa_populate    = (Button) findViewById(R.id.cmd_oa_populate);
        Cmd_oa_close       = (Button) findViewById(R.id.cmd_oa_close);
        Cmd_oa_submit      = (Button) findViewById(R.id.cmd_oa_submit);

        Lst_OpnameAcc      = (ListView) findViewById(R.id.lst_OpnameAcc);
        Lst_OpnameAcc.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        initComboBoxKodeOpname(StrSessionNIK);

        // ----------- event handler ------------
        Cmd_oa_populate.setOnClickListener(this);
        Cmd_oa_close.setOnClickListener(this);
        Cmd_oa_submit.setOnClickListener(this);

        PopulateFlag = false;

        // -------- instant SoapSendDataOA ---------------
        SendOA = new SoapSendDataOA(config);
        SendOA.setParentActivity(this);
        SendOA.setContext(this);
        SendOA.setDBHelper(dbHelper);

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.cmd_oa_populate){
            EH_CMD_POPULATE();
        }
        else if(id == R.id.cmd_oa_close){
            EH_CMD_CLOSE();
        }
        else if(id == R.id.cmd_oa_submit){
            EH_CMD_SEND();
        }
    }

    public void DialogBox(String Title, String Message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle(Title)
                .setMessage(Message)
                .setCancelable(false)
                .setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private Integer do_check_select(){
        Integer result, err, checked;

        result  = 0;
        err     = 0;
        checked = 0;

        if(Lst_OpnameAcc.getCount() == 0){
            err +=1;
            DialogBox("Item Not Populated", "Please Populate Item First");
        }
        else {
            for (int idx = 0; idx < Lst_OpnameAcc.getCount(); idx++) {
                if (Lst_OpnameAcc.isItemChecked(idx) == true) {
                    checked = checked + 1;
                }
            }
            if (checked == 0) {
                err += 1;
                DialogBox("Item Not Selected", "Please Select At Least One");
            }
        }

        if(err > 0){
            result = 0;
        }
        else{
            result = checked;
        }

        return result;
    }

    public void initTextGudang(){
        Txt_oa_org_name.setText("");
        Txt_oa_org_id.setText("");
    }

    public void initComboBoxKodeOpname(String pStrSessionNIK){
        Log.d("[GudangGaram]", "LoadOpnameAccActivity :: initComboBoxKodeOpname ");

        List<StringWithTag> lvi = new ArrayList<StringWithTag>();
        try{
            lvi = dbHelper.loadKodeStockOpnameAcc(pStrSessionNIK);
            initKVPComboBox(R.id.cbo_oa_KodeOpnam,R.id.txt_oa_fileID, R.id.txt_oa_org_name, lvi);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoadOpnameAccActivity :: initComboBoxKodeOpname >> " + e.getMessage().toString());
        }
    }

    private void initKVPComboBox(final int p_list_layout, final int p_list_id, final int p_gudang, List<StringWithTag> lvi){
        Spinner  spinner       = (Spinner) findViewById (p_list_layout);
        final TextView tvid    = (TextView) findViewById(p_list_id);
        final TextView Gudang  = (TextView) findViewById(p_gudang);
                // Creating adapter for spinner
                ArrayAdapter<StringWithTag> dataAdapter = new ArrayAdapter<StringWithTag>(this, android.R.layout.simple_spinner_item, lvi);
                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // attaching data adapter to spinner
                spinner.setAdapter(dataAdapter);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        StringWithTag swt = (StringWithTag) parent.getItemAtPosition(position);
                        String        key = (String) swt.tag;
                        Log.d("[GudangGaram]", "LoadOpnameAccActivity :: KVP Value Selected >> " + swt);
                        Log.d("[GudangGaram]", "LoadOpnameAccActivity :: KVP Key   Selected >> " + key.toString());
                        tvid.setText(key.toString());
                        Gudang.setText(dbHelper.getGudangName(key.toString()));
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
    }

    // ------------- event handler --------------------
    public void EH_CMD_POPULATE(){
        Log.d("[GudangGaram]","LoadOpnameAccActivity :: EH_CMD_POPULATE");
        try{
            Result = dbHelper.getOpnameAccList(Txt_oa_fileID.getText().toString(),StrSessionNIK);
            Lst_OpnameAcc.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            oaadapter = new CustomListAdapterOA(this, Result, Lst_OpnameAcc);
            oaadapter.notifyDataSetChanged();
            Lst_OpnameAcc.setAdapter(oaadapter);

            long xcount = Lst_OpnameAcc.getCount();
            if (xcount == 0) {
                Toast.makeText(this, "No Data Result", Toast.LENGTH_LONG).show();
            }
        }
        catch(Exception e){
        }
    }

    public void EH_CMD_SEND_ACTION(){
        List<OpnameData> listselected;
        int selectedcount = 0;
        int rown          = 0;
        String LastFlag;
        String OpnameDate;

        selectedcount = do_check_select();
        Log.d("[GudangGaram]","Selected Count = " + selectedcount);

        if(selectedcount > 0){
            listselected = new ArrayList<OpnameData>();
            OpnameDate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
            try{
                for(int idx = 0; idx < Lst_OpnameAcc.getCount(); idx ++){
                    if(Lst_OpnameAcc.isItemChecked(idx) == true) {
                        View HoldView = Lst_OpnameAcc.getAdapter().getView(idx, null, null);
                        ViewHolderOA holder = (ViewHolderOA) HoldView.getTag();

                        OpnameData oa = new OpnameData();
                        oa.setOpnameCode(Cbo_oa_KodeOpnam.getSelectedItem().toString());
                        oa.setOrgName(Txt_oa_org_name.getText().toString());
                        oa.setOpnameDate(OpnameDate);
                        oa.setDtl_ID(Integer.parseInt(holder.Xlbl_oa_trx_id.getText().toString()));
                        oa.setFileID(Integer.parseInt(Txt_oa_fileID.getText().toString()));
                        oa.setQtyCheck(Double.parseDouble(holder.Xtxt_oa_Qty_CK.getText().toString()));
                        oa.setNotes(holder.Xtxt_oa_Note.getText().toString());

                        // this is the last item selected
                        if(rown == (selectedcount -1)){
                            LastFlag = "Y";
                            Log.d("[GudangGaram]","Last Flag (" + idx + ")");
                        }
                        else{
                            LastFlag = "N";
                        }
                        SendOA.Send(oa, StrSessionNIK, StrDeviceID, LastFlag);
                        try {
                            Thread.sleep(300);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        rown = rown + 1;
                    }
                }
            }
            catch(Exception e){
            }

            if(rown > 0){
            }
        }
    }

    public void EH_CMD_SEND(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Send Data Confirm")
                .setMessage("Confirm Send Opname Data ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                EH_CMD_SEND_ACTION();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void EH_CMD_CLOSE(){
        Log.d("[GudangGaram]: ","LoadOpnameAccActivity :: EH_CMD_CLOSE");
        /*
        Intent upIntent = NavUtils.getParentActivityIntent(this);
        // passing intent login information before back
        upIntent.putExtra("PIC_NIK",  StrSessionNIK);
        upIntent.putExtra("PIC_NAME", StrSessionName);
        NavUtils.navigateUpTo(this, upIntent);
        */
        EH_cmd_back_main();
    }

}
