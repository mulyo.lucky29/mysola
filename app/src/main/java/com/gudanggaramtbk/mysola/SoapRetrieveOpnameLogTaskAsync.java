package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 11/5/2018.
 */

public class SoapRetrieveOpnameLogTaskAsync extends AsyncTask<String, Void, String>  {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper       dbHelper;
    private String               SentResponse;
    private String               OrgID;
    private String               Rak;
    private String               Bin;
    private ListView             listOpnameLog;
    private List<OpnameLogistic> Result;

    CustomListAdapterOL          oladapter;
    //private ArrayAdapter<String> adapter;

    public SoapRetrieveOpnameLogTaskAsync(){
    }
    public interface SoapRetrieveOpnameLogTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveOpnameLogTaskAsync.SoapRetrieveOpnameLogTaskAsyncResponse delegate = null;
    public SoapRetrieveOpnameLogTaskAsync(SoapRetrieveOpnameLogTaskAsync.SoapRetrieveOpnameLogTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context, MySQLiteHelper dbHelper, SharedPreferences config, ListView lst, String pOrgID, String pRak, String pBin){
        Log.d("[GudangGaram]", "SoapRetrieveOpnameLogTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.pd                = new ProgressDialog(this.context);
        this.Rak               = pRak;
        this.Bin               = pBin;
        this.OrgID             = pOrgID;
        this.listOpnameLog     = lst;

        Result = new ArrayList<OpnameLogistic>();
    }
    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveOpnameLogTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait Retrieving Data From Oracle Inprogress ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    protected  void onProgressUpdate(String[] args) {
        //progressDialog.setMessage(args[0]);
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveOpnameLog TaskAsync :: onPostExecute >> " + SentResponse);
        try{
            listOpnameLog.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            oladapter = new CustomListAdapterOL(context, Result, listOpnameLog);
            oladapter.notifyDataSetChanged();
            listOpnameLog.setAdapter(oladapter);

            long xcount = listOpnameLog.getCount();
            if (xcount == 0) {
                Toast.makeText(context, "No Data Result", Toast.LENGTH_LONG).show();
            }
            delegate.PostSentAction(output);
        }
        catch(Exception e){
        }
        Toast.makeText(context,"Retrieve Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        int count;
        //String response = null;
        String rak_persiapan = "";

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Opname_Logistic";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        Log.d("[GudangGaram]", "SOAP REQUEST PARAM Org_ID  :" + OrgID);
        Log.d("[GudangGaram]", "SOAP REQUEST PARAM Rak     :" + Rak);
        Log.d("[GudangGaram]", "SOAP REQUEST PARAM Bin     :" + Bin);

        // =============== p_org_id ===================
        PropertyInfo prop_p_org_id = new PropertyInfo();
        prop_p_org_id.setName("p_org_id");
        prop_p_org_id.setValue(OrgID);
        prop_p_org_id.setType(String.class);

        // =============== p_rak ===================
        PropertyInfo prop_p_rak = new PropertyInfo();
        prop_p_rak.setName("p_rak");
        prop_p_rak.setValue(Rak);
        prop_p_rak.setType(String.class);

        // =============== p_bin ===================
        PropertyInfo prop_p_bin = new PropertyInfo();
        prop_p_bin.setName("p_bin");
        prop_p_bin.setValue(Bin);
        prop_p_bin.setType(String.class);


        request.addProperty(prop_p_org_id);
        request.addProperty(prop_p_rak);
        request.addProperty(prop_p_bin);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);


        //Web method call
        try {
            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,6000);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj  = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));

                Result.clear();

                if (obj2.getPropertyCount() > 0) {
                    for (int i = 0; i < obj2.getPropertyCount(); i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);

                        //listOpnameLog
                        OpnameLogistic ol = new OpnameLogistic();
                        ol.setItemID(obj3.getProperty(0).toString());
                        ol.setItemCode(obj3.getProperty(1).toString());
                        ol.setItemOld(obj3.getProperty(2).toString());
                        ol.setItemDesc(obj3.getProperty(3).toString());
                        ol.setOrgID(obj3.getProperty(4).toString());
                        ol.setLocRak(obj3.getProperty(5).toString());
                        ol.setLocBin(obj3.getProperty(6).toString());
                        ol.setLocComp(obj3.getProperty(7).toString());
                        ol.setRakSeq(obj3.getProperty(8).toString());
                        ol.setBinSeq(obj3.getProperty(9).toString());
                        ol.setLotNum(obj3.getProperty(10).toString());
                        ol.setKoliNum(obj3.getProperty(11).toString());
                        ol.setBlockID(obj3.getProperty(12).toString());
                        ol.setSubInvStatus(obj3.getProperty(13).toString());
                        ol.setQtyOnHand(Double.parseDouble(obj3.getProperty(14).toString()));
                        ol.setQtyPicked(Double.parseDouble(obj3.getProperty(15).toString()));
                        ol.setQtyOhmp(Double.parseDouble(obj3.getProperty(16).toString()));
                        ol.setQtyCheck(0.00);
                        ol.setUOM(obj3.getProperty(17).toString());
                        ol.setNote(" ");

                        Log.d("[GudangGaram]", "================================= BEGIN ============================================");
                        Log.d("[GudangGaram]", "Item ID      :" + obj3.getProperty(0).toString());
                        Log.d("[GudangGaram]", "Item Code    :" + obj3.getProperty(1).toString());
                        Log.d("[GudangGaram]", "Old Item     :" + obj3.getProperty(2).toString());
                        Log.d("[GudangGaram]", "Item Desc    :" + obj3.getProperty(3).toString());
                        Log.d("[GudangGaram]", "Org_ID       :" + obj3.getProperty(4).toString());
                        Log.d("[GudangGaram]", "Rak          :" + obj3.getProperty(5).toString());
                        Log.d("[GudangGaram]", "Bin          :" + obj3.getProperty(6).toString());
                        Log.d("[GudangGaram]", "Comp         :" + obj3.getProperty(7).toString());
                        Log.d("[GudangGaram]", "Rak Seq      :" + obj3.getProperty(8).toString());
                        Log.d("[GudangGaram]", "Bin Seq      :" + obj3.getProperty(9).toString());
                        Log.d("[GudangGaram]", "Lot No       :" + obj3.getProperty(10).toString());
                        Log.d("[GudangGaram]", "Koli No      :" + obj3.getProperty(11).toString());
                        Log.d("[GudangGaram]", "Block ID     :" + obj3.getProperty(12).toString());
                        Log.d("[GudangGaram]", "SubInv Stat  :" + obj3.getProperty(13).toString());
                        Log.d("[GudangGaram]", "Qty Onhand   :" + obj3.getProperty(14).toString());
                        Log.d("[GudangGaram]", "Qty Picked   :" + obj3.getProperty(15).toString());
                        Log.d("[GudangGaram]", "Qty Ohmp     :" + obj3.getProperty(16).toString());
                        Log.d("[GudangGaram]", "UOM          :" + obj3.getProperty(17).toString());

                        Result.add(ol);
                    }
                } else {
                    Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                }
            }
            catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveTaskItemTaskAsync  :: end doInBackground");
        }

        return "";
    }
}