package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by LuckyM on 11/8/2018.
 */

public class SoapSendReqOL {
    private SharedPreferences                 config;
    private Context                           context;
    private MySQLiteHelper                    dbHelper;
    private LoadOpnameLogisticActivity        pActivity;
    private String                            HeaderID;

    // override constructor
    public SoapSendReqOL(SharedPreferences PConfig){
        Log.d("[GudangGaram]:", "SoapSendReqOL Constructor");
        this.config           = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]:", "SetContext");
        this.context             = context;
    }
    public void setParentActivity(LoadOpnameLogisticActivity pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]:", "SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Notify_Finish(String pNoTrx, String pOrgID, String pOrgName, String pRak, String pBin, String pNik){
        String pOpnameLogDate;
        try{
            pOpnameLogDate =  new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());

            Log.d("[GudangGaram]:", "Notify_Finish : pOrgID : " + pOrgID);

            OpnameHLog ohl = new OpnameHLog();
            ohl.setHLog_NoTrx(pNoTrx);
            ohl.setHLog_Rak(pRak);
            ohl.setHLog_Bin(pBin);
            ohl.setHLog_Gudang(pOrgName);
            ohl.setHLog_TglOpnam(pOpnameLogDate);
            ohl.setHLog_NIK(pNik);

            // save to dh for history list
            dbHelper.add_HLogList(ohl);

            pActivity.clear_all();
            pActivity.DialogBox("Sending Finish","Sending Opname Data Finish");
        }
        catch(Exception e){
        }
    }

    public void Send( final List<OpnameLogistic> lviOpnameLog, final String org_id, final String org_name, final String Rak, final String Bin, final String o_nik, final String o_device_id) {
        Integer ncount;
        final Integer itercount;

        Log.d("[GudangGaram]", "SoapSendReqOL :: Send > Begin");

        try{
            ncount = lviOpnameLog.size();
        }
        catch(Exception e){
            ncount = 0;
        }

        itercount = ncount;
        try {
            // call WS To Retrieve Manifest from oracle and save it to local database table
            SoapSendReqOLTaskAsync SoapRequest = new SoapSendReqOLTaskAsync(new SoapSendReqOLTaskAsync.SoapSendReqOLTaskAsyncResponse() {
                @Override
                public void PostSentAction(final String header_id) {
                    String Message;

                    try {
                        Log.d("[GudangGaram]", "SendRequestUpdateFlag Start");
                        if(header_id.equals("*") == true){
                        }
                        else {
                            Log.d("[GudangGaram]", "list Size = " + lviOpnameLog.size());
                            // call send detail
                            if(itercount > 0) {
                                for (int idx = 0; idx < itercount; idx++) {
                                    Message = "Record " + Integer.toString((idx + 1)) + " of " + Integer.toString(itercount);

                                    Log.d("[GudangGaram]", "============================================================================");
                                    Log.d("[GudangGaram]", "solo_header_id = " + header_id);
                                    Log.d("[GudangGaram]", "Org ID         = " + lviOpnameLog.get(idx).getOrgID());
                                    Log.d("[GudangGaram]", "Org Name       = " + lviOpnameLog.get(idx).getOrgName());
                                    Log.d("[GudangGaram]", "Item ID        = " + lviOpnameLog.get(idx).getItemID());
                                    Log.d("[GudangGaram]", "Item Code      = " + lviOpnameLog.get(idx).getItemCode());
                                    Log.d("[GudangGaram]", "Old Item       = " + lviOpnameLog.get(idx).getItemOld());
                                    Log.d("[GudangGaram]", "Rak            = " + lviOpnameLog.get(idx).getLocRak());
                                    Log.d("[GudangGaram]", "Bin            = " + lviOpnameLog.get(idx).getLocBin());
                                    Log.d("[GudangGaram]", "Comp           = " + lviOpnameLog.get(idx).getLocComp());
                                    Log.d("[GudangGaram]", "Lot No         = " + lviOpnameLog.get(idx).getLotNum());
                                    Log.d("[GudangGaram]", "Koli No        = " + lviOpnameLog.get(idx).getKoliNum());
                                    Log.d("[GudangGaram]", "Block Id       = " + lviOpnameLog.get(idx).getBlockID());
                                    Log.d("[GudangGaram]", "Subinv Status  = " + lviOpnameLog.get(idx).getSubInvStatus());
                                    Log.d("[GudangGaram]", "Qty Onhand     = " + lviOpnameLog.get(idx).getQtyOnHand());
                                    Log.d("[GudangGaram]", "Qty Picked     = " + lviOpnameLog.get(idx).getQtyPicked());
                                    Log.d("[GudangGaram]", "Qty Checked    = " + lviOpnameLog.get(idx).getQtyCheck());
                                    Log.d("[GudangGaram]", "Nik            = " + o_nik);
                                    Log.d("[GudangGaram]", "UOM            = " + lviOpnameLog.get(idx).getUOM());
                                    Log.d("[GudangGaram]", "Note           = " + lviOpnameLog.get(idx).getNote());
                                    Log.d("[GudangGaram]", "Device_ID      = " + o_device_id);

                                    OpnameLogistic ox = new OpnameLogistic();
                                    ox.setOrgID(lviOpnameLog.get(idx).getOrgID());
                                    ox.setOrgName(lviOpnameLog.get(idx).getOrgName());

                                    ox.setItemID(lviOpnameLog.get(idx).getItemID());
                                    ox.setItemOld(lviOpnameLog.get(idx).getItemOld());
                                    ox.setItemCode(lviOpnameLog.get(idx).getItemCode());
                                    ox.setLocRak(lviOpnameLog.get(idx).getLocRak());
                                    ox.setLocBin(lviOpnameLog.get(idx).getLocBin());
                                    ox.setLocComp(lviOpnameLog.get(idx).getLocComp());
                                    ox.setLotNum(lviOpnameLog.get(idx).getLotNum());
                                    ox.setKoliNum(lviOpnameLog.get(idx).getKoliNum());
                                    ox.setBlockID(lviOpnameLog.get(idx).getBlockID());
                                    ox.setSubInvStatus(lviOpnameLog.get(idx).getSubInvStatus());
                                    ox.setQtyOnHand(lviOpnameLog.get(idx).getQtyOnHand());
                                    ox.setQtyPicked(lviOpnameLog.get(idx).getQtyPicked());
                                    ox.setQtyCheck(lviOpnameLog.get(idx).getQtyCheck());
                                    ox.setUOM(lviOpnameLog.get(idx).getUOM());
                                    ox.setNote(lviOpnameLog.get(idx).getNote());

                                    try {
                                        Thread.sleep(300);
                                    }
                                    catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    // send detail info
                                    SoapSendDataOLTaskAsync SoapRequest = new SoapSendDataOLTaskAsync(new SoapSendDataOLTaskAsync.SoapSendDataOLTaskAsyncResponse() {
                                        @Override
                                        public void PostSentAction(String output_id) {
                                            try {
                                                if(output_id.length() > 0) {
                                                        // if reach last item send then clear the list
                                                        if (output_id.equals(Integer.toString(itercount))) {
                                                            Notify_Finish(header_id, org_id, org_name, Rak, Bin,o_nik);
                                                        }
                                                }
                                            }
                                            catch (Exception e) {
                                                Log.d("[GudangGaram]:", "Send Opname Detail Exception " + e.getMessage().toString());
                                            }
                                            finally {
                                                Log.d("[GudangGaram]:", "Send Opname Detail End");
                                            }
                                        }
                                    });
                                    SoapRequest.setAttribute(context, dbHelper, config, header_id, Integer.toString((idx + 1)) ,Integer.toString(itercount), ox, o_nik, o_device_id);
                                    if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                                        //work on sgs3 android 4.0.4
                                        SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                                    }
                                    else {
                                        SoapRequest.execute(); // work on sgs2 android 2.3
                                    }

                                }
                            }


                        }
                    }
                    catch (Exception e) {
                        Log.d("[GudangGaram]", "SoapSendReqOL :: Send > Exception " + e.getMessage().toString());
                    }
                    finally {
                        Log.d("[GudangGaram]", "SoapSendReqOL :: Send > End");
                    }
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, org_id, Rak, Bin, o_nik, o_device_id);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]:", "Send Opname Data End");
        }
    }
}
