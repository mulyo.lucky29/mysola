package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 1/24/2019.
 */

public interface SoapSendVerifyLoginTaskAsyncResponse {
    void PostSentAction(LoginSession output);
}
