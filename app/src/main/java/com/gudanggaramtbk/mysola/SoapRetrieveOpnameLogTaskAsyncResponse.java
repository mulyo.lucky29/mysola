package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 11/5/2018.
 */

public interface SoapRetrieveOpnameLogTaskAsyncResponse {
    void PostSentAction(String output);
}
