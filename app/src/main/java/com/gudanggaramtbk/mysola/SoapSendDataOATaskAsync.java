package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by luckym on 11/23/2018.
 */

public class SoapSendDataOATaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String ResultResponse;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private String header_id;
    private OpnameData ObjOpnameAcc;
    private String nik;
    private String device_id;
    private String seq_total;
    private String seq_no;
    private String lastFlag;

    public SoapSendDataOATaskAsync() {}
    public interface SoapSendDataOATaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapSendDataOATaskAsync.SoapSendDataOATaskAsyncResponse delegate = null;
    public SoapSendDataOATaskAsync(SoapSendDataOATaskAsync.SoapSendDataOATaskAsyncResponse delegate) {
        this.delegate = delegate;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             OpnameData p_ObjOpnameAcc,
                             String     p_nik,
                             String     p_device_id,
                             String     p_lastFlag) {
        Log.d("[GudangGaram]", "SoapSendDataOATaskAsync :: setAttribute");
        this.context      = context;
        this.dbHelper     = dbHelper;
        this.config       = config;
        this.ObjOpnameAcc = p_ObjOpnameAcc;
        this.nik          = p_nik;
        this.device_id    = p_device_id;
        this.lastFlag     = p_lastFlag;

        this.pd = new ProgressDialog(this.context);
        //Result = new ArrayList<DS_TaskItemDetail>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSendDataOATaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Send Data Opname Accounting To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        if (pd.isShowing()) {
            pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
        Log.d("[GudangGaram]", "SoapSendDataOATaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        if(output!=null){
            Log.d("[GudangGaram]", "SoapSendDataOATaskAsync :: onPostExecute >> " + output);
            delegate.PostSentAction(output);
        }
        //Toast.makeText(context, "Send Opname Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        String Result = null;
        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Insert_SOA";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        String xtp_soacc_dtl_id = ObjOpnameAcc.getDtl_ID().toString();
        String xtp_file_id      = ObjOpnameAcc.getFileID().toString();
        String xtp_qty_checked  = ObjOpnameAcc.getQtyCheck().toString();
        String xtp_note         = ObjOpnameAcc.getNotes().toString();
        String xtp_pic_nik      = nik;
        String xtp_device_id    = device_id;

        Log.d("[GudangGaram]", "SoapSendDataOATaskAsync :: Param ");

        // =============== p_soacc_dtl_id ===================
        PropertyInfo prop_p_soacc_detail_id = new PropertyInfo();
        prop_p_soacc_detail_id.setName("p_soacc_detail_id");
        prop_p_soacc_detail_id.setValue(xtp_soacc_dtl_id);
        prop_p_soacc_detail_id.setType(Integer.class);
        Log.d("[GudangGaram]", "xp_soacc_dtl_id      :" + prop_p_soacc_detail_id);

        // =============== p_file_id ===================
        PropertyInfo prop_p_file_id = new PropertyInfo();
        prop_p_file_id.setName("p_file_id");
        prop_p_file_id.setValue(xtp_file_id);
        prop_p_file_id.setType(Integer.class);
        Log.d("[GudangGaram]", "xp_file_id           :" + xtp_file_id);

        // =============== p_qty_checked ===================
        PropertyInfo prop_p_qty_checked = new PropertyInfo();
        prop_p_qty_checked.setName("p_qty_checked");
        prop_p_qty_checked.setValue(xtp_qty_checked);
        prop_p_qty_checked.setType(Double.class);
        Log.d("[GudangGaram]", "xp_qty_checked       :" + xtp_qty_checked);

        // =============== p_note ===================
        PropertyInfo prop_p_note = new PropertyInfo();
        prop_p_note.setName("p_note");
        prop_p_note.setValue(xtp_note);
        prop_p_note.setType(String.class);
        Log.d("[GudangGaram]", "xp_note              :" + xtp_note);

        // =============== p_pic_nik ===================
        PropertyInfo prop_p_pic_nik = new PropertyInfo();
        prop_p_pic_nik.setName("p_pic_nik");
        prop_p_pic_nik.setValue(xtp_pic_nik);
        prop_p_pic_nik.setType(String.class);
        Log.d("[GudangGaram]", "xp_pic_nik           :" + xtp_pic_nik);

        // =============== p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(xtp_device_id);
        prop_p_device_id.setType(String.class);
        Log.d("[GudangGaram]", "xp_device_id         :" + xtp_device_id);

        // =============== p_flag_last ===================
        PropertyInfo prop_p_flag_last = new PropertyInfo();
        prop_p_flag_last.setName("p_flag_last");
        prop_p_flag_last.setValue(lastFlag);
        prop_p_flag_last.setType(String.class);
        Log.d("[GudangGaram]", "xp_flag_last         :" + lastFlag);

        // ===================== add property ============
        request.addProperty(prop_p_soacc_detail_id);
        request.addProperty(prop_p_file_id);
        request.addProperty(prop_p_qty_checked);
        request.addProperty(prop_p_note);
        request.addProperty(prop_p_pic_nik);
        request.addProperty(prop_p_device_id);
        request.addProperty(prop_p_flag_last);

        //Web method call
        try {

            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
            String v_result = resultsRequestSOAP.getProperty(0).toString();

            Result = v_result;

            try {
                Thread.sleep(1800);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        catch (Exception ex) {
            ResultResponse = "||||";
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: end doInBackground");
        }

        return Result;
    }
}