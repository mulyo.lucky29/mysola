package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

/**
 * Created by luckym on 2/1/2019.
 */

public class SoapResetPassword {
        private SharedPreferences   config;
        private Context             context;
        private MySQLiteHelper      dbHelper;
        private LoginChangePassword pActivity;

        // override constructor
        public SoapResetPassword(SharedPreferences PConfig){
            Log.d("[GudangGaram]:", "SoapResetPassword :: Constructor");
            this.config           = PConfig;
        }
        public void setContext(Context context){
            Log.d("[GudangGaram]:", "SoapResetPassword :: SetContext");
            this.context = context;
        }
        public void setParentActivity(LoginChangePassword pActivity){
        this.pActivity = pActivity;
    }
        public void setDBHelper(MySQLiteHelper dbHelper){
            Log.d("[GudangGaram]:", "SoapResetPassword :: SetDBHelper");
            this.dbHelper = dbHelper;
        }

        public void Reset(String p_username, String p_password) {
            Log.d("[GudangGaram]:", "SoapResetPassword :: Reset Begin");
            try {
                // call WS To Retrieve Manifest from oracle and save it to local database table
                SoapResetPasswordTaskAsync SoapRequest = new SoapResetPasswordTaskAsync(new SoapResetPasswordTaskAsync.SoapResetPasswordTaskAsyncResponse() {
                    @Override
                    public void PostSentAction(String p_display_name) {
                    }
                });
                SoapRequest.setAttribute(context, dbHelper, config,p_username, p_password, pActivity);
                if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                    //work on sgs3 android 4.0.4
                    SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                }
                else {
                    SoapRequest.execute(); // work on sgs2 android 2.3
                }
            }
            catch(Exception e){
                e.printStackTrace();
            }
            finally {
                Log.d("[GudangGaram]:", "SoapResetPassword :: Reset End");
            }
        }
}
