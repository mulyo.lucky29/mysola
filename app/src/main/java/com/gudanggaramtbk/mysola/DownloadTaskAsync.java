package com.gudanggaramtbk.mysola;

    import android.app.DownloadManager;
    import android.app.ProgressDialog;
    import android.content.BroadcastReceiver;
    import android.content.Context;
    import android.content.Intent;
    import android.net.Uri;
    import android.os.AsyncTask;
    import android.os.Build;
    import android.os.Environment;
    import android.support.v4.content.FileProvider;
    import android.util.Log;
    import java.io.BufferedInputStream;
    import java.io.File;
    import java.io.FileOutputStream;
    import java.io.IOException;
    import java.io.InputStream;
    import java.io.OutputStream;
    import java.net.HttpURLConnection;
    import java.net.URL;
    import java.net.URLConnection;


public class DownloadTaskAsync extends AsyncTask<String, Void, String> {
    private ProgressDialog pd;
    Context context;

    public void setContext(Context context){
        this.context = context;
        this.pd = new ProgressDialog(this.context);
    }
    public interface DownloadTaskAsyncResponse {
        void PostDownloadAction(String output);
    }
    public DownloadTaskAsyncResponse delegate = null;
    public DownloadTaskAsync(DownloadTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pd.setMessage("Downloading APK to SD Card ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String xstatus) {
        pd.dismiss();
        delegate.PostDownloadAction(xstatus);
    }

    @Override
    protected String doInBackground(String... params) {
        int    Result;
        int    count, size;
        long   ApkSize;
        String apkurl,apkloc;
        long   apksize;
        String apkbaseurl;
        String outputpath;

        apkloc = params[0] + "/app-release.apk";
        count = params.length;

        if(count > 0){
            //ApkSize = GetDownloadAPK(params[0], outputpath);
            outputpath = Environment.getExternalStorageDirectory().toString() + File.separator;
            size = 0;
            try {
                URL url = new URL(apkloc);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();
                Log.d("[GudangGaram]: ", "Download apk size = " + Integer.toString(lenghtOfFile));
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                // Output stream
                OutputStream output = new FileOutputStream(outputpath + "app-release.apk");
                Log.d("[GudangGaram]: " ,"copy apk to " + outputpath + "app-release.apk");

                byte data[] = new byte[1024];
                while ((count = input.read(data)) != -1) {
                    size += count;
                    Log.d("[GudangGaram]: ", "Size :" + size);
                    output.write(data, 0, count);
                }
                Log.d("[GudangGaram]: ", "APK Downloaded");
                output.flush();
                output.close();
                input.close();

            }
            catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
        }

        return "Finish";
    }

}
