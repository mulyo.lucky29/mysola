package com.gudanggaramtbk.mysola;

/**
 * Created by luckym on 10/24/2018.
 */

public interface DownloadTaskAsyncResponse {
    void PostDownloadAction(String output);
}
