package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 11/5/2018.
 */

public class OpnameLogistic {
    private String  ItemID;
    private String  ItemCode;
    private String  ItemOld;
    private String  ItemDesc;
    private String  OrgID;
    private String  OrgName;
    private String  LocRak;
    private String  LocBin;
    private String  LocComp;
    private String  RakSeq;
    private String  BinSeq;
    private String  LotNum;
    private String  KoliNum;
    private String  BlockID;
    private String  SubInvStatus;
    private Double  QtyOnHand;
    private Double  QtyPicked;
    private Double  QtyOhmp;
    private Double  QtyCheck;
    private String  UOM;
    private Boolean Checked;
    private String  Note;
    private Boolean Note_Enabled;

    // ---------- getter --------------
    public String getItemID() {
        return ItemID;
    }
    public String getItemCode() {
        return ItemCode;
    }
    public String getItemOld() {
        return ItemOld;
    }
    public String getItemDesc() {
        return ItemDesc;
    }
    public String getOrgID() {
        return OrgID;
    }
    public String getOrgName() {
        return OrgName;
    }
    public String getLocRak() {
        return LocRak;
    }
    public String getLocBin() {
        return LocBin;
    }
    public String getLocComp() {
        return LocComp;
    }
    public String getRakSeq() {
        return RakSeq;
    }
    public String getBinSeq() {
        return BinSeq;
    }
    public String getLotNum() {
        return LotNum;
    }
    public String getKoliNum() {
        return KoliNum;
    }
    public String getBlockID() {
        return BlockID;
    }
    public String getSubInvStatus() {
        return SubInvStatus;
    }
    public Double getQtyOnHand() {
        return QtyOnHand;
    }
    public Double getQtyPicked() {
        return QtyPicked;
    }
    public String getUOM() {
        return UOM;
    }
    public Double getQtyCheck() {
        return QtyCheck;
    }
    public Boolean getChecked() {
        return Checked;
    }
    public String getNote() {
        return Note;
    }
    public Double getQtyOhmp() {
        return QtyOhmp;
    }
    public Boolean getNote_Enabled() {
        return Note_Enabled;
    }

    // ---------- setter --------------
    public void setItemID(String itemID) {
        ItemID = itemID;
    }
    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }
    public void setItemOld(String itemOld) {
        ItemOld = itemOld;
    }
    public void setItemDesc(String itemDesc) {
        ItemDesc = itemDesc;
    }
    public void setLocRak(String locRak) {
        LocRak = locRak;
    }
    public void setOrgID(String orgID) {
        OrgID = orgID;
    }
    public void setOrgName(String orgName) {
        OrgName = orgName;
    }
    public void setLocBin(String locBin) {
        LocBin = locBin;
    }
    public void setLocComp(String locComp) {
        LocComp = locComp;
    }
    public void setRakSeq(String rakSeq) {
        RakSeq = rakSeq;
    }
    public void setBinSeq(String binSeq) {
        BinSeq = binSeq;
    }
    public void setLotNum(String lotNum) {
        LotNum = lotNum;
    }
    public void setKoliNum(String koliNum) {
        KoliNum = koliNum;
    }
    public void setBlockID(String blockID) {
        BlockID = blockID;
    }
    public void setSubInvStatus(String subInvStatus) {
        SubInvStatus = subInvStatus;
    }
    public void setQtyOnHand(Double qtyOnHand) {
        QtyOnHand = qtyOnHand;
    }
    public void setQtyPicked(Double qtyPicked) {
        QtyPicked = qtyPicked;
    }
    public void setUOM(String UOM) {
        this.UOM = UOM;
    }
    public void setQtyCheck(Double qtyCheck) {
        QtyCheck = qtyCheck;
    }
    public void setChecked(Boolean checked) {
        Checked = checked;
    }
    public void setNote(String note) {
        Note = note;
    }
    public void setQtyOhmp(Double qtyOhmp) {
        QtyOhmp = qtyOhmp;
    }
    public void setNote_Enabled(Boolean note_Enabled) {
        Note_Enabled = note_Enabled;
    }

}
