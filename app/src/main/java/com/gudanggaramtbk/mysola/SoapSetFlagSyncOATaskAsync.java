package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by luckym on 11/16/2018.
 */

public class SoapSetFlagSyncOATaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String ResultResponse;
    private MySQLiteHelper dbHelper;
    private String SentResponse;

    private String file_id;
    private String nik;
    private String device_id;

    public SoapSetFlagSyncOATaskAsync() {
    }
    public interface SoapSetFlagSyncOATaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapSetFlagSyncOATaskAsync.SoapSetFlagSyncOATaskAsyncResponse delegate = null;
    public SoapSetFlagSyncOATaskAsync(SoapSetFlagSyncOATaskAsync.SoapSetFlagSyncOATaskAsyncResponse delegate) {
        this.delegate = delegate;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String p_file_id,
                             String p_pic_nik,
                             String p_device_id) {
        Log.d("[GudangGaram]", "SoapSetFlagSyncOATaskAsync :: setAttribute");
        this.context      = context;
        this.dbHelper     = dbHelper;
        this.config       = config;
        this.file_id      = p_file_id;
        this.nik          = p_pic_nik;
        this.device_id    = p_device_id;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSetFlagSyncOATaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Set Flag To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        pd.setMessage(args[0].toString());
        Log.d("[GudangGaram]", "SoapSetFlagSyncOATaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapSetFlagSyncOATaskAsync :: onPostExecute >> " + ResultResponse);
        delegate.PostSentAction(ResultResponse);
    }

    @Override
    protected String doInBackground(String... params) {
        Long Result;
        Integer ncount;
        String v_res_file_id = "-1";

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Set_Flag_Sync_Acc";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        // =============== p_file_id ===================
        PropertyInfo prop_p_file_id = new PropertyInfo();
        prop_p_file_id.setName("p_file_id");
        prop_p_file_id.setValue(file_id);
        prop_p_file_id.setType(String.class);

        // =============== p_pic_nik ===================
        PropertyInfo prop_p_pic_nik = new PropertyInfo();
        prop_p_pic_nik.setName("p_pic_nik");
        prop_p_pic_nik.setValue(nik);
        prop_p_pic_nik.setType(String.class);

        request.addProperty(prop_p_file_id);
        request.addProperty(prop_p_pic_nik);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
            v_res_file_id = resultsRequestSOAP.getProperty(0).toString();
            ResultResponse = v_res_file_id;
        } catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        } finally {
            Log.d("[GudangGaram]", "SoapSetFlagSyncOATaskAsync :: end doInBackground");
        }

        return "";
    }
}

