package com.gudanggaramtbk.mysola;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.List;

public class SyncAction extends AppCompatActivity {
    private SharedPreferences        config;
    private Context                  context;
    private MySQLiteHelper           dbHelper;
    private SoapRetrievePIC          downloadPIC;
    private SoapRetrieveOpnameAcc    populateOpnameAcc;
    private SoapRetrieveOpnameAccDtl downloadOpname;

    private Button                   Cmd_DMPIC;
    private Button                   Cmd_DOAD;

    private String                   StrSessionNIK;
    private String                   StrSessionName;
    private String                   StrSessionGroup;

    private String                   DeviceID;
    private ListView                 List_Down_Acc;
    private Message                  msg;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Sync Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NIK",   StrSessionNIK);
                    upIntent.putExtra("PIC_NAME",  StrSessionName);
                    upIntent.putExtra("PIC_GROUP", StrSessionGroup);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_action);
        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        context = this;

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // get information intent
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
        StrSessionGroup = getIntent().getStringExtra("PIC_GROUP");

        DeviceID = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        Message msg = new Message(context);

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        List_Down_Acc    = (ListView) findViewById(R.id.list_oad);
        List_Down_Acc.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        // initialize soap downloader master pic
        downloadPIC = new SoapRetrievePIC(config);
        downloadPIC.setContext(context);
        downloadPIC.setDBHelper(dbHelper);

        // service to render list task
        populateOpnameAcc = new SoapRetrieveOpnameAcc(config, List_Down_Acc);
        populateOpnameAcc.setParentActivity(SyncAction.this);
        populateOpnameAcc.setContext(context);
        populateOpnameAcc.setDBHelper(dbHelper);

        // service to download selected list item detail
        downloadOpname = new SoapRetrieveOpnameAccDtl(config);
        downloadOpname.setParentActivity(this);
        downloadOpname.setContext(context);
        downloadOpname.setDBHelper(dbHelper);


        Cmd_DMPIC  = (Button)findViewById(R.id.cmd_down_mpic);
        Cmd_DMPIC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SYNC_M_PIC();
            }
        });

        Cmd_DOAD   = (Button)findViewById(R.id.cmd_down_oad);
        Cmd_DOAD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_LOAD_LIST_DOWN_ACC(config, context, StrSessionNIK, DeviceID);
            }
        });

        // ========= menu item restriction ==============
        if(StrSessionNIK.equals("999999999")){
            // kalo default
            Cmd_DMPIC.setEnabled(true);
            Cmd_DOAD.setEnabled(false);
        }
        else{
            // kalo login pake nik
            Cmd_DMPIC.setEnabled(true);
            Cmd_DOAD.setEnabled(true);
        }

    }

    public void DialogBox(String pTitle, String pMessage){
        msg.Show(pTitle, pMessage);
    }

    public void EH_CMD_LOAD_LIST_DOWN_ACC(SharedPreferences pconfig, Context pcontext, String pStrSessionNIK, String pStrDeviceID){
        Log.d("[GudangGaram]", "SyncAction :: EH_CMD_LOAD_LIST_DOWN_ACC");
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(dbHelper.isServiceConnect(pconfig, cm, pcontext)){
            try{
                populateOpnameAcc.Retrieve(pStrSessionNIK,pStrDeviceID);
            }
            catch(Exception e){
            }
            finally {
            }
        }
        else{
            msg.Show("Service Unavailable", "Please Check Connection");
            //Toast.makeText(context,"Connection Problem ", Toast.LENGTH_LONG).show();
        }
    }

    public void EH_CMD_DOWNLOAD_LIST_ACC(String FileID, String NIK){
        // call web service to download list and save to database
        // call web service to flag
        msg.Show("EH_CMD_DOWNLOAD_LIST_ACC", "File ID = " + FileID + " PIC NIK = " + NIK);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(dbHelper.isServiceConnect(config, cm, context)){
            try{
                downloadOpname.Retrieve(FileID, NIK,DeviceID);
            }
            catch(Exception e){
            }
            finally {
            }
        }
        else{
            msg.Show("Service Unavailable", "Please Check Connection");
        }
    }

    public void EH_CMD_SYNC_M_PIC(){
        int pid;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(dbHelper.isServiceConnect(config, cm, context)){
            try{
                downloadPIC.Retrieve();
            }
            catch(Exception e){
            }
            finally {
            }
        }
        else{
            msg.Show("Service Unavailable", "Please Check Connection");
        }
    }
}
