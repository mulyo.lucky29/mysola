package com.gudanggaramtbk.mysola;

    import android.app.TaskStackBuilder;
    import android.content.Intent;
    import android.content.SharedPreferences;
    import android.support.v4.app.NavUtils;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.util.Log;
    import android.view.MenuItem;
    import android.view.View;
    import android.widget.Button;

public class AboutActivity extends AppCompatActivity {
    private DownloadUpdate updaterApk;
    private SharedPreferences config;
    private Button CmdUpdateApk;
    private String                   StrSessionNIK;
    private String                   StrSessionName;
    private String                   StrSessionGroup;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed About Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NIK",  StrSessionNIK);
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    upIntent.putExtra("PIC_GROUP", StrSessionGroup);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        StrSessionName   = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK    = getIntent().getStringExtra("PIC_NIK");
        StrSessionGroup  = getIntent().getStringExtra("PIC_GROUP");

        // initialize updater apk
        updaterApk = new DownloadUpdate(config);
        updaterApk.setContext(this);

        CmdUpdateApk = (Button)findViewById(R.id.cmd_updateAPK);
        CmdUpdateApk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updaterApk.DownloadApk();
            }
        });

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }
}

