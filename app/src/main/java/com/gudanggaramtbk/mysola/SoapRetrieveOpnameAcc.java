package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;

/**
 * Created by LuckyM on 11/15/2018.
 */

public class SoapRetrieveOpnameAcc {
    private SharedPreferences config;
    private Context           context;
    private MySQLiteHelper    dbHelper;
    private ListView          Lst;
    private SyncAction        pActivity;
    // override constructor
    public SoapRetrieveOpnameAcc(SharedPreferences PConfig, ListView pLst){
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAcc :: Constructor");
        this.config = PConfig;
        this.Lst    = pLst;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAcc  :: SetContext");
        this.context = context;
    }
    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAcc  :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public String Retrieve(String pNik, String pDeviceID) {
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAcc  :: Retrieve");
        try {
            SoapRetrieveOpnameAccTaskAsync SoapRequest = new SoapRetrieveOpnameAccTaskAsync(new SoapRetrieveOpnameAccTaskAsync.SoapRetrieveOpnameAccTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {

                }
            });
            SoapRequest.setParentActivity(pActivity);
            SoapRequest.setAttribute(context, dbHelper, config, Lst, pNik, pDeviceID);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
        }
        return "";
    }
}
