package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 11/15/2018.
 */

public class OpnameListAcc {
    private String TaskID;
    private String FileID;
    private String FileName;
    private String OpnameCode;
    private String AssignedDate;



    private String AssignedNIK;

    // ------------------ getter -------------------
    public String getTaskID() {
        return TaskID;
    }
    public String getFileID() {
        return FileID;
    }
    public String getFileName() {
        return FileName;
    }
    public String getOpnameCode() {
        return OpnameCode;
    }
    public String getAssignedDate() {
        return AssignedDate;
    }
    public String getAssignedNIK() {
        return AssignedNIK;
    }


    // ----------------- setter --------------------
    public void setTaskID(String taskID) {
        TaskID = taskID;
    }
    public void setFileID(String fileID) {
        FileID = fileID;
    }
    public void setFileName(String fileName) {
        FileName = fileName;
    }
    public void setOpnameCode(String opnameCode) {
        OpnameCode = opnameCode;
    }
    public void setAssignedDate(String assignedDate) {
        AssignedDate = assignedDate;
    }
    public void setAssignedNIK(String assignedNIK) {
        AssignedNIK = assignedNIK;
    }
}
