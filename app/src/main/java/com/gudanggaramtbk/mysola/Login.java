package com.gudanggaramtbk.mysola;

    import android.Manifest;
    import android.content.Context;
    import android.content.Intent;
    import android.content.SharedPreferences;
    import android.content.pm.PackageManager;
    import android.net.ConnectivityManager;
    import android.net.NetworkInfo;
    import android.os.Build;
    import android.os.CountDownTimer;
    import android.os.Handler;
    import android.support.v4.app.ActivityCompat;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.util.Log;
    import android.view.View;
    import android.widget.Button;
    import android.widget.Spinner;
    import android.widget.TextView;
    import android.widget.Toast;

    import org.ksoap2.transport.HttpTransportSE;

    import java.io.IOException;
    import java.net.HttpURLConnection;
    import java.net.InetAddress;
    import java.net.MalformedURLException;
    import java.net.SocketTimeoutException;
    import java.net.URL;
    import java.net.URLConnection;


public class Login extends AppCompatActivity {
    private String StrSessionNIK;
    private String StrSessionName;
    private String StrSessionGroup;

    private static Spinner  Cbo_pic_nik;
    private static TextView Txt_pic_nik;
    private static TextView Txt_pic_name;
    private static TextView Txt_pic_password;
    private static TextView Txt_message;


    private static Button   Cmd_Login;
    private static Button   Cmd_Exit;
    private MySQLiteHelper  dbHelper;
    private SharedPreferences config;
    boolean doubleBackToExitPressedOnce         = false;
    private static final int REQUEST_PERMISSION = 1;
    private LoginSession ols;

    private SoapSendVerifyLogin     VerifyLogin;
    private SoapRetrieveName        GetNameLogin;

    private static String[] PERMISSIONS_APPS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    void check_apps_permission() {
        // popup permission granted
        int permission = ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        // if dont have permit before then popup
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    Login.this,
                    PERMISSIONS_APPS,
                    REQUEST_PERMISSION
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);

        Log.d("[GudangGaram]", "Login :: onCreate");
        // check application permission
        check_apps_permission();

        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();


        // service to render list task
        VerifyLogin = new SoapSendVerifyLogin(config);
        VerifyLogin.setParentActivity(Login.this);
        VerifyLogin.setContext(Login.this);
        VerifyLogin.setDBHelper(dbHelper);

        // service to lookup username to get user login name
        GetNameLogin = new SoapRetrieveName(config);
        GetNameLogin.setParentActivity(Login.this);
        GetNameLogin.setContext(Login.this);
        GetNameLogin.setDBHelper(dbHelper);

        //Cbo_pic_nik      = (Spinner)findViewById(R.id.cbo_nik_pic);
        Txt_pic_nik      = (TextView) findViewById(R.id.txt_nik_pic);
        Txt_pic_name     = (TextView) findViewById(R.id.txt_nama_pic);
        Txt_pic_password = (TextView) findViewById(R.id.txt_password);
        Txt_message      = (TextView) findViewById(R.id.txt_message);

        Cmd_Login        = (Button)   findViewById(R.id.cmd_login);
        Cmd_Exit         = (Button)   findViewById(R.id.cmd_exit);

        try{
            StrSessionName  = getIntent().getStringExtra("PIC_NAME");
            StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
            StrSessionGroup = getIntent().getStringExtra("PIC_GROUP");

            // set nik and name from change login
            Txt_pic_nik.setText(StrSessionNIK);
            Txt_pic_name.setText(StrSessionName);
        }
        catch( NullPointerException e){
        }


        Cmd_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // redirect to main page
                EH_cmd_login();
            }
        });

        Cmd_Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Build.VERSION.SDK_INT>=16 && Build.VERSION.SDK_INT<21){
                    finishAffinity();
                } else if(Build.VERSION.SDK_INT>=21){
                    finishAndRemoveTask();
                }
            }
        });

        Txt_pic_nik.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Txt_pic_name.setText("");
                    if(Txt_pic_nik.getText().toString().equals("")){
                        display_error("Username Should Be Filled");
                    }
                    else{
                        if((Txt_pic_nik.getText().toString().equals("999999999"))){
                            Txt_pic_name.setText("Default");
                        }
                        else{
                            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            if(dbHelper.isServiceConnect(config,cm,Login.this) == true){
                                 try {
                                        GetNameLogin.Retrieve(Txt_pic_nik.getText().toString());
                                 }
                                 catch (Exception e) {
                                      display_error(e.getMessage().toString());
                                 }
                            }
                            else{
                                 display_error("Service Unavailable");
                            }
                        }
                    }
                }
            }
        });

        // init combo nik pic
        //initKVPComboBox(R.id.cbo_nik_pic);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public void display_name(final String p_name){
        try{
            Txt_pic_name.setText(p_name);
        }
        catch(Exception e){
        }
    }

    public void display_error(final String errm){
        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
                Txt_message.setText(errm);
                Txt_pic_password.setText("");
                Txt_pic_password.setFocusable(true);
            }
            public void onFinish() {
                Txt_message.setText("");
            }
        }.start();
    }

    public void go_main_page(String privil_group){
        Log.d("[GudangGaram]", "Login :: go_main_page");
        try{
            Intent Main_intent = new Intent(this.getBaseContext(), MainActivity.class);
            //Main_intent.putExtra("PIC_NIK",  Cbo_pic_nik.getSelectedItem().toString());
            Main_intent.putExtra("PIC_NIK",  Txt_pic_nik.getText().toString());
            Main_intent.putExtra("PIC_NAME", Txt_pic_name.getText().toString());
            Main_intent.putExtra("PIC_GROUP", privil_group);
            startActivity(Main_intent);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "Login :: go_main_page Exception : "+ e.getMessage().toString());
        }
    }

    public void go_change_password_page(String privil_group){
        Log.d("[GudangGaram]", "Login :: go_change_password_page");
        try{
            Intent chpwd_intent = new Intent(this.getBaseContext(), LoginChangePassword.class);
            chpwd_intent.putExtra("PIC_NIK",  Txt_pic_nik.getText().toString());
            chpwd_intent.putExtra("PIC_NAME", Txt_pic_name.getText().toString());
            chpwd_intent.putExtra("PIC_GROUP", privil_group);
            startActivity(chpwd_intent);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "Login :: go_change_password_page Exception : "+ e.getMessage().toString());
        }
    }

    public void set_LoginSession(LoginSession olx){
        String loginSession;

        Log.d("[GudangGaram]", "Login :: Result  : " + olx.getLoginResult());
        Log.d("[GudangGaram]", "Login :: Message : " + olx.getLoginMessage());
        Log.d("[GudangGaram]", "Login :: Name    : " + olx.getLoginName());
        Log.d("[GudangGaram]", "Login :: Group   : " + olx.getLoginGroup());
        Log.d("[GudangGaram]", "Login :: Reset   : " + olx.getLoginResetFlag());

        loginSession = olx.getLoginResult();
        if(loginSession.equals("S")){
            // if success then bring em to main home
            go_main_page(olx.getLoginGroup());
        }
        else if(loginSession.equals("R")){
            // if account is reset then bring em to change password page
            go_change_password_page(olx.getLoginGroup());
        }
        else{
            display_error(olx.getLoginMessage());
            Log.d("[GudangGaram]", "Login :: Message : " + olx.getLoginMessage());
        }
    }

    public void EH_cmd_login(){
        String v_txt_pic_nik;
        String v_txt_pic_name;
        String v_txt_pic_password;

        //v_txt_pic_nik      = Cbo_pic_nik.getSelectedItem().toString();
        v_txt_pic_nik      = Txt_pic_nik.getText().toString();
        v_txt_pic_name     = Txt_pic_name.getText().toString();
        v_txt_pic_password = Txt_pic_password.getText().toString();

        if(v_txt_pic_nik.equals("")){
            display_error("Username Should Be Filled");
            Txt_pic_nik.requestFocus();
        }
        else{
            if(v_txt_pic_password.equals("")){
                display_error("Password Should Be Filled");
                Txt_pic_password.requestFocus();
            }
            else{
                if(v_txt_pic_nik.equals("999999999")){
                    go_main_page("DEF");
                }
                else{
                    if(v_txt_pic_password.length() == 8){
                        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        if(dbHelper.isServiceConnect(config,cm,Login.this) == true){
                            try{
                                    VerifyLogin.Verify(v_txt_pic_nik,v_txt_pic_password);
                            }
                            catch(Exception e){
                                display_error(e.getMessage().toString());
                            }
                        }
                        else{
                            display_error("Service Unreachable");
                        }
                    }
                    else{
                        display_error("Password Length Should Be 8 Digits");
                    }
                }
            }
        }
    }

    /*
    // not used sudah di replace dengan textbox
    private void initKVPComboBox(final int p_layout){
        Spinner spinner = (Spinner) findViewById(p_layout);
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();
        lvi = dbHelper.getEmployeeList();

        // Creating adapter for spinner
        ArrayAdapter<StringWithTag> dataAdapter = new ArrayAdapter<StringWithTag>(this, android.R.layout.simple_spinner_item, lvi);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringWithTag swt = (StringWithTag) parent.getItemAtPosition(position);
                String        key = (String) swt.tag;

                Log.d("[GudangGaram]", " KVP Value Selected >> " + swt);
                Log.d("[GudangGaram]", " KVP Key   Selected >> " + key.toString());

                Txt_pic_name.setText(key.toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    */
}

