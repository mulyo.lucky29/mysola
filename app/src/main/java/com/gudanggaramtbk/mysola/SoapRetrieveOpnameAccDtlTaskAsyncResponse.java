package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 11/16/2018.
 */

public interface SoapRetrieveOpnameAccDtlTaskAsyncResponse {
    void PostSentAction(String output);
}
