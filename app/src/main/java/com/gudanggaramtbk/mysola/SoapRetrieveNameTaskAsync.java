package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by luckym on 1/31/2019.
 */

public class SoapRetrieveNameTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences         config;
    private Context                   context;
    private ProgressDialog            pd;
    private String                    ResultResponse;
    private MySQLiteHelper            dbHelper;
    private String                    p_nik;
    private String                    str_name_result;
    private Login                     pActivity;

    public SoapRetrieveNameTaskAsync(){}
    public interface SoapRetrieveNameTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveNameTaskAsync.SoapRetrieveNameTaskAsyncResponse delegate = null;
    public SoapRetrieveNameTaskAsync(SoapRetrieveNameTaskAsync.SoapRetrieveNameTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper    dbHelper,
                             SharedPreferences config,
                             String            p_nik,
                             Login             pActivity){
        Log.d("[GudangGaram]", "SoapRetrieveNameTaskAsync :: setAttribute");
        this.context          = context;
        this.dbHelper         = dbHelper;
        this.config           = config;
        this.p_nik            = p_nik;
        this.pActivity        = pActivity;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveNameTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve List Name From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        pd.setMessage(args[0].toString());
        Log.d("[GudangGaram]", "SoapRetrieveNameTaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveNameTaskAsync :: onPostExecute >> " + str_name_result);
        try{
            pActivity.display_name(str_name_result);

            delegate.PostSentAction(str_name_result);
        }
        catch(Exception e){
        }
        finally {
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Get_Login_Name";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        // =============== p_username ===================
        PropertyInfo prop_p_username = new PropertyInfo();
        prop_p_username.setName("p_username");
        prop_p_username.setValue(p_nik);
        prop_p_username.setType(String.class);


        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        request.addProperty(prop_p_username);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            //HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,3000);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
            str_name_result = resultsRequestSOAP.getProperty(0).toString();
        }
        catch (Exception ex) {
                Log.d("[GudangGaram]", "SoapRetrieveNameTaskAsync Catch Http Transport : " + ex.getMessage().toString());
                Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        finally {
                Log.d("[GudangGaram]", "SoapRetrieveNameTaskAsync  :: end doInBackground");
        }

        return "";
    }
}
