package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 11/5/2018.
 */

public class SoapRetrieveGudangTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences    config;
    private Context              context;
    private ProgressDialog       pd;
    private MySQLiteHelper       dbHelper;
    private String               SentResponse;
    private Spinner              OCbo_Gudang;
    private TextView             OTxt_GudangID;
    private ListView             listBinPersiapan;
    private String               gudang_id, gudang_name;
    private List<StringWithTag>  ResultGudang;


    private ArrayAdapter<String> adapter;

    public SoapRetrieveGudangTaskAsync(){
    }

    public interface SoapRetrieveGudangTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveGudangTaskAsync.SoapRetrieveGudangTaskAsyncResponse delegate = null;
    public SoapRetrieveGudangTaskAsync(SoapRetrieveGudangTaskAsync.SoapRetrieveGudangTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }

    public void setAttribute(Context context, MySQLiteHelper dbHelper, SharedPreferences config, Spinner Cbo_Gudang, TextView Txt_GudangID){
        Log.d("[GudangGaram]", "SoapRetrieveGudangTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.pd                = new ProgressDialog(this.context);
        ResultGudang           = new ArrayList<StringWithTag>();
        this.OCbo_Gudang       = Cbo_Gudang;
        this.OTxt_GudangID     = Txt_GudangID;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveGudangTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait Retrieving Gudang From Oracle Inprogress ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    protected  void onProgressUpdate(String[] args) {
        //progressDialog.setMessage(args[0]);
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveGudang TaskAsync :: onPostExecute >> " + SentResponse);
        try{
            delegate.PostSentAction(output);
            initKVPComboBox(OCbo_Gudang, OTxt_GudangID);
        }
        catch(Exception e){
        }
        Toast.makeText(context,"Retrieve Gudang Done", Toast.LENGTH_LONG).show();
    }


    private void initKVPComboBox(Spinner tmpCbo, TextView tmpTxt){
        Spinner  spinner   = tmpCbo;
        final TextView textLabel = tmpTxt;

        List<StringWithTag> lvi = new ArrayList<StringWithTag>();
        //lvi = dbHelper.getEmployeeListLoad();
        lvi = ResultGudang;
        // Creating adapter for spinner
        ArrayAdapter<StringWithTag> dataAdapter = new ArrayAdapter<StringWithTag>(context, android.R.layout.simple_spinner_item, lvi);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringWithTag swt = (StringWithTag) parent.getItemAtPosition(position);
                String        key = (String) swt.tag;

                Log.d("[GudangGaram]", " Load By KVP Value Selected >> " + swt);
                Log.d("[GudangGaram]", " Load By KVP Key   Selected >> " + key.toString());

                textLabel.setText(key.toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;
        //String response = null;
        String rak_persiapan = "";

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Gudang";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                for (int i = 0; i < obj2.getPropertyCount();i++){
                    obj3 = (SoapObject) obj2.getProperty(i);

                    gudang_id   = obj3.getProperty(0).toString();
                    gudang_name = obj3.getProperty(1).toString();

                    ResultGudang.add(new StringWithTag(gudang_name,gudang_id));
                    Log.d("[GudangGaram]", "Org ID    :" + gudang_id);
                    Log.d("[GudangGaram]", "Org Name  :" + gudang_name);
                }
            }
            catch(NullPointerException e){
                Toast.makeText(context, "Connection Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
        }
        Log.d("[GudangGaram]", "SoapRetrieveGudangTaskAsync  :: end doInBackground");

        return "";
    }
}


