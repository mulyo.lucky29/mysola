package com.gudanggaramtbk.mysola;

/**
 * Created by luckym on 1/31/2019.
 */

public interface SoapRetrieveNameTaskAsyncResponse {
    void PostSentAction(String output);
}
