package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;

/**
 * Created by LuckyM on 11/16/2018.
 */

public class SoapRetrieveOpnameAccDtl {
    private SharedPreferences                 config;
    private Context                           context;
    private MySQLiteHelper                    dbHelper;
    private SyncAction                        pActivity;

    // override constructor
    public SoapRetrieveOpnameAccDtl(SharedPreferences PConfig){
        this.config = PConfig;
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtl :: Constructor");
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtl :: SetContext");
        this.context = context;
    }
    public void setParentActivity(SyncAction opActivity){
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtl :: SetParentActivity");
        this.pActivity = opActivity;
    }

    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtl :: SetDBHelper");
        this.dbHelper = dbHelper;
    }
    public String Retrieve(final String pFileID, final String pNik, final String pDeviceID) {
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtl :: Retrieve");
        try {
            SoapRetrieveOpnameAccDtlTaskAsync SoapRequest = new SoapRetrieveOpnameAccDtlTaskAsync(new SoapRetrieveOpnameAccDtlTaskAsync.SoapRetrieveOpnameAccDtlTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {
                    Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtl :: file_id = " + output);
                    // flag to database
                    SoapSetFlagDownloadOATaskAsync SoapFlagRequest = new SoapSetFlagDownloadOATaskAsync(new SoapSetFlagDownloadOATaskAsync.SoapSetFlagDownloadOATaskAsyncResponse() {
                        @Override
                        public void PostSentAction(String output) {
                            // summon to refresh list
                            try{

                                pActivity.EH_CMD_LOAD_LIST_DOWN_ACC(config, context,pNik, pDeviceID);
                            }
                            catch(Exception e){
                                Log.d("[GudangGaram]", "Exception Refresh " + e.getMessage().toString());
                            }
                        }
                    });
                    SoapFlagRequest.setAttribute(context, dbHelper, config,pFileID,pNik,pDeviceID);
                    if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                        //work on sgs3 android 4.0.4
                        //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                        SoapFlagRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                    }
                    else {
                        SoapFlagRequest.execute(); // work on sgs2 android 2.3
                    }
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, pFileID, pNik);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
        }
        return "";
    }
}
