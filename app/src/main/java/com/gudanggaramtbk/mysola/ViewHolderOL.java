package com.gudanggaramtbk.mysola;

import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by LuckyM on 11/6/2018.
 */

public class ViewHolderOL {
    public TextView XNo;
    public TextView XID;
    public CheckBox Xchk_select;
    public TextView Xtxt_ItemID;
    public TextView Xtxt_ItemCode;
    public TextView Xtxt_ItemDesc;
    public TextView Xtxt_OldItem;
    public TextView Xtxt_SubInvStatus;
    public TextView Xtxt_comp;
    public TextView Xtxt_lotNo;
    public TextView Xtxt_koliNo;
    //public TextView Xtxt_qty;
    public TextView Xtxt_qty_ohmp;
    public TextView Xtxt_qty_onhand;
    public TextView Xtxt_qty_picked;
    public EditText Xtxt_qtyCheck;
    public TextView Xtxt_BlockID;
    public TextView Xtxt_uom;
    public EditText Xtxt_Note;
}

