package com.gudanggaramtbk.mysola;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by LuckyM on 11/15/2018.
 */

public class ViewHolderDOA {
    public TextView Xlbl_oad_no;
    public TextView Xlbl_oad_kodeOpnam;
    public TextView Xlbl_oad_fileID;
    public TextView Xlbl_oad_assigned_date;
    public TextView Xlbl_oad_assigned_nik;
    public ImageButton Xcmd_oad_download;
}
