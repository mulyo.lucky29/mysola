package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 11/9/2018.
 */

public interface SoapSendDataOLTaskAsyncResponse {
    void PostSentAction(String output);
}
