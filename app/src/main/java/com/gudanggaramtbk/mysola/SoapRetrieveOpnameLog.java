package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 11/5/2018.
 */

public class SoapRetrieveOpnameLog {
    private SharedPreferences config;
    private Context           context;
    private MySQLiteHelper    dbHelper;
    private ListView          Lst;

    // override constructor
    public SoapRetrieveOpnameLog(SharedPreferences PConfig, ListView pLst){
        Log.d("[GudangGaram]: ", "SoapRetrieveOpnameLog Constructor");
        this.config = PConfig;
        this.Lst    = pLst;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]: ", "SetContext");
        this.context = context;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]: ", "SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public String Retrieve(String pOrgID, String pRak, String pBin) {
        try {
            SoapRetrieveOpnameLogTaskAsync SoapRequest = new SoapRetrieveOpnameLogTaskAsync(new SoapRetrieveOpnameLogTaskAsync.SoapRetrieveOpnameLogTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, Lst, pOrgID,pRak,pBin);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
        }
        return "";
    }
}
