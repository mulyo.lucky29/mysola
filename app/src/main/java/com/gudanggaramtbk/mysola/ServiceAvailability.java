package com.gudanggaramtbk.mysola;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by LuckyM on 2/4/2019.
 */

public class ServiceAvailability implements Runnable {
    private volatile boolean available;

    @Override
    public void run() {
        available = false;
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL("http://myservice.com").openConnection();
            connection.setConnectTimeout(3000);
            connection.setReadTimeout(1000);
            connection.setRequestMethod("HEAD");

            int responseCode = connection.getResponseCode();
            if (200 <= responseCode && responseCode <= 399) {
                available = true;
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public boolean isAvailable() {

        return available;
    }
}