package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by luckym on 11/23/2018.
 */

public class CustomListAdapterHAcc extends ArrayAdapter<OpnameHAcc> {
    Context                   mcontext;
    List<OpnameHAcc>          ListOpnameHAcc;
    ListView                  olistbatch;
    private SharedPreferences config;
    private MySQLiteHelper    dbHelper;
    private String            xDeviceID;
    private LayoutInflater    mInflater;
    public boolean            checkBoxState[];
    private Message           msg;
    private MainActivity      xParentActivity;


    public CustomListAdapterHAcc(Context context,  List<OpnameHAcc> list) {
        super(context, 0, list);
        this.mcontext = context;
        this.ListOpnameHAcc = list;

        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        msg = new Message(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderHAcc holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.rowlist_hist_acc, parent, false);

            holder = new ViewHolderHAcc();
            holder.Xtxt_HAccID         = (TextView) convertView.findViewById(R.id.txt_HAccID);
            holder.Xtxt_HAcc_NoTrx     = (TextView) convertView.findViewById(R.id.txt_HAcc_NoTrx);
            holder.Xtxt_HAcc_KodeOpnam = (TextView) convertView.findViewById(R.id.txt_HAcc_KodeOpnam);
            holder.Xtxt_HAcc_TglOpnam  = (TextView) convertView.findViewById(R.id.txt_HAcc_TglOpnam);
            holder.Xtxt_HAcc_Gudang    = (TextView) convertView.findViewById(R.id.txt_HAcc_Gudang);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolderHAcc) convertView.getTag();
        }

        TextView Lbl_HAccID = holder.Xtxt_HAccID;
        TextView Lbl_HAcc_NoTrx = holder.Xtxt_HAcc_NoTrx;
        TextView Lbl_HAcc_KodeOpnam = holder.Xtxt_HAcc_KodeOpnam;
        TextView Lbl_HAcc_TglOpnam = holder.Xtxt_HAcc_TglOpnam;
        TextView Lbl_HAcc_Gudang = holder.Xtxt_HAcc_Gudang;

        Lbl_HAccID.setText(ListOpnameHAcc.get(position).getHAccID());
        Lbl_HAcc_NoTrx.setText(ListOpnameHAcc.get(position).getHAcc_NoTrx());
        Lbl_HAcc_KodeOpnam.setText(ListOpnameHAcc.get(position).getHAcc_KodeOpnam());
        Lbl_HAcc_TglOpnam.setText(ListOpnameHAcc.get(position).getHAcc_TglOpnam());
        Lbl_HAcc_Gudang.setText(ListOpnameHAcc.get(position).getHAcc_Gudang());

        return convertView;
    }
}