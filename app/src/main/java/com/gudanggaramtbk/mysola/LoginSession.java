package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 1/25/2019.
 */

public class LoginSession {
    private String loginResult;
    private String LoginMessage;
    private String LoginName;
    private String LoginGroup;
    private String LoginResetFlag;


    // ------------ getter --------------------
    public String getLoginResult() {
        return loginResult;
    }
    public String getLoginMessage() {
        return LoginMessage;
    }
    public String getLoginName() {
        return LoginName;
    }
    public String getLoginGroup() {
        return LoginGroup;
    }
    public String getLoginResetFlag() {
        return LoginResetFlag;
    }

    // ------------ setter --------------------
    public void setLoginResult(String loginResult) {
        this.loginResult = loginResult;
    }
    public void setLoginMessage(String loginMessage) {
        LoginMessage = loginMessage;
    }
    public void setLoginName(String loginName) {
        LoginName = loginName;
    }
    public void setLoginGroup(String loginGroup) {
        LoginGroup = loginGroup;
    }
    public void setLoginResetFlag(String loginResetFlag) {
        LoginResetFlag = loginResetFlag;
    }


}
