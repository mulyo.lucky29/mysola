package com.gudanggaramtbk.mysola;

/**
 * Created by luckym on 10/24/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;

public class SoapRetrievePIC {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper    dbHelper;
    private SyncAction        pActivity;

    // override constructor
    public SoapRetrievePIC(SharedPreferences PConfig){
        Log.d("[GudangGaram]:", "SoapRetrievePIC Constructor");
        this.config           = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]:", "SetContext");
        this.context = context;
    }

    public void setParentActivity(SyncAction pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]:", "SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Retrieve() {

        Log.d("[GudangGaram]:", "Retrieve List PIC Begin");
        try {
            // call WS To Retrieve Manifest from oracle and save it to local database table
            SoapRetrievePICTaskAsync SoapRequest = new SoapRetrievePICTaskAsync(new SoapRetrievePICTaskAsync.SoapRetrievePICTaskAsyncResponse() {
                @Override
                public void PostSentAction(String RTaskID) {
                    try {
                        Log.d("[GudangGaram]:", "SendRequestUpdateFlag Start");
                        if(RTaskID.equals("*") == true){
                        }
                        else {
                        }
                    }
                    catch (Exception e) {
                        Log.d("[GudangGaram]:", "SendRequestUpdateFlag Exception " + e.getMessage().toString());
                    }
                    finally {
                        Log.d("[GudangGaram]:", "SendRequestUpdateFlag End");
                    }
                }
            });

            SoapRequest.setAttribute(context, dbHelper, config);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]:", "Retrieve List PIC End");
        }
    }
}