package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import org.w3c.dom.Text;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Exchanger;

/**
 * Created by LuckyM on 11/6/2018.
 */

public class CustomListAdapterOL extends ArrayAdapter<OpnameLogistic>{
    Context                mcontext;
    List<OpnameLogistic>   TrxOL;
    ListView               olisttask;
    private LayoutInflater mInflater;
    public  boolean         checkBoxState[];
    private Timer timer1,timer2;

    public CustomListAdapterOL(Context context, List<OpnameLogistic> list, ListView listTask) {
        super(context, 0, list);
        mcontext      = context;
        TrxOL         = list;
        checkBoxState = new boolean[list.size()];
        olisttask     = listTask;
        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // initialized list data
        for (int i = 0; i < list.size(); i++) {
            TrxOL.get(i).setQtyCheck(0.00);
            TrxOL.get(i).setChecked(false);

            // -- note selalu di enabled tanpa kondisi
            TrxOL.get(i).setNote_Enabled(true);
            //TrxOL.get(i).setNote_Enabled(false);

            olisttask.setItemChecked(i, false);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderOL holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.rowlist_opname_logistic, parent, false);
            holder = new ViewHolderOL();
            holder.XNo               = (TextView) convertView.findViewById(R.id.lbl_ol_no);
            holder.Xchk_select       = (CheckBox) convertView.findViewById(R.id.chk_ol);
            holder.Xtxt_ItemID       = (TextView) convertView.findViewById(R.id.txt_ol_itemID);
            holder.Xtxt_ItemCode     = (TextView) convertView.findViewById(R.id.txt_ol_itemCode);
            holder.Xtxt_OldItem      = (TextView) convertView.findViewById(R.id.txt_ol_OldItem);
            holder.Xtxt_ItemDesc     = (TextView) convertView.findViewById(R.id.txt_ol_itemDesc);
            holder.Xtxt_SubInvStatus = (TextView) convertView.findViewById(R.id.txt_ol_subInvStatus);
            holder.Xtxt_comp         = (TextView) convertView.findViewById(R.id.txt_ol_comp);
            holder.Xtxt_lotNo        = (TextView) convertView.findViewById(R.id.txt_ol_lot);
            holder.Xtxt_koliNo       = (TextView) convertView.findViewById(R.id.txt_ol_koli);
            holder.Xtxt_BlockID      = (TextView) convertView.findViewById(R.id.txt_ol_blockID);
            holder.Xtxt_qty_onhand   = (TextView) convertView.findViewById(R.id.txt_ol_Qty_OHS);
            holder.Xtxt_qty_picked   = (TextView) convertView.findViewById(R.id.txt_ol_Qty_PK);
            holder.Xtxt_qty_ohmp     = (TextView) convertView.findViewById(R.id.txt_ol_Qty_ohmp);
            holder.Xtxt_qtyCheck     = (EditText) convertView.findViewById(R.id.txt_ol_Qty_CK);
            holder.Xtxt_uom          = (TextView) convertView.findViewById(R.id.txt_ol_UOM);
            holder.Xtxt_Note         = (EditText) convertView.findViewById(R.id.txt_ol_Note);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderOL) convertView.getTag();
            holder.Xchk_select.setOnCheckedChangeListener(null);
        }

        holder.Xchk_select.setFocusable(true);
        holder.Xchk_select.setChecked(TrxOL.get(position).getChecked());
        holder.Xchk_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EH_CMD_CHECK(v,holder, position);
            }
        });


        holder.Xtxt_qtyCheck.setFocusable(false);
        holder.Xtxt_qtyCheck.setFocusableInTouchMode(true);
        /*
        holder.Xtxt_qtyCheck.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                Log.d("[GudangGaram]", "CustomListAdapterOK :: Xtxt_qtyCheck :: Lost Focus");
                EH_XQty_CHANGE2(s, holder, position);
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {  }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        });
        */

        holder.Xtxt_qtyCheck.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // When focus is lost check that the text field has valid values.
                if (!hasFocus) {
                    Log.d("[GudangGaram]", "CustomListAdapterOK :: Xtxt_qtyCheck :: Lost Focus");
                    EH_XQty_CHANGE(v, holder, position);
                    holder.Xtxt_qtyCheck.setFocusable(false);
                    holder.Xtxt_Note.requestFocus();
                }
                else{
                    Log.d("[GudangGaram]", "CustomListAdapterOK :: Xtxt_qtyCheck :: Got Focus");
                }
            }
        });


        holder.Xtxt_Note.setFocusable(false);
        holder.Xtxt_Note.setFocusableInTouchMode(true);
        holder.Xtxt_Note.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // When focus is lost check that the text field has valid values.
                if (!hasFocus) {
                    Log.d("[GudangGaram]", "CustomListAdapterOK :: Xtxt_ol_Note :: Lost Focus");
                    EH_XTxt_Note_CHANGE(v, holder, position);
                }
                else{
                    Log.d("[GudangGaram]", "CustomListAdapterOK :: Xtxt_ol_Note :: Got Focus");
                }
            }
        });

        // init
        holder.XNo.setText(Integer.toString(position + 1));
        holder.Xchk_select.setChecked(TrxOL.get(position).getChecked());
        holder.Xtxt_ItemID.setText(TrxOL.get(position).getItemID());
        holder.Xtxt_ItemCode.setText(TrxOL.get(position).getItemCode());
        holder.Xtxt_ItemDesc.setText(TrxOL.get(position).getItemDesc());
        holder.Xtxt_OldItem.setText(TrxOL.get(position).getItemOld());
        holder.Xtxt_SubInvStatus.setText(TrxOL.get(position).getSubInvStatus());
        holder.Xtxt_comp.setText(TrxOL.get(position).getLocComp());
        holder.Xtxt_lotNo.setText(TrxOL.get(position).getLotNum());
        holder.Xtxt_koliNo.setText(TrxOL.get(position).getKoliNum());
        holder.Xtxt_qty_onhand.setText(TrxOL.get(position).getQtyOnHand().toString());
        holder.Xtxt_qty_picked.setText(TrxOL.get(position).getQtyPicked().toString());
        holder.Xtxt_qty_ohmp.setText(TrxOL.get(position).getQtyOhmp().toString());
        holder.Xtxt_qtyCheck.setText(TrxOL.get(position).getQtyCheck().toString());
        //holder.Xtxt_qtyCheck.setEnabled(TrxOL.get(position).getChecked());

        holder.Xtxt_BlockID.setText(TrxOL.get(position).getBlockID());
        holder.Xtxt_uom.setText(TrxOL.get(position).getUOM());
        holder.Xtxt_Note.setText(TrxOL.get(position).getNote());

        // -- note selalu di enabled tanpa kondisi
        //holder.Xtxt_Note.setEnabled(TrxOL.get(position).getNote_Enabled());
        holder.Xtxt_Note.setEnabled(true);

        return convertView;
    }

    public void EH_CMD_CHECK(View v, ViewHolderOL hd, Integer position){
        Boolean selection;
        CheckBox cb = (CheckBox) v ;
        selection = ((CheckBox) v).isChecked();

        try{
            // init clear input
            //TrxOL.get(position).setNote("");
            // -- note selalu di enabled tanpa kondisi
            //TrxOL.get(position).setNote_Enabled(false);
            TrxOL.get(position).setNote_Enabled(true);

            if(selection == true){
                Log.d("[GudangGaram]:", "EH_CMD_CHECK :: Checked");
                if(Double.compare(Double.parseDouble(TrxOL.get(position).getQtyOhmp().toString()), 0.0) == 0){
                    Log.d("[GudangGaram]:", "EH_CMD_CHECK :: A");
                    TrxOL.get(position).setQtyCheck(0.0);
                    //TrxOL.get(position).setNote("");
                    //TrxOL.get(position).setChecked(false);
                    //olisttask.setItemChecked(position, false);
                }
                else{
                    Log.d("[GudangGaram]:", "EH_CMD_CHECK :: B");
                    TrxOL.get(position).setQtyCheck(Double.parseDouble(TrxOL.get(position).getQtyOhmp().toString()));
                    TrxOL.get(position).setChecked(true);
                    olisttask.setItemChecked(position, true);
                }
            }
            else{
                Log.d("[GudangGaram]:", "EH_CMD_CHECK :: UnChecked");
                TrxOL.get(position).setQtyCheck(0.0);
                TrxOL.get(position).setChecked(false);
                //TrxOL.get(position).setNote("");
                olisttask.setItemChecked(position, false);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "EH_CMD_CHECK Exception " + e.getMessage().toString());
        }
    }



    public void EH_XQty_CHANGE(View v, ViewHolderOL hd, Integer position) {
        EditText et = (EditText) v;
        try {
            Log.d("[GudangGaram]", "EH_XQty_CHANGE et.getText() = " + et.getText().toString());
            if (Double.compare(Double.parseDouble(et.getText().toString()), 0.0) > 0) {
                Log.d("[GudangGaram]", "EH_XQty_CHANGE > 0");
                TrxOL.get(position).setQtyCheck(Double.parseDouble(et.getText().toString()));
                TrxOL.get(position).setChecked(true);
                // clearkan inputan notes
                //TrxOL.get(position).setNote("");
                olisttask.setItemChecked(position, true);

                /*
                if (TrxOL.get(position).getChecked().equals(true) &&
                        (Double.parseDouble(TrxOL.get(position).getQtyOnHand().toString()) != Double.parseDouble(et.getText().toString()))) {
                    Log.d("[GudangGaram]", "EH_XQty_CHANGE :: A");
                    TrxOL.get(position).setNote_Enabled(true);

                }
                else {
                    Log.d("[GudangGaram]", "EH_XQty_CHANGE :: B");
                    // -- field note di set enabled tanpa terkecuali
                    TrxOL.get(position).setNote_Enabled(true);
                    //TrxOL.get(position).setNote_Enabled(false);
                }
                */
            }
            else if (Double.compare(Double.parseDouble(et.getText().toString()), 0.0) == 0) {
                Log.d("[GudangGaram]", "EH_XQty_CHANGE = 0");
                TrxOL.get(position).setQtyCheck(0.0);
                /*
                if(TrxOL.get(position).getChecked() == true){
                    // qty opname 0 maka harus di isi reason nya (hilang / rusak / dsb)
                    TrxOL.get(position).setNote_Enabled(true);
                }
                else{
                    // -- field note di set enabled tanpa terkecuali
                    TrxOL.get(position).setNote_Enabled(true);
                    //TrxOL.get(position).setNote_Enabled(false);
                }
                */

                //TrxOL.get(position).setChecked(false);
                //TrxOL.get(position).setNote_Enabled(false);
                //olisttask.setItemChecked(position, false);
            }
            hd.Xtxt_Note.setEnabled(TrxOL.get(position).getNote_Enabled());


        } catch (Exception e) {
            Log.d("[GudangGaram]", "EH_XQty_CHANGE Exception " + e.getMessage());
            TrxOL.get(position).setQtyCheck(0.0);
            TrxOL.get(position).setChecked(false);
            // -- field note di set enabled tanpa terkecuali
            //TrxOL.get(position).setNote_Enabled(true);

            //TrxOL.get(position).setNote_Enabled(false);
            olisttask.setItemChecked(position, false);
        }
    }


    public void EH_XTxt_Note_CHANGE(View v, ViewHolderOL hd, Integer position) {
        EditText et = (EditText) v;
        Log.d("[GudangGaram]", "EH_XTxt_Note_CHANGE :" + et.getText());
        try {
            if(et.length() > 0){
                TrxOL.get(position).setNote(et.getText().toString());
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "EH_XTxt_Note_CHANGE Exception : " + e.getMessage());
        }
    }

}
