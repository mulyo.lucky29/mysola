package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.List;

/**
 * Created by LuckyM on 11/9/2018.
 */

public class SoapSendDataOLTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String                    ResultResponse;
    private MySQLiteHelper            dbHelper;
    private String                    SentResponse;
    private String                    header_id;
    private OpnameLogistic            ObjOpnameLogistic;
    private String                    nik;
    private String                    device_id;
    private String                    seq_total;
    private String                    seq_no;

    public SoapSendDataOLTaskAsync(){}
    public interface SoapSendDataOLTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapSendDataOLTaskAsync.SoapSendDataOLTaskAsyncResponse delegate = null;
    public SoapSendDataOLTaskAsync(SoapSendDataOLTaskAsync.SoapSendDataOLTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper       dbHelper,
                             SharedPreferences    config,
                             String               p_header_id,
                             String               p_seq,
                             String               p_seq_total,
                             OpnameLogistic       p_ObjOpnameLogistic,
                             String               p_nik,
                             String               p_device_id){
        Log.d("[GudangGaram]", "SoapSendDataOLTaskAsync :: setAttribute");
        this.context                = context;
        this.dbHelper               = dbHelper;
        this.config                 = config;
        this.header_id              = p_header_id;
        this.ObjOpnameLogistic      = p_ObjOpnameLogistic;
        this.nik                    = p_nik;
        this.seq_no                 = p_seq;
        this.seq_total              = p_seq_total;
        this.device_id              = p_device_id;

        this.pd = new ProgressDialog(this.context);
        //Result = new ArrayList<DS_TaskItemDetail>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSendDataOLTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Send Data Opname Logistic To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        if(pd.isShowing()){
            pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
        Log.d("[GudangGaram]", "SoapSendDataOLTaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapSendDataOLTaskAsync :: onPostExecute >> " + output);
        delegate.PostSentAction(output);
        Toast.makeText(context,"Send Opname Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        Long     Result;
        Integer  ncount;

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Insert_SOL";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

                String v_org_id        = ObjOpnameLogistic.getOrgID();
                String v_item_code     = ObjOpnameLogistic.getItemCode();
                String v_item_id       = ObjOpnameLogistic.getItemID();
                String v_old_item      = ObjOpnameLogistic.getItemOld();
                String v_rak           = ObjOpnameLogistic.getLocRak();
                String v_bin           = ObjOpnameLogistic.getLocBin();
                String v_comp          = ObjOpnameLogistic.getLocComp();
                String v_lot           = ObjOpnameLogistic.getLotNum();
                String v_koli          = ObjOpnameLogistic.getKoliNum();
                String v_block_id      = ObjOpnameLogistic.getBlockID();
                String v_status_subinv = ObjOpnameLogistic.getSubInvStatus();
                String v_qty_oh        = ObjOpnameLogistic.getQtyOnHand().toString();
                String v_qty_picked    = ObjOpnameLogistic.getQtyPicked().toString();
                String v_qty_checked   = ObjOpnameLogistic.getQtyCheck().toString();
                String v_uom           = ObjOpnameLogistic.getUOM();
                String v_note          = ObjOpnameLogistic.getNote();

                Log.d("[GudangGaram]", "SoapSendDataOlTaskAsync :: Param ");

                // =============== p_solo_header_id ===================
                PropertyInfo prop_p_header_id = new PropertyInfo();
                prop_p_header_id.setName("p_solo_header_id");
                prop_p_header_id.setValue(header_id);
                prop_p_header_id.setType(String.class);
                Log.d("[GudangGaram]", "v_solo_header_id    :" + header_id);

                // =============== p_seq_no ===================
                PropertyInfo prop_p_seq_no = new PropertyInfo();
                prop_p_seq_no.setName("p_seq_no");
                prop_p_seq_no.setValue(seq_no);
                prop_p_seq_no.setType(String.class);
                Log.d("[GudangGaram]", "v_seq_no            :" + seq_no);

                // =============== p_org_id ==========================
                PropertyInfo prop_p_org_id = new PropertyInfo();
                prop_p_org_id.setName("p_org_id");
                prop_p_org_id.setValue(v_org_id);
                prop_p_org_id.setType(String.class);
                Log.d("[GudangGaram]", "v_org_id            :" + v_org_id);

                // =============== p_item_id ===================
                PropertyInfo prop_p_item_id = new PropertyInfo();
                prop_p_item_id.setName("p_item_id");
                prop_p_item_id.setValue(v_item_id);
                prop_p_item_id.setType(String.class);
                Log.d("[GudangGaram]", "v_item_id           :" + v_item_id);

                // =============== p_item_code ===================
                PropertyInfo prop_p_item_code = new PropertyInfo();
                prop_p_item_code.setName("p_item_code");
                prop_p_item_code.setValue(v_item_code);
                prop_p_item_code.setType(String.class);
                Log.d("[GudangGaram]", "v_item_code         :" + v_item_code);

                // =============== p_old_item ===================
                PropertyInfo prop_p_old_item = new PropertyInfo();
                prop_p_old_item.setName("p_old_item");
                prop_p_old_item.setValue(v_old_item);
                prop_p_old_item.setType(String.class);
                Log.d("[GudangGaram]", "v_old_item          :" + v_old_item);

                // =============== p_rak ===================
                PropertyInfo prop_p_rak = new PropertyInfo();
                prop_p_rak.setName("p_rak");
                prop_p_rak.setValue(v_rak);
                prop_p_rak.setType(String.class);
                Log.d("[GudangGaram]", "v_rak               :" + v_rak);

                // =============== p_bin ===================
                PropertyInfo prop_p_bin = new PropertyInfo();
                prop_p_bin.setName("p_bin");
                prop_p_bin.setValue(v_bin);
                prop_p_bin.setType(String.class);
                Log.d("[GudangGaram]", "v_bin               :" + v_bin);

                // =============== p_comp ===================
                PropertyInfo prop_p_comp = new PropertyInfo();
                prop_p_comp.setName("p_comp");
                prop_p_comp.setValue(v_comp);
                prop_p_comp.setType(String.class);
                Log.d("[GudangGaram]", "v_comp              :" + v_comp);

                // =============== p_lot ===================
                PropertyInfo prop_p_lot = new PropertyInfo();
                prop_p_lot.setName("p_lot_no");
                prop_p_lot.setValue(v_lot);
                prop_p_lot.setType(String.class);
                Log.d("[GudangGaram]", "v_lot               :" + v_lot);

                // =============== p_koli ===================
                PropertyInfo prop_p_koli = new PropertyInfo();
                prop_p_koli.setName("p_koli_no");
                prop_p_koli.setValue(v_koli);
                prop_p_koli.setType(String.class);
                Log.d("[GudangGaram]", "v_koli              :" + v_koli);

                // =============== p_block_id ===================
                PropertyInfo prop_p_block_id = new PropertyInfo();
                prop_p_block_id.setName("p_block_id");
                prop_p_block_id.setValue(v_block_id);
                prop_p_block_id.setType(Integer.class);
                Log.d("[GudangGaram]", "v_block_id          :" + v_block_id);

                // =============== p_status_subinv ===================
                PropertyInfo prop_p_status_subinv = new PropertyInfo();
                prop_p_status_subinv.setName("p_status_subinv");
                prop_p_status_subinv.setValue(v_status_subinv);
                prop_p_status_subinv.setType(String.class);
                Log.d("[GudangGaram]", "v_status_subinv     :" + v_status_subinv);

                // =============== p_qty_oh ===================
                PropertyInfo prop_p_qty_oh = new PropertyInfo();
                prop_p_qty_oh.setName("p_qty_oh");
                prop_p_qty_oh.setValue(v_qty_oh);
                prop_p_qty_oh.setType(String.class);
                Log.d("[GudangGaram]", "v_qty_oh            :" + v_qty_oh);

                // =============== p_qty_picked ===================
                PropertyInfo prop_p_qty_picked = new PropertyInfo();
                prop_p_qty_picked.setName("p_qty_picked");
                prop_p_qty_picked.setValue(v_qty_picked);
                prop_p_qty_picked.setType(String.class);
                Log.d("[GudangGaram]", "v_qty_picked        :" + v_qty_picked);

                // =============== p_qty_checked ===================
                PropertyInfo prop_p_qty_checked = new PropertyInfo();
                prop_p_qty_checked.setName("p_qty_checked");
                prop_p_qty_checked.setValue(v_qty_checked);
                prop_p_qty_checked.setType(String.class);
                Log.d("[GudangGaram]", "v_qty_checked       :" + v_qty_checked);

                // =============== p_uom ===================
                PropertyInfo prop_p_uom = new PropertyInfo();
                prop_p_uom.setName("p_uom");
                prop_p_uom.setValue(v_uom);
                prop_p_uom.setType(String.class);
                Log.d("[GudangGaram]", "v_uom               :" + v_uom);

                // =============== p_nik ===================
                PropertyInfo prop_p_nik = new PropertyInfo();
                prop_p_nik.setName("p_nik");
                prop_p_nik.setValue(nik);
                prop_p_nik.setType(String.class);
                Log.d("[GudangGaram]", "v_nik               :" + nik);

                // =============== p_note ===================
                PropertyInfo prop_p_note = new PropertyInfo();
                prop_p_note.setName("p_note");
                prop_p_note.setValue(v_note);
                prop_p_note.setType(String.class);
                Log.d("[GudangGaram]", "v_note              :" + v_note);

                // =============== p_device_id ===================
                PropertyInfo prop_p_device_id = new PropertyInfo();
                prop_p_device_id.setName("p_device_id");
                prop_p_device_id.setValue(device_id);
                prop_p_device_id.setType(String.class);
                Log.d("[GudangGaram]", "v_device_id         :" + device_id);

                request.addProperty(prop_p_header_id);
                request.addProperty(prop_p_seq_no);
                request.addProperty(prop_p_org_id);
                request.addProperty(prop_p_item_id);
                request.addProperty(prop_p_item_code);
                request.addProperty(prop_p_old_item);
                request.addProperty(prop_p_rak);
                request.addProperty(prop_p_bin);
                request.addProperty(prop_p_comp);
                request.addProperty(prop_p_lot);
                request.addProperty(prop_p_koli);
                request.addProperty(prop_p_block_id);
                request.addProperty(prop_p_status_subinv);
                request.addProperty(prop_p_qty_oh);
                request.addProperty(prop_p_qty_picked);
                request.addProperty(prop_p_qty_checked);
                request.addProperty(prop_p_uom);
                request.addProperty(prop_p_nik);
                request.addProperty(prop_p_note);
                request.addProperty(prop_p_device_id);

                //Web method call
                try {
                    HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                    httpTransport.debug = true;
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    httpTransport.call(SOAP_ACTION, envelope);

                    SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                    String v_solo_detail_id = resultsRequestSOAP.getProperty(0).toString();
                    if(v_solo_detail_id.length() > 0){
                        ResultResponse = seq_no;
                    }
                    //ResultResponse  = v_solo_detail_id;
                    Log.d("[GudangGaram]", "Result  : " + resultsRequestSOAP.getProperty(0).toString());

                    publishProgress("Sending Data Opname Record " + seq_no  + " of " + seq_total);

                    try {
                        Thread.sleep(1800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
                catch (Exception ex) {
                    ResultResponse = "-1";
                    Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
                }
                finally {
                    Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: end doInBackground");
                }

        return ResultResponse;
    }
}
