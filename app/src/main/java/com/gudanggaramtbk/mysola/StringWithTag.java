package com.gudanggaramtbk.mysola;

/**
 * Created by luckym on 10/24/2018.
 */

public class StringWithTag {
    public String string;
    public Object tag;

    public StringWithTag(String string, Object tag) {
        this.string = string;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return string;
    }
}
