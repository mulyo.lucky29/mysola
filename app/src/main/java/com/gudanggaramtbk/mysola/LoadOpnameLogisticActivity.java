package com.gudanggaramtbk.mysola;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class LoadOpnameLogisticActivity extends AppCompatActivity implements View.OnClickListener{
    private SharedPreferences config;
    private MySQLiteHelper    dbHelper;
    private String            StrSessionNIK;
    private String            StrSessionName;
    private String            StrSessionGroup;
    private String            StrDeviceID;
    private Spinner           Cbo_ol_Gudang;
    private EditText          Txt_ol_Rak;
    private EditText          Txt_ol_Bin;
    private TextView          Lbl_org_id;
    private ListView          List_OpnameLog;
    private Button            Cmd_ol_populate;
    private Button            Cmd_ol_close;
    private Button            Cmd_ol_submit;
    private Button            Cmd_ol_ScanRak;
    private Button            Cmd_ol_ScanBin;
    private String            TypeScan;
    private Boolean           PopulateFlag;
    private SoapRetrieveGudang    populateGudang;
    private SoapRetrieveOpnameLog populateOpnameLog;
    private SoapSendReqOL         SendOpname;
    private String                org_id;
    private String                bin;
    private String                rak;

    public void clear_all(){
        Txt_ol_Rak.setText("");
        Txt_ol_Bin.setText("");
        List_OpnameLog.setAdapter(null);
        Txt_ol_Rak.requestFocus();
    }

    private Integer do_check_select(){
        Integer err, checked;

        err     = 0;
        checked = 0;

        if(List_OpnameLog.getCount() == 0){
            err +=1;
            DialogBox("Item Not Populated", "Please Populate Item First");
        }
        else {
            for (int idx = 0; idx < List_OpnameLog.getCount(); idx++) {
                if (List_OpnameLog.isItemChecked(idx) == true) {
                    checked = checked + 1;
                }
            }
            if (checked == 0) {
                err += 1;
                DialogBox("Item Not Selected", "Please Select At Least One");
            }
        }
        return err;
    }

    private Integer do_validation_input(){
        Integer err;
        err = 0;
        try{
            // validation Gudang
            if(Cbo_ol_Gudang.getCount() == 0) {
                err += 1;
            }
            else{
                if(Cbo_ol_Gudang.getSelectedItem().toString().length() == 0){
                    err +=1;
                    DialogBox("Gudang Not Complete","Please Select Gudang First !");
                }
            }
            // validation Rak
            if(Txt_ol_Rak.getText().length() == 0){
                err +=1;
                DialogBox("Rak Not Complete","Please Input Rak !");
            }
            // validation Bin
            if(Txt_ol_Bin.getText().length() == 0){
                err +=1;
                Toast.makeText(this,"Please Enter Bin", Toast.LENGTH_LONG).show();
                DialogBox("Bin Not Complete","Please Input Bin !");
            }
        }
        catch (Exception e){
        }

        return err;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_opname_logistic);

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // get information intent
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
        StrSessionGroup = getIntent().getStringExtra("PIC_GROUP");

        StrDeviceID    = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        Cbo_ol_Gudang      = (Spinner) findViewById(R.id.cbo_ol_Gudang);
        Txt_ol_Rak         = (EditText) findViewById(R.id.txt_ol_rak);
        Txt_ol_Bin         = (EditText) findViewById(R.id.txt_ol_bin);
        Lbl_org_id         = (TextView) findViewById(R.id.lbl_org_id);

        Cmd_ol_populate    = (Button) findViewById(R.id.cmd_ol_populate);
        Cmd_ol_close       = (Button) findViewById(R.id.cmd_ol_close);
        Cmd_ol_submit      = (Button) findViewById(R.id.cmd_ol_submit);
        Cmd_ol_ScanBin     = (Button) findViewById(R.id.cmd_ol_scan_bin);
        Cmd_ol_ScanRak     = (Button) findViewById(R.id.cmd_ol_scan_rak);

        List_OpnameLog     = (ListView) findViewById(R.id.lst_OpnameLogistic);
        List_OpnameLog.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        // initialized soap service
        populateGudang = new SoapRetrieveGudang(config);
        populateGudang.setContext(this);
        populateGudang.setDBHelper(dbHelper);

        populateOpnameLog = new SoapRetrieveOpnameLog(config, List_OpnameLog);
        populateOpnameLog.setContext(this);
        populateOpnameLog.setDBHelper(dbHelper);

        SendOpname = new SoapSendReqOL(config);
        SendOpname.setContext(this);
        SendOpname.setParentActivity(this);
        SendOpname.setDBHelper(dbHelper);


        // call WS to get update Rak Persiapan from Oracle
        initComboBoxGudang();
        // ----------- event handler ------------
        Cmd_ol_populate.setOnClickListener(this);
        Cmd_ol_close.setOnClickListener(this);
        Cmd_ol_submit.setOnClickListener(this);
        Cmd_ol_ScanRak.setOnClickListener(this);
        Cmd_ol_ScanBin.setOnClickListener(this);

        PopulateFlag = false;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.cmd_ol_populate){
            EH_CMD_POPULATE();
        }
        else if(id == R.id.cmd_ol_scan_rak){
            EH_CMD_SCAN_RAK();
        }
        else if(id == R.id.cmd_ol_scan_bin){
            EH_CMD_SCAN_BIN();
        }
        else if(id == R.id.cmd_ol_close){
            EH_CMD_CLOSE();
        }
        else if(id == R.id.cmd_ol_submit){
            EH_CMD_SEND();
        }
    }

    public void initComboBoxGudang()
    {
        try{
            populateGudang.Retrieve(Cbo_ol_Gudang, Lbl_org_id);
        }
        catch(Exception e){
        }
    }

    public void DialogBox(String Title, String Message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle(Title)
                .setMessage(Message)
                .setCancelable(false)
                .setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void EH_CMD_POPULATE(){
        Integer err = 0;
        err = do_validation_input();
        try{
            if(err == 0){
                Log.d("[GudangGaram]:", "populateOpnameLog.Retrieve >> ");

                org_id = Lbl_org_id.getText().toString();
                bin    = Txt_ol_Bin.getText().toString();
                rak    = Txt_ol_Rak.getText().toString();

                Log.d("[GudangGaram]:", "PARAMETERS  >> ");
                Log.d("[GudangGaram]:", "Org ID   : " + org_id);
                Log.d("[GudangGaram]:", "Bin      : " + bin);
                Log.d("[GudangGaram]:", "Rak      : " + rak);

                // call WS to retrieve Bin Persiapan from oracle
                populateOpnameLog.Retrieve(org_id, rak, bin);
                PopulateFlag = true;
            }
            else{
                DialogBox("Input Not Complete","Please Check Your Input !");
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "Retrieve Exception  : " + e.getMessage().toString());
            DialogBox("Retrieve Exception",e.getMessage().toString());
        }
    }

    public void EH_CMD_SEND(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Send Data Confirm")
                .setMessage("Confirm Send Opname Data ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                EH_CMD_SEND_ACTION();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void EH_CMD_SEND_ACTION(){
        List<OpnameLogistic> listselected;
        Integer rown = 0;
        Integer err  = 0;
        Integer verr = 0;

        err = do_validation_input();
        if(err == 0){
            try{
                if(do_check_select() == 0){
                    listselected = new ArrayList<OpnameLogistic>();
                    // gathered list selected item
                    for(int idx = 0; idx < List_OpnameLog.getCount(); idx ++){
                        if(List_OpnameLog.isItemChecked(idx) == true) {
                            View HoldView = List_OpnameLog.getAdapter().getView(idx, null, null);
                            ViewHolderOL holder = (ViewHolderOL) HoldView.getTag();

                            OpnameLogistic ox = new OpnameLogistic();
                            ox.setItemID(holder.Xtxt_ItemID.getText().toString());                            // ok
                            ox.setItemCode(holder.Xtxt_ItemCode.getText().toString());                        // ok
                            ox.setItemOld(holder.Xtxt_OldItem.getText().toString());                          // ok
                            ox.setItemDesc(holder.Xtxt_ItemDesc.getText().toString());                        // ok
                            ox.setOrgID(Lbl_org_id.getText().toString());                                     // ok
                            ox.setOrgName(Cbo_ol_Gudang.getSelectedItem().toString());                        // ok
                            ox.setLocRak(Txt_ol_Rak.getText().toString());                                    // ok
                            ox.setLocBin(Txt_ol_Bin.getText().toString());                                    // ok
                            ox.setSubInvStatus(holder.Xtxt_SubInvStatus.getText().toString());                // ok
                            ox.setLocComp(holder.Xtxt_comp.getText().toString());                             // ok
                            ox.setKoliNum(holder.Xtxt_koliNo.getText().toString());                           // ok
                            ox.setLotNum(holder.Xtxt_lotNo.getText().toString());                             // ok
                            ox.setBlockID(holder.Xtxt_BlockID.getText().toString());                          // ok
                            ox.setQtyOnHand(Double.parseDouble(holder.Xtxt_qty_onhand.getText().toString())); // ok
                            ox.setQtyPicked(Double.parseDouble(holder.Xtxt_qty_picked.getText().toString())); // ok
                            ox.setQtyOhmp(Double.parseDouble(holder.Xtxt_qty_ohmp.getText().toString()));     // ok
                            ox.setQtyCheck(Double.parseDouble(holder.Xtxt_qtyCheck.getText().toString()));    // ok
                            ox.setUOM(holder.Xtxt_uom.getText().toString());                                  // ok
                            ox.setNote(holder.Xtxt_Note.getText().toString());

                            listselected.add(ox);
                            rown = rown + 1;
                        }
                    }

                    if(rown > 0){
                        SendOpname.Send(listselected, org_id, Cbo_ol_Gudang.getSelectedItem().toString(), rak, bin, StrSessionNIK, StrDeviceID);
                    }
                }
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "EH_CMD_SEND Exception :: " + e.getMessage().toString());
            }
        }
    }

    public void EH_CMD_SCAN_RAK(){
        try{
            Log.d("[GudangGaram]", "Scan Rak :: onActivityResult Expected  : " + IntentIntegrator.REQUEST_CODE);
            TypeScan = "ScanRak";
            IntentIntegrator integrator = new IntentIntegrator(LoadOpnameLogisticActivity.this);
            integrator.setCaptureActivity(CaptureCustomeActivity.class);
            integrator.setBeepEnabled(true);
            integrator.initiateScan();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_SCAN_RAK Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "EH_CMD_SCAN_RAK End");
    }
    public void EH_CMD_SCAN_BIN(){
        try{
            Log.d("[GudangGaram]", "Scan Bin :: onActivityResult Expected  : " + IntentIntegrator.REQUEST_CODE);
            TypeScan = "ScanBin";
            IntentIntegrator integrator = new IntentIntegrator(LoadOpnameLogisticActivity.this);
            integrator.setCaptureActivity(CaptureCustomeActivity.class);
            integrator.setBeepEnabled(true);
            integrator.initiateScan();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_CMD_SCAN_BIN Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "EH_CMD_SCAN_BIN End");
    }

    public void EH_CMD_CLOSE(){
        Log.d("[GudangGaram]: ","EH_CMD_CLOSE");
        Intent upIntent = NavUtils.getParentActivityIntent(this);
        // passing intent login information before back
        upIntent.putExtra("PIC_NIK",   StrSessionNIK);
        upIntent.putExtra("PIC_NAME",  StrSessionName);
        upIntent.putExtra("PIC_GROUP", StrSessionGroup);

        NavUtils.navigateUpTo(this, upIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NIK",   StrSessionNIK);
                    upIntent.putExtra("PIC_NAME",  StrSessionName);
                    upIntent.putExtra("PIC_GROUP", StrSessionGroup);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("[GudangGaram]", "onActivityResult : " + requestCode);

        // intent from barcode scanner
        if(requestCode == IntentIntegrator.REQUEST_CODE){
            if (resultCode == RESULT_CANCELED){
            }
            else
            {
                IntentResult ScanResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
                if(ScanResult != null){
                    String StrScanResult = ScanResult.getContents();
                    if(TypeScan.equals("ScanRak")){
                        Txt_ol_Rak.setText(StrScanResult);
                        Txt_ol_Bin.requestFocus();
                    }
                    else{
                        Txt_ol_Bin.setText(StrScanResult);
                        EH_CMD_POPULATE();
                    }
                }
            }
        }
    }

}
