package com.gudanggaramtbk.mysola;

import java.util.List;

/**
 * Created by LuckyM on 11/5/2018.
 */

public interface SoapRetrieveGudangTaskAsyncResponse {
    void PostSentAction(String output);
}
