package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 11/8/2018.
 */

public class SoapSendReqOLTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String                    ResultResponse;
    private MySQLiteHelper            dbHelper;
    private String                    SentResponse;
    private OpnameLogistic            objOpnameLogistic;

    private String                    org_id;
    private String                    Rak;
    private String                    Bin;
    private String                    nik;
    private String                    device_id;

    public SoapSendReqOLTaskAsync(){}
    public interface SoapSendReqOLTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapSendReqOLTaskAsync.SoapSendReqOLTaskAsyncResponse delegate = null;
    public SoapSendReqOLTaskAsync(SoapSendReqOLTaskAsync.SoapSendReqOLTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper    dbHelper,
                             SharedPreferences config,
                             String            p_org_id,
                             String            p_rak,
                             String            p_bin,
                             String            p_nik,
                             String            p_device_id){
        Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        //this.objOpnameLogistic = OobjOpnameLogistic;
        this.org_id            = p_org_id;
        this.Rak               = p_rak;
        this.Bin               = p_bin;
        this.device_id         = p_device_id;
        this.nik               = p_nik;
        this.pd = new ProgressDialog(this.context);
        //Result = new ArrayList<DS_TaskItemDetail>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve List PIC From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        pd.setMessage(args[0].toString());
        Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: onPostExecute >> " + ResultResponse);
        try{
            delegate.PostSentAction(ResultResponse);
        }
        catch(Exception e){
        }
        finally {
            if(output.equals("-1")){
                Toast.makeText(context,"Retrieve PIC Done", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(context,"Retrieve PIC" + output + " Done", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Long     Result;
        Integer  ncount;
        String   v_org_id;
        String   v_rak;
        String   v_bin;
        String   v_solo_header_id = "*";

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "GET_No_OL_Trx";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        v_org_id = org_id;
        v_bin    = Bin;
        v_rak    = Rak;

        // =============== p_org_id ===================
        PropertyInfo prop_p_org_id = new PropertyInfo();
        prop_p_org_id.setName("p_org_id");
        prop_p_org_id.setValue(v_org_id);
        prop_p_org_id.setType(String.class);

        // =============== p_rak ===================
        PropertyInfo prop_p_rak = new PropertyInfo();
        prop_p_rak.setName("p_rak");
        prop_p_rak.setValue(v_rak);
        prop_p_rak.setType(String.class);

        // =============== p_bin ===================
        PropertyInfo prop_p_bin = new PropertyInfo();
        prop_p_bin.setName("p_bin");
        prop_p_bin.setValue(v_bin);
        prop_p_bin.setType(String.class);

        // =============== p_nik ===================
        PropertyInfo prop_p_nik = new PropertyInfo();
        prop_p_nik.setName("p_nik");
        prop_p_nik.setValue(nik);
        prop_p_nik.setType(String.class);

        // =============== p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(device_id);
        prop_p_device_id.setType(String.class);

        request.addProperty(prop_p_org_id);
        request.addProperty(prop_p_rak);
        request.addProperty(prop_p_bin);
        request.addProperty(prop_p_nik);
        request.addProperty(prop_p_device_id);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
            v_solo_header_id = resultsRequestSOAP.getProperty(0).toString();
            ResultResponse  = v_solo_header_id;
            Log.d("[GudangGaram]", "ResultResponse : " + ResultResponse);


        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        finally {
            Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: end doInBackground");
        }

        return ResultResponse;
    }
}
