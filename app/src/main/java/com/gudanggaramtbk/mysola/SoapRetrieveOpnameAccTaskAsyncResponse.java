package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 11/15/2018.
 */

public interface SoapRetrieveOpnameAccTaskAsyncResponse {
    void PostSentAction(String output);
}
