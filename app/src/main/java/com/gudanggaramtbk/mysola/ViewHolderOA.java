package com.gudanggaramtbk.mysola;

import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by LuckyM on 11/15/2018.
 */

public class ViewHolderOA {
    public TextView Xlbl_oa_trx_id;
    public TextView Xlbl_oa_Seq;
    public TextView Xtxt_oa_itemCode;
    public TextView Xtxt_oa_OldItem;
    public TextView Xtxt_oa_rak;
    public TextView Xtxt_oa_bin;
    public TextView Xtxt_oa_comp;
    public TextView Xtxt_oa_itemID;
    public TextView Xtxt_oa_itemDesc;
    public TextView Xtxt_oa_blockID;
    public TextView Xtxt_oa_subinvstatus;
    public TextView Xtxt_oa_koli;
    public TextView Xtxt_oa_lot;
    public TextView Xtxt_oa_Qty_OHS;
    public TextView Xtxt_oa_Qty_PK;
    public CheckBox Xchk_oa;
    public EditText Xtxt_oa_Qty_CK;
    public TextView Xtxt_oa_UOM;
    public TextView Xtxt_oa_Note;
}
