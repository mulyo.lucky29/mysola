package com.gudanggaramtbk.mysola;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by LuckyM on 11/15/2018.
 */

public class CustomListAdapterDOA extends ArrayAdapter<OpnameListAcc> {
    Context                mcontext;
    List<OpnameListAcc>    ListBatchAcc;
    ListView               olistbatch;
    private SharedPreferences config;
    private MySQLiteHelper    dbHelper;
    private String         xDeviceID;
    private LayoutInflater mInflater;
    public boolean         checkBoxState[];
    private Message        msg;
    private SyncAction     xParentActivity;

    private SoapRetrieveOpnameAccDtl DownloadOpname;

    public CustomListAdapterDOA(Context context, SyncAction pParentActivity , MySQLiteHelper  dbHelper, SharedPreferences config, String pDeviceID, List<OpnameListAcc> list, ListView listTask) {
        super(context, 0, list);
        this.config          = config;
        this.mcontext        = context;
        this.ListBatchAcc    = list;
        this.olistbatch      = listTask;
        this.dbHelper        = dbHelper;
        this.xDeviceID       = pDeviceID;
        this.xParentActivity = pParentActivity;

        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        msg = new Message(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderDOA holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.rowlist_dwn_lstacc, parent, false);

                holder = new ViewHolderDOA();
                holder.Xlbl_oad_no              = (TextView) convertView.findViewById(R.id.lbl_oad_no);
                holder.Xlbl_oad_kodeOpnam       = (TextView) convertView.findViewById(R.id.lbl_oad_kodeOpnam);
                holder.Xlbl_oad_fileID          = (TextView) convertView.findViewById(R.id.lbl_oad_fileID);
                holder.Xlbl_oad_assigned_date   = (TextView) convertView.findViewById(R.id.lbl_oad_assigned_date);
                holder.Xlbl_oad_assigned_nik    = (TextView) convertView.findViewById(R.id.lbl_oad_assigned_nik);
                holder.Xcmd_oad_download        = (ImageButton) convertView.findViewById(R.id.cmd_oad_downs);

                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolderDOA) convertView.getTag();
            }

            TextView Lbl_oad_no             = holder.Xlbl_oad_no;
            TextView Lbl_oad_kodeOpnam      = holder.Xlbl_oad_kodeOpnam;
            TextView Lbl_oad_fileID         = holder.Xlbl_oad_fileID;
            TextView Lbl_oad_assigned_date  = holder.Xlbl_oad_assigned_date;
            TextView Lbl_oad_assigned_nik   = holder.Xlbl_oad_assigned_nik;
            ImageButton Cmd_oad_download    = holder.Xcmd_oad_download;

            Lbl_oad_no.setText(Integer.toString(position + 1));
            Lbl_oad_kodeOpnam.setText(ListBatchAcc.get(position).getOpnameCode());
            Lbl_oad_fileID.setText(ListBatchAcc.get(position).getFileID());
            Lbl_oad_assigned_date.setText(ListBatchAcc.get(position).getAssignedDate());
            Lbl_oad_assigned_nik.setText(ListBatchAcc.get(position).getAssignedNIK());

            Cmd_oad_download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mcontext);
                    alertDialogBuilder
                            .setTitle("Download Data File ID = " + ListBatchAcc.get(position).getFileID() + " Confirm ")
                            .setMessage("Confirm Download Data ?")
                            .setCancelable(false)
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            EH_CMD_DOWNLOAD_LIST_ACC(ListBatchAcc.get(position).getFileID(), ListBatchAcc.get(position).getAssignedNIK(), xDeviceID);
                                        }
                                    })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });

        return convertView;
    }

    public void EH_CMD_DOWNLOAD_LIST_ACC(String FileID, String NIK, String DeviceID){
         DownloadOpname = new SoapRetrieveOpnameAccDtl(config);
         DownloadOpname.setContext(mcontext);
         DownloadOpname.setParentActivity(xParentActivity);
         DownloadOpname.setDBHelper(dbHelper);
         DownloadOpname.Retrieve(FileID, NIK, DeviceID);
    }
}