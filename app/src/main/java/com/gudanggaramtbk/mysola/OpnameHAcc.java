package com.gudanggaramtbk.mysola;

/**
 * Created by luckym on 12/3/2018.
 */

public class OpnameHAcc {
    private String HAccID;
    private String HAcc_NoTrx;
    private String HAcc_KodeOpnam;
    private String HAcc_TglOpnam;
    private String HAcc_Gudang;
    private String HAcc_NIK;

    // ---------------- getter -----------------------
    public String getHAccID() {
        return HAccID;
    }
    public String getHAcc_NoTrx() {
        return HAcc_NoTrx;
    }
    public String getHAcc_KodeOpnam() {
        return HAcc_KodeOpnam;
    }
    public String getHAcc_TglOpnam() {
        return HAcc_TglOpnam;
    }
    public String getHAcc_Gudang() {
        return HAcc_Gudang;
    }
    public String getHAcc_NIK() {
        return HAcc_NIK;
    }


    // ---------------- setter -----------------------
    public void setHAccID(String HAccID) {
        this.HAccID = HAccID;
    }
    public void setHAcc_NoTrx(String HAcc_NoTrx) {
        this.HAcc_NoTrx = HAcc_NoTrx;
    }
    public void setHAcc_KodeOpnam(String HAcc_KodeOpnam) {
        this.HAcc_KodeOpnam = HAcc_KodeOpnam;
    }
    public void setHAcc_TglOpnam(String HAcc_TglOpnam) {
        this.HAcc_TglOpnam = HAcc_TglOpnam;
    }
    public void setHAcc_Gudang(String HAcc_Gudang) {
        this.HAcc_Gudang = HAcc_Gudang;
    }
    public void setHAcc_NIK(String HAcc_NIK) {
        this.HAcc_NIK = HAcc_NIK;
    }


}
