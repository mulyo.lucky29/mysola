package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 1/24/2019.
 */

public class SoapSendVerifyLoginTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String ResultResponse;
    private MySQLiteHelper dbHelper;
    private String SentResponse;

    private String username;
    private String password;
    private LoginSession ols;

    public SoapSendVerifyLoginTaskAsync() {}
    public interface SoapSendVerifyLoginTaskAsyncResponse {
        void PostSentAction(LoginSession output);
    }
    public SoapSendVerifyLoginTaskAsync.SoapSendVerifyLoginTaskAsyncResponse delegate = null;
    public SoapSendVerifyLoginTaskAsync(SoapSendVerifyLoginTaskAsync.SoapSendVerifyLoginTaskAsyncResponse delegate) {
        this.delegate = delegate;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String p_username,
                             String p_password) {
        Log.d("[GudangGaram]", "SoapSendVerifyLoginTaskAsync :: setAttribute");
        this.context  = context;
        this.dbHelper = dbHelper;
        this.config   = config;
        this.username = p_username;
        this.password = p_password;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSendVerifyLoginTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Verify Account, Please Wait ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        pd.setMessage(args[0].toString());
        Log.d("[GudangGaram]", "SoapSendVerifyLoginTaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapSendVerifyLoginTaskAsync :: onPostExecute ");
        delegate.PostSentAction(ols);
    }

    @Override
    protected String doInBackground(String... params) {
        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "GET_Verify_Login";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        // =============== p_username ===================
        PropertyInfo prop_p_username = new PropertyInfo();
        prop_p_username.setName("p_username");
        prop_p_username.setValue(username);
        prop_p_username.setType(String.class);

        // =============== p_password ===================
        PropertyInfo prop_p_passowrd = new PropertyInfo();
        prop_p_passowrd.setName("p_password");
        prop_p_passowrd.setValue(password);
        prop_p_passowrd.setType(String.class);

        request.addProperty(prop_p_username);
        request.addProperty(prop_p_passowrd);

        ols = new LoginSession();

        //Web method call
        try {
            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS, 6000);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));

                if (obj2.getPropertyCount() > 0) {
                    for (int i = 0; i < obj2.getPropertyCount(); i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);

                        ols.setLoginResult(obj3.getProperty(0).toString()) ;
                        ols.setLoginMessage(obj3.getProperty(1).toString());
                        ols.setLoginName(obj3.getProperty(2).toString());
                        ols.setLoginGroup(obj3.getProperty(3).toString());
                        ols.setLoginResetFlag(obj3.getProperty(4).toString());

                        Log.d("[GudangGaram]", "Result       = " + obj3.getProperty(0).toString());
                        Log.d("[GudangGaram]", "Message      = " + obj3.getProperty(1).toString());
                        Log.d("[GudangGaram]", "Name         = " + obj3.getProperty(2).toString());
                        Log.d("[GudangGaram]", "Group        = " + obj3.getProperty(3).toString());
                        Log.d("[GudangGaram]", "Reset Flag   = " + obj3.getProperty(4).toString());
                    }
                } else {
                    Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                }
            } catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }
        } catch (Exception ex) {
            Log.d("[GudangGaram]", "SoapRetrieveOpnameAccTaskAsync Catch Http Transport : " + ex.getMessage().toString());
        } finally {
            Log.d("[GudangGaram]", "SoapRetrieveOpnameAccTaskAsync  :: end doInBackground");
        }

        return "";
    }
}
