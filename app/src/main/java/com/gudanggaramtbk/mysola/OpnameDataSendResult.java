package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 11/27/2018.
 */

public class OpnameDataSendResult {
    String RSoacc_int_id;
    String RSoacc_dtl_id;
    String RTrx_No;

    // =============== getter ==================
    public String getRSoacc_int_id() {
        return RSoacc_int_id;
    }
    public String getRSoacc_dtl_id() {
        return RSoacc_dtl_id;
    }
    public String getRTrx_No() {
        return RTrx_No;
    }

    // ================ setter =================
    public void setRSoacc_int_id(String RSoacc_int_id) {
        this.RSoacc_int_id = RSoacc_int_id;
    }
    public void setRSoacc_dtl_id(String RSoacc_dtl_id) {
        this.RSoacc_dtl_id = RSoacc_dtl_id;
    }
    public void setRTrx_No(String RTrx_No) {
        this.RTrx_No = RTrx_No;
    }

}
