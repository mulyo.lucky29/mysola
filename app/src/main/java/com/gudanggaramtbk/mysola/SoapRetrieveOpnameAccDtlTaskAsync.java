package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by LuckyM on 11/16/2018.
 */

public class SoapRetrieveOpnameAccDtlTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private String FileID;
    private String Nik;

    public SoapRetrieveOpnameAccDtlTaskAsync(SharedPreferences config) {
        this.config = config;
    }

    public interface SoapRetrieveOpnameAccDtlTaskAsyncResponse {
        void PostSentAction(String output);
    }

    public SoapRetrieveOpnameAccDtlTaskAsync.SoapRetrieveOpnameAccDtlTaskAsyncResponse delegate = null;
    public SoapRetrieveOpnameAccDtlTaskAsync(SoapRetrieveOpnameAccDtlTaskAsync.SoapRetrieveOpnameAccDtlTaskAsyncResponse delegate) {
        this.delegate = delegate;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public void setAttribute(Context context, MySQLiteHelper dbHelper, SharedPreferences config, String pFileID, String pNik) {
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtlTaskAsync :: setAttribute");
        this.context = context;
        this.dbHelper = dbHelper;
        this.config = config;
        this.pd = new ProgressDialog(this.context);
        this.FileID   = pFileID;
        this.Nik      = pNik;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtlTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait Retrieving Data From Oracle Inprogress ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    protected void onProgressUpdate(String[] args) {
        super.onProgressUpdate(args);
        if(pd.isShowing()){
            pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtlTaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        try {
            delegate.PostSentAction(output);
        }
        catch (Exception e) {
        }
        Toast.makeText(context, "Retrieve Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        int ncount;
        String file_id  = "-1";
        String rak_persiapan = "";

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Task_Detail";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());
        Log.d("[GudangGaram]", "SOAP REQUEST PARAM P_NIK  :" + Nik);

        // =============== p_file_id ===================
        PropertyInfo prop_p_file_id = new PropertyInfo();
        prop_p_file_id.setName("p_file_id");
        prop_p_file_id.setValue(FileID);
        prop_p_file_id.setType(String.class);

        // =============== p_pic_nik ===================
        PropertyInfo prop_p_pic_nik = new PropertyInfo();
        prop_p_pic_nik.setName("p_pic_nik");
        prop_p_pic_nik.setValue(Nik);
        prop_p_pic_nik.setType(String.class);

        request.addProperty(prop_p_file_id);
        request.addProperty(prop_p_pic_nik);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        String xp_creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());

        //Web method call
        try {
            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS, 6000);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));

                ncount = obj2.getPropertyCount();

                if (ncount > 0) {
                    for (int i = 0; i < ncount; i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);

                        file_id = obj3.getProperty(2).toString();

                        OpnameData oad = new OpnameData();
                        Log.d("[GudangGaram]", "-----------------------------------" + Integer.toString(i) + "--------------------------------");
                        Log.d("[GudangGaram]", "soacc_dtl_id      : " + obj3.getProperty(0).toString());
                        Log.d("[GudangGaram]", "sequence_number   : " + obj3.getProperty(1).toString());
                        Log.d("[GudangGaram]", "file_id           : " + obj3.getProperty(2).toString());
                        Log.d("[GudangGaram]", "organization_id   : " + obj3.getProperty(3).toString());
                        Log.d("[GudangGaram]", "organization_name : " + obj3.getProperty(4).toString());
                        Log.d("[GudangGaram]", "item_id           : " + obj3.getProperty(5).toString());
                        Log.d("[GudangGaram]", "item_code         : " + obj3.getProperty(6).toString());
                        Log.d("[GudangGaram]", "item_description  : " + obj3.getProperty(7).toString());
                        Log.d("[GudangGaram]", "old_iten          : " + obj3.getProperty(8).toString());
                        Log.d("[GudangGaram]", "rak               : " + obj3.getProperty(9).toString());
                        Log.d("[GudangGaram]", "bin               : " + obj3.getProperty(10).toString());
                        Log.d("[GudangGaram]", "compartement      : " + obj3.getProperty(11).toString());
                        Log.d("[GudangGaram]", "lot_number        : " + obj3.getProperty(12).toString());
                        Log.d("[GudangGaram]", "koli_number       : " + obj3.getProperty(13).toString());
                        Log.d("[GudangGaram]", "status_subinv     : " + obj3.getProperty(14).toString());
                        Log.d("[GudangGaram]", "rak_seq           : " + obj3.getProperty(15).toString());
                        Log.d("[GudangGaram]", "bin_seq           : " + obj3.getProperty(16).toString());
                        Log.d("[GudangGaram]", "qty_onhand        : " + obj3.getProperty(17).toString());
                        Log.d("[GudangGaram]", "qty_picked        : " + obj3.getProperty(18).toString());
                        Log.d("[GudangGaram]", "primary_uom       : " + obj3.getProperty(19).toString());
                        Log.d("[GudangGaram]", "pic_nik           : " + obj3.getProperty(20).toString());
                        Log.d("[GudangGaram]", "pic_person_id     : " + obj3.getProperty(21).toString());
                        Log.d("[GudangGaram]", "pic_name          : " + obj3.getProperty(22).toString());
                        Log.d("[GudangGaram]", "opname_code       : " + obj3.getProperty(23).toString());

                        oad.setDtl_ID(Integer.parseInt(obj3.getProperty(0).toString()));
                        oad.setSeq(Integer.parseInt(obj3.getProperty(1).toString()));
                        oad.setFileID(Integer.parseInt(obj3.getProperty(2).toString()));
                        oad.setOrgID(Integer.parseInt(obj3.getProperty(3).toString()));
                        oad.setOrgName(obj3.getProperty(4).toString());
                        oad.setItemID(Integer.parseInt(obj3.getProperty(5).toString()));
                        oad.setItemCode(obj3.getProperty(6).toString());
                        oad.setItemDesc(obj3.getProperty(7).toString());
                        oad.setItemOld(obj3.getProperty(8).toString());
                        oad.setLocRak(obj3.getProperty(9).toString());
                        oad.setLocBin(obj3.getProperty(10).toString());
                        oad.setLocComp(obj3.getProperty(11).toString());
                        oad.setLotNum(obj3.getProperty(12).toString());
                        oad.setKoliNum(obj3.getProperty(13).toString());
                        oad.setSubInvStatus(obj3.getProperty(14).toString());
                        oad.setRakSeq(Integer.parseInt(obj3.getProperty(15).toString()));
                        oad.setBinSeq(Integer.parseInt(obj3.getProperty(16).toString()));
                        oad.setQtyOnHand(Double.parseDouble(obj3.getProperty(17).toString()));
                        oad.setQtyPicked(Double.parseDouble(obj3.getProperty(18).toString()));
                        oad.setQtyCheck(0.00);
                        oad.setNotes("");
                        oad.setUom(obj3.getProperty(19).toString());
                        oad.setPicNIK(obj3.getProperty(20).toString());
                        oad.setPicPersonID(Integer.parseInt(obj3.getProperty(21).toString()));
                        oad.setPicName(obj3.getProperty(22).toString());
                        oad.setOpnameCode(obj3.getProperty(23).toString());
                        oad.setCreationDate(xp_creation_date);
                        oad.setChkFlag(false);

                        // add to database
                        dbHelper.addOpnameData(oad);

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        publishProgress("Download AccOpname Record " + Integer.toString(i+1)  + " of " + Integer.toString(ncount));
                    }
                } else {
                    Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                }
            } catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }
        } catch (Exception ex) {
            Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtlTaskAsync Catch Http Transport : " + ex.getMessage().toString());
        } finally {
            Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtlTaskAsync  :: end doInBackground");
        }

        return file_id;
    }
}