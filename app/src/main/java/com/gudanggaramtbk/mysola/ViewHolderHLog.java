package com.gudanggaramtbk.mysola;

import android.widget.TextView;

/**
 * Created by luckym on 12/3/2018.
 */

public class ViewHolderHLog  {
    public TextView Xtxt_HLogID;
    public TextView Xtxt_HLog_NoTrx;
    public TextView Xtxt_HLog_TglOpnam;
    public TextView Xtxt_HLog_Gudang;
    public TextView Xtxt_HLog_Rak;
    public TextView Xtxt_HLog_Bin;
}
