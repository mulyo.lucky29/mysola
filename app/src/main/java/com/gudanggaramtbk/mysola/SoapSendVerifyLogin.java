package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by LuckyM on 1/24/2019.
 */

public class SoapSendVerifyLogin {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper                    dbHelper;
    private Login                             pActivity;
    private String                            HeaderID;
    private LoginSession                      LoginResult;


    // override constructor
    public SoapSendVerifyLogin(SharedPreferences PConfig){
        Log.d("[GudangGaram]:", "SoapSendVerifyLogin :: Constructor");
        this.config           = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]:", "SoapSendVerifyLogin :: SetContext");
        this.context             = context;
    }
    public void setParentActivity(Login pActivity){
        this.pActivity = pActivity;
    }

    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]:", "SoapSendVerifyLogin :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Verify(String p_username , final String p_password) {
        Log.d("[GudangGaram]", "SoapSendVerifyLogin :: Verify > Begin");
        LoginResult = new LoginSession();
        try {
            // call WS To Verify Login To Server
            SoapSendVerifyLoginTaskAsync SoapRequest = new SoapSendVerifyLoginTaskAsync(new SoapSendVerifyLoginTaskAsync.SoapSendVerifyLoginTaskAsyncResponse() {
                @Override
                public void PostSentAction(LoginSession output) {
                    pActivity.set_LoginSession(output);
                }
            });

            SoapRequest.setAttribute(context, dbHelper, config, p_username, p_password);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            } else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.d("[GudangGaram]:", "Send Opname Data End");
        }
    }
}
