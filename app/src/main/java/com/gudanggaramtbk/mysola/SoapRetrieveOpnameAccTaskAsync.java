package com.gudanggaramtbk.mysola;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 11/15/2018.
 */

public class SoapRetrieveOpnameAccTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences   config;
    private Context             context;
    private ProgressDialog      pd;
    private MySQLiteHelper      dbHelper;
    private String              SentResponse;
    private String              Nik;
    private String              DeviceID;
    private ListView            listOpnameAcc;
    private List<OpnameListAcc> Result;
    private SyncAction          pActivity;

    CustomListAdapterDOA        oladapter;
    //private ArrayAdapter<String> adapter;

    public SoapRetrieveOpnameAccTaskAsync() { }
    public interface SoapRetrieveOpnameAccTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveOpnameAccTaskAsync.SoapRetrieveOpnameAccTaskAsyncResponse delegate = null;
    public SoapRetrieveOpnameAccTaskAsync(SoapRetrieveOpnameAccTaskAsync.SoapRetrieveOpnameAccTaskAsyncResponse delegate) {
        this.delegate = delegate;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public void setParentActivity(SyncAction opActivity){
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccDtl :: SetParentActivity");
        this.pActivity = opActivity;
    }
    public void setAttribute(Context context, MySQLiteHelper dbHelper, SharedPreferences config, ListView lst, String pNik, String pDeviceID) {
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccTaskAsync :: setAttribute");
        this.context    = context;
        this.dbHelper   = dbHelper;
        this.config     = config;
        this.pd         = new ProgressDialog(this.context);
        this.listOpnameAcc = lst;
        this.Nik           = pNik;
        this.DeviceID      = pDeviceID;
        Result             = new ArrayList<OpnameListAcc>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveOpnameAccTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait Retrieving Data From Oracle Inprogress ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    protected void onProgressUpdate(String[] args) {
        //progressDialog.setMessage(args[0]);
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        try {
            //listOpnameLog.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            oladapter = new CustomListAdapterDOA(context, pActivity, dbHelper, config, DeviceID, Result, listOpnameAcc);
            oladapter.notifyDataSetChanged();
            listOpnameAcc.setAdapter(oladapter);

            long xcount = listOpnameAcc.getCount();
            if (xcount == 0) {
                Toast.makeText(context, "No Data Result", Toast.LENGTH_LONG).show();
            }
            delegate.PostSentAction(output);
        } catch (Exception e) {
        }
        Toast.makeText(context, "Retrieve Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        int count;
        //String response = null;
        String rak_persiapan = "";

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Task";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());
        Log.d("[GudangGaram]", "SOAP REQUEST PARAM P_NIK  :" + Nik);

        // =============== p_pic_nik ===================
        PropertyInfo prop_p_pic_nik = new PropertyInfo();
        prop_p_pic_nik.setName("p_pic_nik");
        prop_p_pic_nik.setValue(Nik);
        prop_p_pic_nik.setType(String.class);

        request.addProperty(prop_p_pic_nik);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS, 6000);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));

                Result.clear();
                if (obj2.getPropertyCount() > 0) {
                    for (int i = 0; i < obj2.getPropertyCount(); i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);
                        OpnameListAcc oa = new OpnameListAcc();
                        oa.setTaskID(obj3.getProperty(0).toString());
                        oa.setFileID(obj3.getProperty(1).toString());
                        oa.setFileName(obj3.getProperty(2).toString());
                        oa.setOpnameCode(obj3.getProperty(3).toString());
                        oa.setAssignedDate(obj3.getProperty(4).toString());
                        oa.setAssignedNIK(Nik);

                        Log.d("[GudangGaram]: ", "Task ID       = " + obj3.getProperty(0).toString());
                        Log.d("[GudangGaram]: ", "File ID       = " + obj3.getProperty(1).toString());
                        Log.d("[GudangGaram]: ", "File Name     = " + obj3.getProperty(2).toString());
                        Log.d("[GudangGaram]: ", "Opname Code   = " + obj3.getProperty(3).toString());
                        Log.d("[GudangGaram]: ", "Assigned Date = " + obj3.getProperty(4).toString());
                        Log.d("[GudangGaram]: ", "Assigned Nik  = " + Nik);

                        Result.add(oa);
                    }
                } else {
                    Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                }
            } catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }
        } catch (Exception ex) {
            Log.d("[GudangGaram]", "SoapRetrieveOpnameAccTaskAsync Catch Http Transport : " + ex.getMessage().toString());
        } finally {
            Log.d("[GudangGaram]", "SoapRetrieveOpnameAccTaskAsync  :: end doInBackground");
        }

        return "";
    }
}