package com.gudanggaramtbk.mysola;

    import android.app.ProgressDialog;
    import android.content.Context;
    import android.content.SharedPreferences;
    import android.os.AsyncTask;
    import android.util.Log;
    import android.widget.ArrayAdapter;
    import android.widget.Toast;
    import org.ksoap2.SoapEnvelope;
    import org.ksoap2.serialization.PropertyInfo;
    import org.ksoap2.serialization.SoapObject;
    import org.ksoap2.serialization.SoapSerializationEnvelope;
    import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by luckym on 7/13/2018.
 */

public class SoapRetrievePICTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String                    ResultResponse;
    private MySQLiteHelper            dbHelper;
    private String                    SentResponse;
    private String                    TaskID;
    private String                    picNIK;

    public SoapRetrievePICTaskAsync(){}
    public interface SoapRetrievePICTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrievePICTaskAsync.SoapRetrievePICTaskAsyncResponse delegate = null;
    public SoapRetrievePICTaskAsync(SoapRetrievePICTaskAsync.SoapRetrievePICTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper    dbHelper,
                             SharedPreferences config){
        Log.d("[GudangGaram]", "SoapRetrievePICTaskAsync :: setAttribute");
        this.context          = context;
        this.dbHelper         = dbHelper;
        this.config           = config;
        this.pd = new ProgressDialog(this.context);
        //Result = new ArrayList<DS_TaskItemDetail>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrievePICTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Retrieve List PIC From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        pd.setMessage(args[0].toString());
        Log.d("[GudangGaram]", "SoapRetrievePICTaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrievePIC :: onPostExecute >> " + ResultResponse);
        try{
            delegate.PostSentAction(ResultResponse);
        }
        catch(Exception e){
        }
        finally {
            if(output.equals("-1")){
                Toast.makeText(context,"Retrieve PIC Done", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(context,"Retrieve PIC" + output + " Done", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Long     Result;
        String   v_PICPersonID;
        String   v_PicNik;
        String   v_PICName;
        Integer  ncount;
        Integer  nerr;

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Get_List_Pic";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);
            SoapObject obj, obj1, obj2, obj3;
            obj     = (SoapObject) envelope.getResponse();
            obj1    = (SoapObject) obj.getProperty("diffgram");
            obj2    = (SoapObject) obj1.getProperty("NewDataSet");

            ncount = obj2.getPropertyCount();
            nerr   = 0;
            Log.d("[GudangGaram]", ">>> count  >>> " + ncount.toString());
            if(ncount > 0){
                for (int i = 0; i < ncount ;i++){
                    obj3 = (SoapObject) obj2.getProperty(i);

                    Log.d("[GudangGaram]", "----------------------------------------------------------");
                    Log.d("[GudangGaram]", ">>> PersonID      >>> " + obj3.getProperty(0).toString());
                    Log.d("[GudangGaram]", ">>> NIK           >>> " + obj3.getProperty(1).toString());
                    Log.d("[GudangGaram]", ">>> Name          >>> " + obj3.getProperty(2).toString());

                    v_PICPersonID = obj3.getProperty(0).toString();
                    v_PicNik      = obj3.getProperty(1).toString();
                    v_PICName     = obj3.getProperty(2).toString();

                    Result =  dbHelper.addPIC(
                            v_PICPersonID,
                            v_PicNik,
                            v_PICName);

                    if(Result > 0){
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        publishProgress("Add PIC Info [ " + Integer.toString(i+1) + " of " + ncount + " ] : " + v_PicNik + " - " + v_PICName);
                    }
                    else{
                        nerr +=1;
                    }

                    Thread.sleep(500);
                }
                if(nerr > 0) {
                    ResultResponse = "*";
                }
                else{
                    ResultResponse = TaskID;
                }
            }
            else{
                ResultResponse = "*";
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrievePICTaskAsync  :: end doInBackground");
        }

        return "";
    }
}
