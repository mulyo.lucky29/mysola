package com.gudanggaramtbk.mysola;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.ksoap2.transport.HttpTransportSE;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by luckym on 10/24/2018.
 */

public class MySQLiteHelper extends SQLiteOpenHelper  {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MySola.db";
    private static final String TABLE_M_PIC = "GGGG_SOLA_M_PIC";
    private static final String TABLE_M_OPNAME = "GGGG_SOLA_T_OPNAME";
    private static final String TABLE_M_LOOKUP = "GGGG_SOLA_M_LOOKUP";
    private static final String TABLE_H_ACC = "GGGG_SOLA_H_ACC";
    private static final String TABLE_H_LOG = "GGGG_SOLA_H_LOG";

    private static final String VIEW_LOGIN = "GGGG_SOLA_V_LOGIN";
    private static final String VIEW_KODE_OPNAME_ACC = "GGGG_SOLA_KODE_OPNAME_ACC_V";

    // ========== table historical trx no ====================
    private static final String KEY_H_ACC_ID = "HACC_ID";
    private static final String KEY_H_ACC_NoTrx = "HACC_NoTrx";
    private static final String KEY_H_ACC_KodeOpname = "HACC_KodeOpname";
    private static final String KEY_H_ACC_TglOpname = "HACC_TglOpname";
    private static final String KEY_H_ACC_Gudang = "HACC_Gudang";
    private static final String KEY_H_ACC_NIK = "HACC_NIK";

    private static final String KEY_H_LOG_ID = "HLOG_ID";
    private static final String KEY_H_LOG_NoTrx = "HLOG_NoTrx";
    private static final String KEY_H_LOG_KodeOpname = "HLOG_KodeOpname";
    private static final String KEY_H_LOG_TglOpaname = "HLOG_TglOpname";
    private static final String KEY_H_LOG_Gudang = "HLOG_Gudang";
    private static final String KEY_H_LOG_Rak = "HLOG_Rak";
    private static final String KEY_H_LOG_Bin = "HLOG_Bin";
    private static final String KEY_H_LOG_NIK = "HLOG_NIK";

    // =========== table opname ==============================
    private static final String KEY_T_SOACC_DTL_ID = "Dtl_ID";
    private static final String KEY_T_FILE_ID  = "FileID";
    private static final String KEY_T_SEQUENCE_NUMBER = "Seq";
    private static final String KEY_T_ORG_ID = "OrgID";
    private static final String KEY_T_ORG_NAME = "OrgName";
    private static final String KEY_T_ITEM_ID = "ItemID";
    private static final String KEY_T_ITEM_CODE = "ItemCode";
    private static final String KEY_T_ITEM_DESC = "ItemDesc";
    private static final String KEY_T_ITEM_OLD = "ItemOld";
    private static final String KEY_T_RAK = "LocRak";
    private static final String KEY_T_BIN = "LocBin";
    private static final String KEY_T_COMP = "LocComp";
    private static final String KEY_T_LOT_NUM = "LotNum";
    private static final String KEY_T_KOLI_NUM = "KoliNum";
    private static final String KEY_T_SUBINV_STATUS = "SubInvStatus";
    private static final String KEY_T_RAK_SEQ = "RakSeq";
    private static final String KEY_T_BIN_SEQ = "BinSeq";
    private static final String KEY_T_QTY_ONHAND = "QtyOnHand";
    private static final String KEY_T_QTY_PICKED = "QtyPicked";
    private static final String KEY_T_QTY_CHECK = "QtyCheck";
    private static final String KEY_T_NOTES = "Notes";
    private static final String KEY_T_UOM = "Uom";
    private static final String KEY_T_PIC_NIK = "PicNIK";
    private static final String KEY_T_PIC_PERSON_ID = "PicPersonID";
    private static final String KEY_T_PIC_NAME = "PicName";
    private static final String KEY_T_CREATION_DATE = "CreationDate";
    private static final String KEY_T_OPNAME_DATE = "OpnameDate";
    private static final String KEY_T_OPNAME_NUMBER = "OpnameNumber";
    private static final String KEY_T_OPNAME_CODE = "OpnameCode";
    private static final String KEY_T_CHK_FLAG = "ChkFlag";

    // =========== Master PIC ================================
    private static final String KEY_M_PIC_PERSON_ID = "Person_ID";
    private static final String KEY_M_PIC_NIK = "PIC_NIK";
    private static final String KEY_M_PIC_NAME = "PIC_Name";

    // =========== Master Lookup =============================
    private static final String KEY_M_LOOKUP_CONTEXT = "LContext";
    private static final String KEY_M_LOOKUP_VALUE = "LValue";
    private static final String KEY_M_LOOKUP_MEANNG = "LMeaning";

    // =============================================================
    private static Cursor XMICursor;
    private static Cursor XMLCursor;

    public String Get_DatabaseName() {
        return DATABASE_NAME;
    }

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    private static final String CREATE_H_LOG_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_H_LOG + " ( " +
            KEY_H_LOG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_H_LOG_NoTrx + " TEXT, " +
            KEY_H_LOG_KodeOpname + " TEXT, " +
            KEY_H_LOG_TglOpaname + " TEXT, " +
            KEY_H_LOG_Gudang + " TEXT, " +
            KEY_H_LOG_Rak + " TEXT, " +
            KEY_H_LOG_Bin + " TEXT, " +
            KEY_H_LOG_NIK + " TEXT); ";

    private static final String CREATE_H_ACC_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_H_ACC + " ( " +
            KEY_H_ACC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_H_ACC_NoTrx + " TEXT, " +
            KEY_H_ACC_KodeOpname + " TEXT, " +
            KEY_H_ACC_TglOpname + " TEXT, " +
            KEY_H_ACC_Gudang + " TEXT, " +
            KEY_H_ACC_NIK + " TEXT); ";

    private static final String CREATE_LOGIN_VW = " CREATE VIEW IF NOT EXISTS " + VIEW_LOGIN + " " +
            "  AS " +
            "  SELECT PersonID, NIK, Name " +
            "  FROM ( " +
            "  SELECT " + KEY_M_PIC_PERSON_ID + " as PersonID," +
            " " + KEY_M_PIC_NIK + " as NIK," +
            " " + KEY_M_PIC_NAME + " as Name " +
            " FROM " + TABLE_M_PIC + " UNION ALL " +
            " SELECT -1 as PersonID, '999999999' as NIK, 'Default' as Name); ";

    private static final String CREATE_KODE_OPNAME_ACC_V = " CREATE VIEW IF NOT EXISTS " + VIEW_KODE_OPNAME_ACC + " " +
            " AS " +
            " SELECT OpnameCode, FileID, picNIK " +
            " FROM GGGG_SOLA_T_OPNAME " +
            " WHERE ChkFlag = 0 " +
            " GROUP BY OpnameCode, FileID, picNIK " +
            " ORDER BY FileID; ";

    private static final String DROP_LOGIN_VW = " DROP VIEW IF EXISTS " + VIEW_LOGIN + ";";

    private static final String CREATE_OPNAME_TABLE = " CREATE TABLE IF NOT EXISTS " + TABLE_M_OPNAME + " ( " +
            KEY_T_SOACC_DTL_ID + " INTEGER PRIMARY KEY, " +
            KEY_T_FILE_ID + " INTEGER, " +
            KEY_T_SEQUENCE_NUMBER + " INTEGER, " +
            KEY_T_ORG_ID + " INTEGER, " +
            KEY_T_ORG_NAME + " TEXT, " +
            KEY_T_ITEM_ID + " INTEGER, " +
            KEY_T_ITEM_CODE + " TEXT, " +
            KEY_T_ITEM_DESC + " TEXT, " +
            KEY_T_ITEM_OLD + " TEXT, " +
            KEY_T_RAK + " TEXT, " +
            KEY_T_BIN + " TEXT, " +
            KEY_T_COMP + " TEXT, " +
            KEY_T_SUBINV_STATUS + " TEXT, " +
            KEY_T_LOT_NUM + " TEXT, " +
            KEY_T_KOLI_NUM + " TEXT, " +
            KEY_T_RAK_SEQ + " INTEGER, " +
            KEY_T_BIN_SEQ + " INTEGER, " +
            KEY_T_QTY_ONHAND + " NUMERIC, " +
            KEY_T_QTY_PICKED + " NUMERIC, " +
            KEY_T_QTY_CHECK + " NUMERIC, " +
            KEY_T_UOM + " TEXT, " +
            KEY_T_PIC_NIK + " TEXT, " +
            KEY_T_PIC_PERSON_ID + " INTEGER, " +
            KEY_T_PIC_NAME + " TEXT, " +
            KEY_T_NOTES + " TEXT, " +
            KEY_T_CREATION_DATE + " TEXT, " +
            KEY_T_OPNAME_DATE + " TEXT, " +
            KEY_T_OPNAME_NUMBER + " TEXT, " +
            KEY_T_OPNAME_CODE + " TEXT, " +
            KEY_T_CHK_FLAG + " INTEGER ); ";

    private static final String CREATE_MPIC_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_PIC + " ( " +
            KEY_M_PIC_PERSON_ID + " INTEGER PRIMARY KEY, " +
            KEY_M_PIC_NIK + " TEXT, " +
            KEY_M_PIC_NAME + " TEXT); ";

    private static final String CREATE_MLOOKUP_TABLE = " CREATE TABLE IF NOT EXISTS " + TABLE_M_LOOKUP + " ( " +
            KEY_M_LOOKUP_CONTEXT + " TEXT, " +
            KEY_M_LOOKUP_VALUE + " TEXT, " +
            KEY_M_LOOKUP_MEANNG + " TEXT); ";

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("[GudangGaram]", "MySQLiteHelper :: onCreate");

        db.execSQL(CREATE_MPIC_TABLE);
        db.execSQL(CREATE_MLOOKUP_TABLE);
        db.execSQL(CREATE_OPNAME_TABLE);
        db.execSQL(CREATE_H_LOG_TABLE);
        db.execSQL(CREATE_H_ACC_TABLE);
        db.execSQL(CREATE_LOGIN_VW);
        db.execSQL(CREATE_KODE_OPNAME_ACC_V);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older master table if existed
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_M_ITEM);
        //onCreate(db);
    }

    public void flushAll() {
        flushTable(TABLE_M_PIC);
        flushTable(TABLE_H_ACC);
        flushTable(TABLE_H_LOG);
        flushTable(TABLE_M_LOOKUP);
        flushTable(TABLE_M_OPNAME);
    }

    // ================================= db operation util =========================================
    public void flushTable(String pTableName) {
        Log.d("[GudangGaram]", "Flush Table : " + pTableName);
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(pTableName, null, null);
        //db.close();
    }

    public void execute(String pquery) {
        Log.d("[GudangGaram]", "Execute : " + pquery);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(pquery);
        //db.close();
    }

    /*
    public boolean isNetworkConnected(SharedPreferences config, Context ctx) {
        String xfactor;
        String Message;
        Message = "Checking Connection ... Please Wait ...";

        xfactor = "S";
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        String SSoapAddress = config.getString("SoapAddress", "");
        String SSoapNamespace = config.getString("SoapNamespace", "");
        String SSoapMethodTest = "Get_Test_Result";

        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SSoapAddress);
            httpTransport.debug = true;
            httpTransport.call(SSoapNamespace + SSoapMethodTest, envelope);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        } catch (Exception ex) {
            xfactor = ex.getMessage().toString();
            Toast.makeText(ctx, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
        }
        if (xfactor == "S") {
            Toast.makeText(ctx, "Test Connection Succeed ", Toast.LENGTH_LONG).show();
            return true;
        } else {
            return false;
        }
    }
    */

    /*
    public boolean isServiceConnect(SharedPreferences config, ConnectivityManager cm, Context ctx)
    {
        Boolean status = false;
        String SSoapAddress = config.getString("SoapAddress", "");
        Log.d("[GudangGaram]", "isServiceConnect :" + SSoapAddress);
        try{
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected())
            {
                //Network is available but check if we can get access from the network.
                URL url = new URL(SSoapAddress);
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(10000); // Timeout 10 seconds.
                urlc.connect();
                if (urlc.getResponseCode() == 200)  //Successful response.
                {
                    Log.d("[GudangGaram]", "Connection Exists");
                    status = true;
                }
                else
                {
                    Log.d("[GudangGaram]", "NO Connection");
                    status = false;
                }
            }
        }
        catch(Exception e) {
            status = false;
            e.printStackTrace();
        }
        Log.d("[GudangGaram]", "isServiceConnect Status = " + status);

        return status;
    }
*/

    public boolean isServiceConnect(SharedPreferences config, ConnectivityManager cm, Context ctx)
    {
        Boolean status = false;
        String SSoapAddress = config.getString("SoapAddress", "");
        try
        {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(SSoapAddress,1000);
            status = true;
        }
        catch(NullPointerException e){
            status = false;
        }
        return status;
        /*
        try{
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected())
            {
                //Network is available but check if we can get access from the network.
                URL url = new URL("http://10.50.131.18/WSSolaDev/WSSola.asmx");
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(10000); // Timeout 2 seconds.
                urlc.connect();
                if (urlc.getResponseCode() == 200)  //Successful response.
                {
                    return true;
                }
                else
                {
                    Log.d("NO INTERNET", "NO INTERNET");
                    return false;
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return false;
        */
    }


    public int getRecordCount(String pTableName, String pWhere) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            String count = "SELECT count(*) FROM " + pTableName + " " + pWhere;
            Cursor mcursor = db.rawQuery(count, null);
            mcursor.moveToFirst();
            int icount = mcursor.getInt(0);
            if (icount > 0) {
                return icount;
            } else {
                return 0;
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
            return 0;
        } finally {
            //db.close();
        }
    }

    public List<OpnameData> getOpnameAccList(String pFileID, String pNik){
        Integer position;
        List<OpnameData> lvi = new ArrayList<OpnameData>();
        SQLiteDatabase db = this.getReadableDatabase();
        String kueri = " SELECT Dtl_ID, " +
                              " Seq, " +
                              " ItemID, " +
                              " ItemCode, " +
                              " ItemDesc, " +
                              " ItemOld, " +
                              " LocRak, " +
                              " LocBin, " +
                              " LocComp, " +
                              " SubInvStatus, " +
                              " KoliNum, " +
                              " LotNum, " +
                              " QtyOnHand, " +
                              " QtyPicked, " +
                              " Uom,  " +
                              " Notes " +
                " FROM GGGG_SOLA_T_OPNAME " +
                " WHERE 1 = 1 " +
                "  AND FileID = " + pFileID +
                "  AND PicNik = '" + pNik + "' " +
                " ORDER BY Seq ";

        XMICursor = db.rawQuery(kueri, null);
        try {
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();

                position = 0;
                while (!XMICursor.isAfterLast()) {

                    OpnameData oa = new OpnameData();
                    oa.setDtl_ID(XMICursor.getInt(0));
                    oa.setSeq(XMICursor.getInt(1));
                    oa.setItemID(XMICursor.getInt(2));
                    oa.setItemCode(XMICursor.getString(3));
                    oa.setItemDesc(XMICursor.getString(4));
                    oa.setItemOld(XMICursor.getString(5));
                    oa.setLocRak(XMICursor.getString(6));
                    oa.setLocBin(XMICursor.getString(7));
                    oa.setLocComp(XMICursor.getString(8));
                    oa.setSubInvStatus(XMICursor.getString(9));
                    oa.setKoliNum(XMICursor.getString(10));
                    oa.setLotNum(XMICursor.getString(11));
                    oa.setQtyOnHand(XMICursor.getDouble(12));
                    oa.setQtyPicked(XMICursor.getDouble(13));
                    oa.setQtyCheck(0.00);
                    oa.setUom(XMICursor.getString(14));
                    oa.setNotes(XMICursor.getString(15));

                    lvi.add(oa);
                    position +=1;
                    XMICursor.moveToNext();
                }
            }
        } catch (Exception e) {
        } finally {
            XMICursor.close();
            //db.close();
        }
        return lvi;
    }

    public List<StringWithTag> getEmployeeList() {
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();

        SQLiteDatabase db = this.getReadableDatabase();
        String kueri = " SELECT NIK, NAME " +
                " FROM " + VIEW_LOGIN +
                " ORDER BY NIK ";

        XMICursor = db.rawQuery(kueri, null);
        try {
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast()) {
                    String key = XMICursor.getString(1);
                    String value = XMICursor.getString(0);
                    Log.d("[GudangGaram]", "KVP Load : (Key,Value) -> " + key + "," + value);
                    lvi.add(new StringWithTag(value, key));
                    XMICursor.moveToNext();
                }
            }
        } catch (Exception e) {
        } finally {
            XMICursor.close();
            //db.close();
        }
        return lvi;
    }

    public long addPIC(
            String pPersonID,
            String pNIK,
            String pName) {
        long savestatus;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try {
            values.put(KEY_M_PIC_PERSON_ID, Integer.parseInt(pPersonID));
            values.put(KEY_M_PIC_NIK, pNIK);
            values.put(KEY_M_PIC_NAME, pName);
            savestatus = db.insert(TABLE_M_PIC, null, values); // key/value -> keys = column names/ values = column values
        } catch (Exception e) {
            savestatus = -1;
        } finally {
            //db.close();
        }
        return savestatus;
    }


    public void delOpnameData(String  p_soacc_dtl_id)
    {
        Log.d("[GudangGaram]","MySQLiteHelper :: delOpnameData");
        String pWehere = KEY_T_SOACC_DTL_ID + "= " + p_soacc_dtl_id + " ";
        SQLiteDatabase db  = this.getWritableDatabase();
        try{
            db.delete(TABLE_M_OPNAME,pWehere, null);
        }
        catch(Exception e){
            Log.d("[GudangGaram]","MySQLiteHelper :: delOpnameData Exception : " + e.getMessage().toString());
        }
    }

    public void updateOpnameData(String p_soacc_dtl_id, String p_notes){
        Log.d("[GudangGaram]","MySQLiteHelper :: updateOpnameData");
        String pWehere = KEY_T_SOACC_DTL_ID + "= " + p_soacc_dtl_id + " ";

        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_T_NOTES, p_notes);
        try{
            db.update(TABLE_M_OPNAME,args,pWehere, null);
        }
        catch(Exception e){
            Log.d("[GudangGaram]","MySQLiteHelper :: delOpnameData Exception : " + e.getMessage().toString());
        }

    }

    public long addOpnameData(OpnameData oad)
    {
        OpnameData xoad = oad;
        Log.d("[GudangGaram]","MySQLiteHelper :: addOpnameData");
        long savestatus;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_T_SOACC_DTL_ID, Integer.parseInt(xoad.getDtl_ID().toString()));
            values.put(KEY_T_FILE_ID, xoad.getFileID());
            values.put(KEY_T_SEQUENCE_NUMBER, xoad.getSeq());
            values.put(KEY_T_ORG_ID, xoad.getOrgID());
            values.put(KEY_T_ORG_NAME, xoad.getOrgName().toString());
            values.put(KEY_T_ITEM_ID, xoad.getItemID());
            values.put(KEY_T_ITEM_CODE, xoad.getItemCode());
            values.put(KEY_T_ITEM_DESC, xoad.getItemDesc());
            values.put(KEY_T_ITEM_OLD, xoad.getItemOld());
            values.put(KEY_T_RAK, xoad.getLocRak());
            values.put(KEY_T_BIN, xoad.getLocBin());
            values.put(KEY_T_COMP, xoad.getLocComp());
            values.put(KEY_T_LOT_NUM, xoad.getLotNum());
            values.put(KEY_T_KOLI_NUM, xoad.getKoliNum());
            values.put(KEY_T_SUBINV_STATUS, xoad.getSubInvStatus());
            values.put(KEY_T_RAK_SEQ, xoad.getRakSeq());
            values.put(KEY_T_BIN_SEQ, xoad.getBinSeq());
            values.put(KEY_T_QTY_ONHAND, xoad.getQtyOnHand());
            values.put(KEY_T_QTY_PICKED, xoad.getQtyPicked());
            values.put(KEY_T_QTY_CHECK, xoad.getQtyCheck());
            values.put(KEY_T_NOTES, xoad.getNotes());
            values.put(KEY_T_UOM, xoad.getUom());
            values.put(KEY_T_PIC_NIK, xoad.getPicNIK());
            values.put(KEY_T_PIC_PERSON_ID, xoad.getPicPersonID());
            values.put(KEY_T_PIC_NAME, xoad.getPicName());
            values.put(KEY_T_OPNAME_CODE, xoad.getOpnameCode());
            values.put(KEY_T_CREATION_DATE, xoad.getOpnameDate());
            values.put(KEY_T_CHK_FLAG,xoad.getChkFlag());

            savestatus = db.insert(TABLE_M_OPNAME, null, values); // key/value -> keys = column names/ values = column values
        }
        catch(Exception e) {
            Log.d("[GudangGaram]","MySQLiteHelper :: addOpnameData Exception : " + e.getMessage().toString());
            savestatus = -1;
        }
        finally {
            //db.close();
        }
        return savestatus;
    }

    public String getGudangName(String pFileID){
        String result = "";
        String kueri = " SELECT OrgName " +
                       " FROM GGGG_SOLA_T_OPNAME " +
                       " WHERE FileID = " + pFileID + " " +
                       " GROUP BY OrgName;";

        SQLiteDatabase db = this.getReadableDatabase();
        XMLCursor = db.rawQuery(kueri, null);
        try{
            XMLCursor.moveToFirst();
            while (!XMLCursor.isAfterLast())
            {
                result = XMLCursor.getString(0);
                XMLCursor.moveToNext();
            }
        }
        catch(Exception e){
        }
        finally {
            XMLCursor.close();
            // db.close();
        }
        return result;
    }


    public List<StringWithTag> loadKodeStockOpnameAcc(String pNik){
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();

        SQLiteDatabase db =  this.getReadableDatabase();
        String kueri = " SELECT OpnameCode,FileID " +
                       " FROM GGGG_SOLA_KODE_OPNAME_ACC_V " +
                       " WHERE picNIK = '" + pNik + "' " +
                       " ORDER BY FileID ";

        Log.d("[GudangGaram]","MySQLiteHelper :: loadKodeStockOpnameAcc : " + kueri);

        XMLCursor = db.rawQuery(kueri, null);
        try{
            if(XMLCursor.getCount() > 0) {
                XMLCursor.moveToFirst();
                while (!XMLCursor.isAfterLast())
                {
                    String key  = XMLCursor.getString(1);
                    String value = XMLCursor.getString(0);
                    Log.d("[GudangGaram]", "MySQLiteHelper :: loadKodeStockOpnameAcc :: KVP Load : (Key,Value) -> " + key + "," + value);
                    lvi.add(new StringWithTag(value, key));
                    XMLCursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            XMLCursor.close();
        }
        return lvi;
    }

    public void del_HAccList(String pHAcc_ID){
        Log.d("[GudangGaram]","MySQLiteHelper :: del_HAccList");
        String pWehere = KEY_H_ACC_ID + "= " + pHAcc_ID + " ";
        SQLiteDatabase db  = this.getWritableDatabase();
        try{
            db.delete(TABLE_H_ACC,pWehere, null);
        }
        catch(Exception e){
            Log.d("[GudangGaram]","MySQLiteHelper :: del_HAccList Exception : " + e.getMessage().toString());
        }
    }

    public long add_HAccList(OpnameHAcc pOHAcc)
    {
        OpnameHAcc OHAcc = pOHAcc;
        Log.d("[GudangGaram]","MySQLiteHelper :: add_HAccList");
        long savestatus;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_H_ACC_NoTrx, OHAcc.getHAcc_NoTrx());
            values.put(KEY_H_ACC_KodeOpname, OHAcc.getHAcc_KodeOpnam());
            values.put(KEY_H_ACC_TglOpname, OHAcc.getHAcc_TglOpnam());
            values.put(KEY_H_ACC_Gudang, OHAcc.getHAcc_Gudang());
            values.put(KEY_H_ACC_NIK, OHAcc.getHAcc_NIK());

            savestatus = db.insert(TABLE_H_ACC, null, values); // key/value -> keys = column names/ values = column values
        }
        catch(Exception e) {
            Log.d("[GudangGaram]","MySQLiteHelper :: add_HAccList Exception : " + e.getMessage().toString());
            savestatus = -1;
        }
        finally {
        }
        return savestatus;
    }


    public List<OpnameHAcc> get_HAcc_List(String pNik)
    {
        List<OpnameHAcc> ListHAcc = new ArrayList<OpnameHAcc>();
        SQLiteDatabase db = this.getWritableDatabase();

        String kueri = " select HACC_ID, HACC_NoTrx, HACC_KodeOpname, HACC_TglOpname, HACC_Gudang " +
                       " from GGGG_SOLA_H_ACC " +
                       " where HACC_NIK = '" + pNik + "' " +
                       " order by HACC_TglOpname;";

        Log.d("[GudangGaram]:", "MySQLiteHelper :: get_HAcc_List : " + kueri);

        try{
            XMLCursor = db.rawQuery(kueri, null);
            XMLCursor.moveToFirst();
            while (!XMLCursor.isAfterLast())
            {
                OpnameHAcc ohaac = new OpnameHAcc();
                ohaac.setHAccID(XMLCursor.getString(0));
                ohaac.setHAcc_NoTrx(XMLCursor.getString(1));
                ohaac.setHAcc_KodeOpnam(XMLCursor.getString(2));
                ohaac.setHAcc_TglOpnam(XMLCursor.getString(3));
                ohaac.setHAcc_Gudang(XMLCursor.getString(4));
                ohaac.setHAcc_NIK(pNik);

                ListHAcc.add(ohaac);
                XMLCursor.moveToNext();
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "MySQLiteHelper :: get_HAcc_List Exception : " + e.getMessage().toString());
        }
        finally {
        }

        return ListHAcc;
    }

    public void del_HLogList(String pHLog_ID){
        Log.d("[GudangGaram]","MySQLiteHelper :: del_HLogList");
        String pWehere = KEY_H_LOG_ID + "= " + pHLog_ID + " ";
        SQLiteDatabase db  = this.getWritableDatabase();
        try{
            db.delete(TABLE_H_LOG,pWehere, null);
        }
        catch(Exception e){
            Log.d("[GudangGaram]","MySQLiteHelper :: del_HLogList Exception : " + e.getMessage().toString());
        }
    }

    public long add_HLogList(OpnameHLog pOHLog)
    {
        OpnameHLog OHLog = pOHLog;
        Log.d("[GudangGaram]","MySQLiteHelper :: add_HLogList");
        long savestatus;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_H_LOG_NoTrx, OHLog.getHLog_NoTrx());
            values.put(KEY_H_LOG_TglOpaname, OHLog.getHLog_TglOpnam());
            values.put(KEY_H_LOG_Gudang, OHLog.getHLog_Gudang());
            values.put(KEY_H_LOG_Rak, OHLog.getHLog_Rak());
            values.put(KEY_H_LOG_Bin, OHLog.getHLog_Bin());
            values.put(KEY_H_LOG_NIK, OHLog.getHLog_NIK());

            savestatus = db.insert(TABLE_H_LOG, null, values); // key/value -> keys = column names/ values = column values
        }
        catch(Exception e) {
            Log.d("[GudangGaram]","MySQLiteHelper :: add_HLogList Exception : " + e.getMessage().toString());
            savestatus = -1;
        }
        finally {
        }
        return savestatus;
    }


    public List<OpnameHLog> get_HLog_List(String pNik){
        List<OpnameHLog> ListHLog = new ArrayList<OpnameHLog>();
        SQLiteDatabase db = this.getWritableDatabase();

        String kueri = " select HLOG_ID, HLOG_NoTrx, HLOG_TglOpname, HLOG_Gudang, HLOG_Rak, HLOG_Bin " +
                       " from GGGG_SOLA_H_LOG " +
                       " where HLOG_NIK = '" + pNik + "' " +
                       " order by HLOG_TglOpname;";

        Log.d("[GudangGaram]:", "MySQLiteHelper :: get_HLog_List : " + kueri);

        try{
            XMLCursor = db.rawQuery(kueri, null);
            XMLCursor.moveToFirst();
            while (!XMLCursor.isAfterLast())
            {
                OpnameHLog ohlog = new OpnameHLog();
                ohlog.setHLogID(XMLCursor.getString(0));
                ohlog.setHLog_NoTrx(XMLCursor.getString(1));
                ohlog.setHLog_TglOpnam(XMLCursor.getString(2));
                ohlog.setHLog_Gudang(XMLCursor.getString(3));
                ohlog.setHLog_Rak(XMLCursor.getString(4));
                ohlog.setHLog_Bin(XMLCursor.getString(5));
                ohlog.setHLog_NIK(pNik);

                ListHLog.add(ohlog);
                XMLCursor.moveToNext();
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "MySQLiteHelper :: get_HLog_List Exception : " + e.getMessage().toString());
        }
        finally {
        }

        return ListHLog;
    }
}
