package com.gudanggaramtbk.mysola;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import org.w3c.dom.Text;
import java.util.List;

// SearchView.OnQueryTextListener
public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {
    private MySQLiteHelper               dbHelper;
    private SharedPreferences            config;
    private String                       StrSessionNIK;
    private String                       StrSessionName;
    private String                       StrSessionGroup;
    TabHost                              thost;
    private ListView                     HLog_Hist;
    private ListView                     HAcc_Hist;
    private CustomListAdapterHAcc        adapter_hacc;
    private CustomListAdapterHLog        adapter_hlog;
    private List<OpnameHAcc>             lvi_hacc;
    private List<OpnameHLog>             lvi_hlog;
    private Message                      msg;


    public void EH_CMD_REFRESH_HACC(Context ctx){
        Integer lstcount;

        Log.d("[GudangGaram]:", "Main Activity :: EH_CMD_REFRESH_HACC Begin");
        try {
            lvi_hacc = dbHelper.get_HAcc_List(StrSessionNIK);
            lstcount = lvi_hacc.size();
            Log.d("[GudangGaram]:", "Main Activity :: EH_CMD_REFRESH_HACC : " + lstcount);
            if(lstcount > 0){
                adapter_hacc = new CustomListAdapterHAcc(ctx, lvi_hacc);
                adapter_hacc.notifyDataSetChanged();
                HAcc_Hist.setAdapter(adapter_hacc);
            }
            else{
                HAcc_Hist.setAdapter(null);
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]:","MainActivity :: EH_CMD_REFRESH_HACC Exception " + e.getMessage().toString());
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]:", "Main Activity :: EH_CMD_REFRESH_HACC End");
        }
    }

    public void EH_CMD_REFRESH_HLOG(Context ctx){
        Integer lstcount;

        Log.d("[GudangGaram]:", "Main Activity :: EH_CMD_REFRESH_HLOG Begin");
        try {
            lvi_hlog = dbHelper.get_HLog_List(StrSessionNIK);
            lstcount = lvi_hlog.size();
            Log.d("[GudangGaram]:", "Main Activity :: EH_CMD_REFRESH_HLOG : " + lstcount);
            if(lstcount > 0){
                adapter_hlog = new CustomListAdapterHLog(ctx, lvi_hlog);
                adapter_hlog.notifyDataSetChanged();
                HLog_Hist.setAdapter(adapter_hlog);
            }
            else{
                HLog_Hist.setAdapter(null);
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]:","MainActivity :: EH_CMD_REFRESH_HLOG Exception " + e.getMessage().toString());
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]:", "Main Activity :: EH_CMD_REFRESH_HLOG End");
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //}

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(StrSessionNIK.equals("999999999")){
                }
                else {
                    if (StrSessionGroup.equals("ACC")) {
                        EH_CMD_REFRESH_HACC(MainActivity.this);
                    } else if (StrSessionGroup.equals("LOG")) {
                        EH_CMD_REFRESH_HLOG(MainActivity.this);
                    }
                }
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        msg = new Message(MainActivity.this);

        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
        StrSessionGroup = getIntent().getStringExtra("PIC_GROUP");

        // ================== navigation drawer =============================
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        TextView nav_device_id = (TextView) hView.findViewById(R.id.txt_xdevice_id);
        TextView nav_pic_nik   = (TextView) hView.findViewById(R.id.txt_nav_pic_nik);
        TextView nav_pic_name  = (TextView) hView.findViewById(R.id.txt_nav_pic_name);
        nav_pic_nik.setText(StrSessionNIK);
        nav_pic_name.setText(StrSessionName);
        nav_device_id.setText(Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));
        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();
        thost = (TabHost)findViewById(R.id.TabOpname);
        thost.setup();


        TabHost.TabSpec spec = thost.newTabSpec("Opname Logistic History");
        try{
            //Tab 1
            spec.setContent(R.id.MTab_OL);
            spec.setIndicator("Opname Logistic");
            thost.addTab(spec);

            //Tab 2
            spec = thost.newTabSpec("Opname Accounting History");
            spec.setContent(R.id.MTab_OA);
            spec.setIndicator("Opname Accounting");
            thost.addTab(spec);
        }
        catch(Exception e){
        }

        HAcc_Hist = (ListView) findViewById(R.id.lst_HAcc_Hist);
        HLog_Hist = (ListView) findViewById(R.id.lst_HLog_Hist);

        // ========= menu item restriction ==============
        if(StrSessionNIK.equals("999999999")){
            thost.setVisibility(navigationView.GONE);
            menu.findItem(R.id.nav_setting).setVisible(true);
            menu.findItem(R.id.nav_opname_logistic).setVisible(false);
            menu.findItem(R.id.nav_opname_accounting).setVisible(false);
            menu.findItem(R.id.nav_sync).setVisible(true);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);
        }
        else{
            thost.setVisibility(navigationView.VISIBLE);
            menu.findItem(R.id.nav_setting).setVisible(true);

            if(StrSessionGroup.equals("ACC")){
                menu.findItem(R.id.nav_opname_accounting).setVisible(true);
                menu.findItem(R.id.nav_opname_logistic).setVisible(false);
                menu.findItem(R.id.nav_sync).setVisible(true);
                menu.findItem(R.id.nav_about).setVisible(true);
                menu.findItem(R.id.nav_exit).setVisible(true);

                thost.getTabWidget().getChildAt(0).setVisibility(View.GONE);
                thost.getTabWidget().getChildAt(1).setVisibility(View.VISIBLE);
                thost.setCurrentTab(1);

                HAcc_Hist.setTextFilterEnabled(true);
                HAcc_Hist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                        alertDialogBuilder
                                .setTitle("Confirm Delete Hist Opname Accounting")
                                .setMessage("Delete Hist Opname Accounting #" + lvi_hacc.get(position).getHAcc_NoTrx() +  " ?")
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                if(lvi_hacc.size() > 0){
                                                    dbHelper.del_HAccList(lvi_hacc.get(position).getHAccID());
                                                }
                                                EH_CMD_REFRESH_HACC(MainActivity.this);
                                            }
                                        })
                                .setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                dialog.cancel();
                                            }
                                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                        return true;
                    }
                });
                // refresh
                EH_CMD_REFRESH_HACC(MainActivity.this);
            }
            else if(StrSessionGroup.equals("LOG")){
                menu.findItem(R.id.nav_opname_accounting).setVisible(false);
                menu.findItem(R.id.nav_opname_logistic).setVisible(true);
                menu.findItem(R.id.nav_sync).setVisible(false);
                menu.findItem(R.id.nav_about).setVisible(true);
                menu.findItem(R.id.nav_exit).setVisible(true);

                thost.getTabWidget().getChildAt(0).setVisibility(View.VISIBLE);
                thost.getTabWidget().getChildAt(1).setVisibility(View.GONE);
                thost.setCurrentTab(0);

                HLog_Hist.setTextFilterEnabled(true);
                HLog_Hist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                        alertDialogBuilder
                                .setTitle("Confirm Delete Hist Opname Logistic")
                                .setMessage("Delete Hist Opname Logistic #" + lvi_hlog.get(position).getHLog_NoTrx() +  " ?")
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                if(lvi_hlog.size() > 0){
                                                    dbHelper.del_HLogList(lvi_hlog.get(position).getHLogID());
                                                }
                                                EH_CMD_REFRESH_HLOG(MainActivity.this);
                                            }
                                        })
                                .setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                dialog.cancel();
                                            }
                                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                        return true;
                    }
                });

                // refresh list
                EH_CMD_REFRESH_HLOG(MainActivity.this);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement

        if (id == R.id.mn_flushData) {
            if(StrSessionNIK.equals("999999999")){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("Warning, This Action Will Erase All Data and its Irreversible, Continue ?");
                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Proceed",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {

                                            EH_CMD_EraseAll();
                                            //EH_CMD_REFRESH_LIST(getBaseContext());
                                            EH_cmd_exit();

                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }
            else{
                msg.Show("Restriced","This Function Is Restricted Only For Default");
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.nav_sync){
            EH_cmd_sync();
        }
        else if(id == R.id.nav_opname_accounting){
            EH_cmd_load_opname_acc();
        }
        else if(id == R.id.nav_opname_logistic){
            EH_cmd_load_opname_log();
        }
        else if(id == R.id.nav_setting){
            EH_cmd_setting();
        }
        else if(id == R.id.nav_about){
            EH_cmd_about();
        }
        else if(id == R.id.nav_exit){
            EH_cmd_exit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    // ========================= EVENT HANDLER NAVIGATION ==============================
    public void EH_cmd_load_opname_log(){
        Intent I_load_opname_log;
        Log.d("[GudangGaram]", "LoadOpnameLogisticActivity Page Called");
        I_load_opname_log = new Intent(this, LoadOpnameLogisticActivity.class);
        I_load_opname_log.putExtra("PIC_NIK",  StrSessionNIK);
        I_load_opname_log.putExtra("PIC_NAME", StrSessionName);
        I_load_opname_log.putExtra("PIC_GROUP",StrSessionGroup);
        startActivity(I_load_opname_log);
    }

    public void EH_cmd_load_opname_acc(){
        Intent I_load_opname_acc;
        Log.d("[GudangGaram]", "LoadOpnameAccActivity Page Called");
        I_load_opname_acc = new Intent(this, LoadOpnameAccActivity.class);
        I_load_opname_acc.putExtra("PIC_NIK",   StrSessionNIK);
        I_load_opname_acc.putExtra("PIC_NAME",  StrSessionName);
        I_load_opname_acc.putExtra("PIC_GROUP", StrSessionGroup);
        startActivity(I_load_opname_acc);
    }

    public void EH_cmd_sync(){
        Intent I_syncPicture;
        I_syncPicture = new Intent(this, SyncAction.class);
        I_syncPicture.putExtra("PIC_NIK",  StrSessionNIK);
        I_syncPicture.putExtra("PIC_NAME", StrSessionName);
        I_syncPicture.putExtra("PIC_GROUP",StrSessionGroup);
        startActivity(I_syncPicture);
    }

    public void EH_cmd_setting(){
        Intent I_Setting;
        Log.d("[GudangGaram]", "Setting Page Called");
        I_Setting = new Intent(this, SettingActivity.class);
        I_Setting.putExtra("PIC_NIK",  StrSessionNIK);
        I_Setting.putExtra("PIC_NAME", StrSessionName);
        I_Setting.putExtra("PIC_GROUP",StrSessionGroup);
        startActivity(I_Setting);
    }

    public void EH_cmd_about(){
        Intent I_About;
        Log.d("[GudangGaram]", "About Page Called");
        I_About = new Intent(this, AboutActivity.class);
        I_About.putExtra("PIC_NIK",  StrSessionNIK);
        I_About.putExtra("PIC_NAME", StrSessionName);
        I_About.putExtra("PIC_GROUP",StrSessionGroup);
        startActivity(I_About);
    }

    public void EH_cmd_exit(){
        Intent objsignOut = new Intent(getBaseContext(),Login.class);
        objsignOut.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(objsignOut);
    }


    public void EH_CMD_EraseAll(){
        dbHelper.flushAll();
    }

}
