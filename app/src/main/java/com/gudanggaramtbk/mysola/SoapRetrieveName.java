package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

/**
 * Created by luckym on 1/31/2019.
 */

public class SoapRetrieveName {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper    dbHelper;
    private Login             pActivity;

    // override constructor
    public SoapRetrieveName(SharedPreferences PConfig){
        Log.d("[GudangGaram]:", "SoapRetrieveName Constructor");
        this.config           = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]:", "SetContext");
        this.context = context;
    }
    public void setParentActivity(Login pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]:", "SetDBHelper");
        this.dbHelper = dbHelper;
    }
    public void Retrieve(String p_nik) {
        Log.d("[GudangGaram]:", "Retrieve Name Begin");
        try {
            // call WS To Retrieve Manifest from oracle and save it to local database table
            SoapRetrieveNameTaskAsync SoapRequest = new SoapRetrieveNameTaskAsync(new SoapRetrieveNameTaskAsync.SoapRetrieveNameTaskAsyncResponse() {
                @Override
                public void PostSentAction(String p_display_name) {

                    /*
                    try {
                        if(p_display_name.equals("*") == true){
                            pActivity.display_name(p_display_name);
                        }
                        else {
                        }
                    }
                    catch (Exception e) {
                        Log.d("[GudangGaram]:", "Retrieve Exception " + e.getMessage().toString());
                    }
                    finally {
                    }
                    */
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config,p_nik,pActivity);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]:", "Retrieve Name End");
        }
    }
}