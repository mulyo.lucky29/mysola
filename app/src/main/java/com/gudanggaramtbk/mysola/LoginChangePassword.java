package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginChangePassword extends AppCompatActivity {
    private MySQLiteHelper  dbHelper;
    private SharedPreferences config;

    private static TextView Txt_ch_nik;
    private static TextView Txt_ch_group;
    private static TextView Txt_ch_new_password;
    private static TextView Txt_ch_con_new_password;
    private static TextView Txt_ch_message;
    private static Button   Cmd_ch_submit;

    private String StrSessionNIK;
    private String StrSessionName;
    private String StrSessionGroup;
    private SoapResetPassword  ResetPassword;

    public String getStrSessionName() {
        return StrSessionName;
    }
    public String getStrSessionNIK() {
        return StrSessionNIK;
    }
    public String getStrSessionGroup() {
        return StrSessionGroup;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_change_password);
        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);

        Log.d("[GudangGaram]", "LoginChangePassword :: onCreate");

        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
        StrSessionGroup = getIntent().getStringExtra("PIC_GROUP");

        Txt_ch_nik              = (TextView) findViewById(R.id.txt_ch_nik);
        Txt_ch_group            = (TextView) findViewById(R.id.txt_ch_Group);
        Txt_ch_new_password     = (TextView) findViewById(R.id.txt_ch_new_password);
        Txt_ch_con_new_password = (TextView) findViewById(R.id.txt_ch_con_new_password);
        Txt_ch_message          = (TextView) findViewById(R.id.txt_ch_message);
        Cmd_ch_submit           = (Button)   findViewById(R.id.cmd_ch_submit);

        Txt_ch_nik.setText(StrSessionNIK);
        Txt_ch_group.setText(StrSessionGroup);

        Cmd_ch_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // redirect to main page
                EH_reset_password();
            }
        });

        // service to send request to reset password
        ResetPassword = new SoapResetPassword(config);
        ResetPassword.setParentActivity(LoginChangePassword.this);
        ResetPassword.setContext(LoginChangePassword.this);
        ResetPassword.setDBHelper(dbHelper);

    }

    public void display_error(final String errm){
        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
                Txt_ch_message.setText(errm);
                Txt_ch_new_password.setText("");
                Txt_ch_con_new_password.setText("");
            }
            public void onFinish() {
                Txt_ch_message.setText("");
            }
        }.start();
    }

    public void go_login_page(String p_nik, String p_name, String p_group){
        Log.d("[GudangGaram]", "LoginChangePassword :: go_login_page Begin");
        try{
            Intent Login_intent = new Intent(this.getBaseContext(), Login.class);
            Login_intent.putExtra("PIC_NIK",   p_nik);
            Login_intent.putExtra("PIC_NAME",  p_name);
            Login_intent.putExtra("PIC_GROUP", p_group);
            startActivity(Login_intent);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoginChangePassword :: go_login_page Exception : "+ e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "LoginChangePassword :: go_login_page End");
    }

    public void EH_reset_password(){
        Log.d("[GudangGaram]", "LoginChangePassword :: EH_reset_password Begin");

        if((Txt_ch_new_password.getText().length() > 0) && (Txt_ch_con_new_password.getText().length() > 0)){
            if(Txt_ch_new_password.getText().toString().equals(Txt_ch_con_new_password.getText().toString()) == true){
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                if(dbHelper.isServiceConnect(config,cm,LoginChangePassword.this) == true){
                    try{
                        // call web service to reset password, after finish called login page
                        ResetPassword.Reset(StrSessionNIK, Txt_ch_new_password.getText().toString());
                    }
                    catch(Exception e){
                    }
                }
                else{
                    display_error("Service Unavailable");
                }
            }
            else{
                display_error("New Password and Password Confirm Not Match");
            }
        }
        else{
            display_error("Please Mention New Password and Password Confirm");
        }
        Log.d("[GudangGaram]", "LoginChangePassword :: EH_reset_password End");
    }


}
