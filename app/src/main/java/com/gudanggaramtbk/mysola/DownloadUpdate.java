package com.gudanggaramtbk.mysola;

import android.Manifest;
        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.content.pm.PackageManager;
        import android.content.pm.ResolveInfo;
        import android.net.Uri;
        import android.os.AsyncTask;
        import android.os.Build;
        import android.os.Environment;
        import android.support.v4.app.ActivityCompat;
        import android.support.v4.content.FileProvider;
        import android.util.Log;

        import java.io.File;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.net.HttpURLConnection;
        import java.net.URL;
        import java.util.List;

/**
 * Created by LuckyM on 3/6/2018.
 */

public class DownloadUpdate {
    private SharedPreferences config;
    private Context context;
    private String apkbaseurl;
    private ProgressDialog pdLoading;

    // override constructor
    public DownloadUpdate(SharedPreferences PConfig){
        this.config = PConfig;
    }
    public void setContext(Context context){
        this.context = context;
    }

    public void InstalApk(){
        Intent intent;
        Uri myuri;
        File fileapk;
        String PathName;
        Log.d("[GudangGaram]: ", "APK Install");

        try{
            PathName = Environment.getExternalStorageDirectory().toString() + File.separator + "app-release.apk";
            fileapk  = new File(PathName);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.d("[GudangGaram]: ", "APK Install Higher Version ( >= 7.0)");
                myuri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", fileapk);
                Uri packageURI = Uri.parse("com.gudanggaramtbk.mysola");

                intent = new Intent(android.content.Intent.ACTION_VIEW, packageURI);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(myuri, "application/vnd.android.package-archive");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            else {
                Log.d("[GudangGaram]: ", "APK Install Lower Version");
                myuri = Uri.fromFile(fileapk);

                intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(myuri, "application/vnd.android.package-archive");
            }
            context.startActivity(intent);
            Log.d("[GudangGaram]", "APK Install Done");
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "APK Install Exception : " + e.getMessage());
        }
    }

    public void DownloadApk()
    {
        Log.d("[GudangGaram]", "DownloadUpdate :: DownloadAPK");
        this.apkbaseurl = config.getString("URLUpdater","");
        DownloadTaskAsync d = new DownloadTaskAsync(new DownloadTaskAsync.DownloadTaskAsyncResponse() {
            @Override
            public void PostDownloadAction(String output) {
                Log.d("[GudangGaram]", "APK Download Status " + output);
                if(output == "Finish"){
                    InstalApk();
                }
                //pdLoading.dismiss();
            }
        });
        d.setContext(context);
        d.execute(apkbaseurl);
    }
}