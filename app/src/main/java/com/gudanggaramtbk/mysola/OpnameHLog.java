package com.gudanggaramtbk.mysola;

/**
 * Created by luckym on 12/3/2018.
 */

public class OpnameHLog {
    private String HLogID;
    private String HLog_NoTrx;
    private String HLog_TglOpnam;
    private String HLog_Gudang;
    private String HLog_Rak ;
    private String HLog_Bin;
    private String HLog_NIK;


    // ------------- getter ----------------------
    public String getHLogID() {
        return HLogID;
    }
    public String getHLog_NoTrx() {
        return HLog_NoTrx;
    }
    public String getHLog_TglOpnam() {
        return HLog_TglOpnam;
    }
    public String getHLog_Gudang() {
        return HLog_Gudang;
    }
    public String getHLog_Rak() {
        return HLog_Rak;
    }
    public String getHLog_Bin() {
        return HLog_Bin;
    }
    public String getHLog_NIK() {
        return HLog_NIK;
    }


    // -------------- setter ---------------------
    public void setHLogID(String HLogID) {
        this.HLogID = HLogID;
    }
    public void setHLog_NoTrx(String HLog_NoTrx) {
        this.HLog_NoTrx = HLog_NoTrx;
    }
    public void setHLog_TglOpnam(String HLog_TglOpnam) {
        this.HLog_TglOpnam = HLog_TglOpnam;
    }
    public void setHLog_Gudang(String HLog_Gudang) {
        this.HLog_Gudang = HLog_Gudang;
    }
    public void setHLog_Rak(String HLog_Rak) {
        this.HLog_Rak = HLog_Rak;
    }
    public void setHLog_Bin(String HLog_Bin) {
        this.HLog_Bin = HLog_Bin;
    }
    public void setHLog_NIK(String HLog_NIK) {
        this.HLog_NIK = HLog_NIK;
    }
}
