package com.gudanggaramtbk.mysola;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by luckym on 11/26/2018.
 */

public class SoapSendDataOA {
    private SharedPreferences       config;
    private Context                 context;
    private MySQLiteHelper          dbHelper;
    private LoadOpnameAccActivity   pActivity;

    // override constructor
    public SoapSendDataOA(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapSendDataOA :: Constructor");
        this.config           = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapSendDataOA :: SetContext");
        this.context = context;
    }
    public void setParentActivity(LoadOpnameAccActivity pActivity){
        this.pActivity = pActivity;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapSendDataOA :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void Send(final OpnameData oOpnameAcc, final String pNiK, String pDeviceID, String pLastFlag) {
        final Integer ncount;
        final Integer itercount;
        Log.d("[GudangGaram]", "SoapSendDataOA :: Send > Begin");

        try {
            // call WS To Retrieve Manifest from oracle and save it to local database table
            SoapSendDataOATaskAsync SoapRequest = new SoapSendDataOATaskAsync(new SoapSendDataOATaskAsync.SoapSendDataOATaskAsyncResponse() {
                @Override
                public void PostSentAction(String ResultCode) {
                    String[] ResultString;
                    String sNik;

                    sNik = pNiK;
                    try {
                        Log.d("[GudangGaram]", "Send :: PostSentAction :: " + ResultCode);

                        if(ResultCode.contains("|")){
                            ResultString = ResultCode.split(Pattern.quote("|"));
                            String soacc_int_id    = ResultString[0].toString();
                            String soacc_detail_id = ResultString[1].toString();
                            String trx_no          = ResultString[2].toString();
                            //String OrgName         = ResultString[3].toString();

                            Log.d("[GudangGaram]", "Send :: soacc_int_id    :: " + soacc_int_id);
                            Log.d("[GudangGaram]", "Send :: soacc_detail_id :: " + soacc_detail_id);
                            Log.d("[GudangGaram]", "Send :: trx_no          :: " + trx_no);

                            // delete per line data opnamed transfered to oracle
                            if(soacc_detail_id.length() > 0){
                                dbHelper.delOpnameData(soacc_detail_id);
                                Log.d("[GudangGaram]", "dbHelper.delOpnameData(" + soacc_detail_id + ")");
                            }
                            // insert no trx for last transaction confirmed
                            if(trx_no.length() > 0){
                                if(trx_no.equals("0")){
                                }
                                else{
                                    // save to db no trx already sent

                                    // ----------- save to history acc --------
                                    Log.d("[GudangGaram]", "===============================================================");
                                    Log.d("[GudangGaram]", "SoapSenDataOA :: No Trx      = " + trx_no);
                                    Log.d("[GudangGaram]", "SoapSenDataOA :: Opname Code = " + oOpnameAcc.getOpnameCode().toString());
                                    Log.d("[GudangGaram]", "SoapSenDataOA :: Org Name    = " + oOpnameAcc.getOrgName().toString());
                                    Log.d("[GudangGaram]", "SoapSenDataOA :: Opname Date = " + oOpnameAcc.getOpnameDate().toString());
                                    Log.d("[GudangGaram]", "SoapSenDataOA :: Nik         = " + sNik);

                                    OpnameHAcc oHacc = new OpnameHAcc();
                                    oHacc.setHAcc_NoTrx(trx_no);
                                    oHacc.setHAcc_KodeOpnam(oOpnameAcc.getOpnameCode().toString());
                                    oHacc.setHAcc_TglOpnam(oOpnameAcc.getOpnameDate().toString());
                                    oHacc.setHAcc_Gudang(oOpnameAcc.getOrgName());
                                    oHacc.setHAcc_NIK(sNik);

                                    dbHelper.add_HAccList(oHacc);

                                    // reresh combobox kode opname
                                    pActivity.initComboBoxKodeOpname(pNiK);
                                    // clear textfield gudang
                                    pActivity.initTextGudang();
                                    // refresh list
                                    pActivity.EH_CMD_POPULATE();
                                    // refresh combo list
                                    pActivity.DialogBox("Send Opname Acc Done","Opname Acc Record As #" + trx_no);
                                }
                            }
                        }
                        else{
                            Log.d("[GudangGaram]", "Send :: ResultCode Not Contain |");
                        }
                    }
                    catch (Exception e) {
                        Log.d("[GudangGaram]:", "SoapSendDataOA :: Exception " + e.getMessage().toString());
                    }
                    finally {
                        Log.d("[GudangGaram]:", "SoapSendDataOA Finally End");
                    }
                }
            });

            SoapRequest.setAttribute(context, dbHelper, config, oOpnameAcc, pNiK, pDeviceID, pLastFlag);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]:", "SoapSendDataOA :: Send OA End");
        }
    }

}
