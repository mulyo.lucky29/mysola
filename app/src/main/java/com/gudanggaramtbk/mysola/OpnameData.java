package com.gudanggaramtbk.mysola;

/**
 * Created by LuckyM on 11/1/2018.
 */

public class OpnameData {
    private Integer Dtl_ID;         // soacc_dtl_id
    private Integer FileID;         // fileid
    private Integer Seq;            // sequence_number
    private Integer OrgID;          // organization_id
    private String  OrgName;        // organization_name
    private Integer ItemID;         // item_id
    private String  ItemCode;       // item_code
    private String  ItemDesc;       // item_description
    private String  ItemOld;        // old_item
    private String  LocRak;         // rak
    private String  LocBin;         // bin
    private String  LocComp;        // compartement
    private String  LotNum;         // lot_number
    private String  KoliNum;        // koli_number
    private String  SubInvStatus;   // status_subinv
    private Integer RakSeq;         // rak_Seq
    private Integer BinSeq;         // bin_Seq
    private Double  QtyOnHand;      // qty_onhand
    private Double  QtyPicked;      // qty_picked
    private Double  QtyCheck;       //
    private String  Notes;          //
    private Boolean Note_Enabled;
    private String  Uom;            // primary_uom
    private String  PicNIK;         // pic_nik
    private Integer PicPersonID;    // pic_person_id
    private String  PicName;        // pic_name
    private String  CreationDate;
    private String  OpnameDate;
    private String  OpnameNumber;
    private String  OpnameCode;     // opname_code
    private Boolean ChkFlag;

    // --------- getter --------------
    public Integer getDtl_ID() {
        return Dtl_ID;
    }
    public Integer getFileID() {
        return FileID;
    }
    public Integer getSeq() {
        return Seq;
    }
    public Integer getOrgID() {
        return OrgID;
    }
    public String  getOrgName() {
        return OrgName;
    }
    public Integer getItemID() {
        return ItemID;
    }
    public String  getItemCode() {
        return ItemCode;
    }
    public String  getItemDesc() {
        return ItemDesc;
    }
    public String  getItemOld() {
        return ItemOld;
    }
    public String  getLocRak() {
        return LocRak;
    }
    public String  getLocBin() {
        return LocBin;
    }
    public String  getLocComp() {
        return LocComp;
    }
    public String  getLotNum() {
        return LotNum;
    }
    public String  getKoliNum() {
        return KoliNum;
    }
    public String  getSubInvStatus() {
        return SubInvStatus;
    }
    public Integer getRakSeq() {
        return RakSeq;
    }
    public Integer getBinSeq() {
        return BinSeq;
    }
    public Double  getQtyOnHand() {
        return QtyOnHand;
    }
    public Double  getQtyPicked() {
        return QtyPicked;
    }
    public Double  getQtyCheck() {
        return QtyCheck;
    }
    public String  getNotes() {
        return Notes;
    }
    public String  getUom() {
        return Uom;
    }
    public String  getPicNIK() {
        return PicNIK;
    }
    public Integer getPicPersonID() {
        return PicPersonID;
    }
    public String  getPicName() {
        return PicName;
    }
    public String  getCreationDate() {
        return CreationDate;
    }
    public String  getOpnameDate() {
        return OpnameDate;
    }
    public String  getOpnameNumber() {
        return OpnameNumber;
    }
    public String  getOpnameCode() {
        return OpnameCode;
    }
    public Boolean getChkFlag() {
        return ChkFlag;
    }
    public Boolean getNote_Enabled() {
        return Note_Enabled;
    }

    // --------- setter --------------
    public void setDtl_ID(Integer dtl_ID) {
        Dtl_ID = dtl_ID;
    }
    public void setFileID(Integer fileID) {
        FileID = fileID;
    }
    public void setSeq(Integer seq) {
        Seq = seq;
    }
    public void setOrgID(Integer orgID) {
        OrgID = orgID;
    }
    public void setOrgName(String orgName) {
        OrgName = orgName;
    }
    public void setItemID(Integer itemID) {
        ItemID = itemID;
    }
    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }
    public void setItemDesc(String itemDesc) {
        ItemDesc = itemDesc;
    }
    public void setItemOld(String itemOld) {
        ItemOld = itemOld;
    }
    public void setLocRak(String locRak) {
        LocRak = locRak;
    }
    public void setLocBin(String locBin) {
        LocBin = locBin;
    }
    public void setLocComp(String locComp) {
        LocComp = locComp;
    }
    public void setLotNum(String lotNum) {
        LotNum = lotNum;
    }
    public void setKoliNum(String koliNum) {
        KoliNum = koliNum;
    }
    public void setSubInvStatus(String subInvStatus) {
        SubInvStatus = subInvStatus;
    }
    public void setRakSeq(Integer rakSeq) {
        RakSeq = rakSeq;
    }
    public void setBinSeq(Integer binSeq) {
        BinSeq = binSeq;
    }
    public void setQtyOnHand(Double qtyOnHand) {
        QtyOnHand = qtyOnHand;
    }
    public void setQtyPicked(Double qtyPicked) {
        QtyPicked = qtyPicked;
    }
    public void setQtyCheck(Double qtyCheck) {
        QtyCheck = qtyCheck;
    }
    public void setNotes(String notes) {
        Notes = notes;
    }
    public void setUom(String uom) {
        Uom = uom;
    }
    public void setPicNIK(String picNIK) {
        PicNIK = picNIK;
    }
    public void setPicPersonID(Integer picPersonID) {
        PicPersonID = picPersonID;
    }
    public void setPicName(String picName) {
        PicName = picName;
    }
    public void setCreationDate(String creationDate) {
        CreationDate = creationDate;
    }
    public void setOpnameDate(String opnameDate) {
        OpnameDate = opnameDate;
    }
    public void setOpnameNumber(String opnameNumber) {
        OpnameNumber = opnameNumber;
    }
    public void setOpnameCode(String opnameCode) {
        OpnameCode = opnameCode;
    }
    public void setChkFlag(Boolean chkFlag) {
        ChkFlag = chkFlag;
    }
    public void setNote_Enabled(Boolean note_Enabled) {
        Note_Enabled = note_Enabled;
    }

}
