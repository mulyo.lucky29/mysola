# My SOLA
##
SOLA adalah aplikasi mobile yang menghandle proses stock opname logistic dan accounting. 2 process opname yang di jadikan satu aplikasi yang di jalankan oleh 2 pihak yang berbeda. Opname logistic lebih kepada opname tata letak barang. sedangkan opname Accounting lebih kepada jumlah persediaan. 
 
## Screen Example 
login <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/mysola/mysola_login.png?raw=true)

menu <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/mysola/mysola_menu.png?raw=true)

Opname Logistic <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/mysola/mysola_opname.png?raw=true)
Opname Accounting
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/mysola/mysola_transact.png?raw=true)

## License
**Copyright 2022 Lucky Mulyo**

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.